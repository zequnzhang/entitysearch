package extraction.email;
import extraction.Entity;
%%
%class EmailExtractorImpl
%unicode
%pack
%char
%caseless
%function getNextToken
%type Entity
%{
  final static String EMAIL_TAG="email";
%}


WORD				=	[A-Za-z0-9]("-"|"_"|[A-Za-z0-9])*
EMAIL				=	{WORD}("@"|" at "|"[at]"){WORD}(("."|" dot "){WORD})+

%%
<YYINITIAL>	{
{EMAIL}		{	return new Entity(EMAIL_TAG, yychar,yychar+yytext().length());	}
.			{	}
}
