package index;

import index.TextTokenList;

public class IndexDoc {
	public String docId;
	public String url;
	public TextTokenList tokenList;

	public String toString() {
		String r = "";
		r += "Doc ID: " + docId + "\n";
		r += "URL: " + url + "\n";
		r += "Content: " + tokenList.size() + " Tokens.\n";
		return r;
	}
}
