package index.builder;

import index.TextTokenList;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import util.ByteList;
import util.ExtStringMap;
import util.ForwardArrayList;
import util.NumberUtil;
import util.StringMap;

public class ForwardTextIndexBuilder {

	String termMapFile = "./term";
	ForwardArrayList list = null;
	StringMap map;
	int num;
	ExtStringMap<Integer> docIdMap;

	public ForwardTextIndexBuilder(String indexPath) throws IOException,
			ClassNotFoundException, SQLException {
		map = new StringMap(termMapFile, false);
		list = new ForwardArrayList(indexPath, "text", true);
		docIdMap = new ExtStringMap<Integer>(indexPath + "/doc_id.db", true, 40, "doc", "id", new Integer(0));
		num = 0;
	}

	public void addDocument(String docId, String url, TextTokenList tokenList)
			throws IOException, SQLException {
		ByteList byteList = new ByteList();
		int offset = 0;
		byteList.append(map.compressString(docId, 2));
		byteList.append(map.compressString(url, 2));
		for (int i = 0; i < tokenList.size(); i++) {
			byteList.append(map.compressString(tokenList.get(i).text, 2));
			int increment = tokenList.get(i).startOffset - offset;
			assert (increment >= 0);
			byteList.append(NumberUtil.unsignedIntToBytes(increment));
			offset = tokenList.get(i).startOffset;
		}
		list.append(byteList.toBytes());
		docIdMap.add(docId, num);
		if (num % 10000 == 0)
			System.out.println(num);
		num++;
	}

	public void close() throws IOException, SQLException {
		System.out.println("Building Index ...");
		docIdMap.buildIndex();
		docIdMap.close();
		list.close();
	}
}
