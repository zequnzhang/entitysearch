package index.builder;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import util.Pair;
import extraction.Entity;
import extraction.EntityInfo;
import extraction.reader.EntityReader;

import util.BufferedFileReader;
import util.ByteList;
import util.ForwardArrayList;
import util.NumberUtil;
import index.IndexDoc;
import index.TextTokenList;
import index.reader.ForwardTextIndexReader;
import index.tokenizer.DoCQSTokenizer;

public class ForwardNERIndexBuilder {
	public static final String[] TYPE_LIST = new String[] { "#organization",
			"#date", "#percent", "#person", "#location", "#money", "#time",
			"#email", "#number" };

	ArrayList<String> fileList;
	HashMap<String, Integer> type2id;
	String forwardIndexPath;

	public ForwardNERIndexBuilder(ArrayList<String> fileList,
			String forwardIndexPath) {
		this.fileList = fileList;
		this.forwardIndexPath = forwardIndexPath;
		type2id = new HashMap<>();
		for (int i = 0; i < TYPE_LIST.length; i++) {
			type2id.put(TYPE_LIST[i], i);
		}
	}

	public void buildIndex() throws IOException, ClassNotFoundException,
			SQLException {
		ForwardTextIndexReader textReader = new ForwardTextIndexReader(
				forwardIndexPath);
		ForwardArrayList output = new ForwardArrayList(forwardIndexPath,
				"entity_ner", true);
		int k = 0;
		for (String filename : fileList) {
			EntityReader nerReader = new EntityReader(filename.replaceAll(
					"text", "ner"));
			EntityReader emailReader = new EntityReader(filename.replaceAll(
					"text", "email"));
			EntityReader numberReader = new EntityReader(filename.replaceAll(
					"text", "number"));
			while (nerReader.nextDoc()) {
				emailReader.nextDoc();
				numberReader.nextDoc();
				IndexDoc indexDoc = textReader.getTokenList(k);

				if (!nerReader.doc().equals(indexDoc.docId)
						|| !nerReader.doc().equals(indexDoc.docId)
						|| !nerReader.doc().equals(indexDoc.docId)) {
					System.out.println("Error: " + indexDoc.docId + "\t"
							+ nerReader.doc() + "\t" + emailReader.doc() + "\t"
							+ numberReader.doc());
					System.exit(-1);
				}

				ArrayList<Entity> entityList = new ArrayList<>();
				entityList.addAll(nerReader.entities());
				entityList.addAll(numberReader.entities());
				entityList.addAll(emailReader.entities());

				ArrayList<EntityInfo> infoList = new ArrayList<EntityInfo>();
				for (Entity e : entityList) {
					Pair<Integer, Integer> v = indexDoc.tokenList.getPosition(
							e.startOffset, e.endOffset);
					EntityInfo info = new EntityInfo(type2id.get(e.type),
							e.type, v.first, v.second,
							indexDoc.tokenList.getSeperatedText(v.first,
									v.second));
					infoList.add(info);
				}

				Collections.sort(infoList);
				int pos = 0;
				ByteList byteList = new ByteList();
				for (EntityInfo info : infoList) {
					byteList.append(NumberUtil.unsignedIntToBytes(info.typeId));
					byteList.append(NumberUtil.unsignedIntToBytes(info.pos
							- pos));
					pos = info.pos;
					byteList.append(NumberUtil.unsignedIntToBytes(info.len));
				}
				output.append(byteList.toBytes());

				if (k % 10000 == 0)
					System.out.println(k);
				k++;
			}
		}
		textReader.close();
		output.close();
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, SQLException {
		BufferedFileReader reader = new BufferedFileReader(
				"./config_file/keyword_index/" + args[0]);
		ArrayList<String> fileList = new ArrayList<>();
		String line;
		while ((line=reader.readLine())!=null) {
			fileList.add(line);
		}
		reader.close();

		ForwardNERIndexBuilder builder = new ForwardNERIndexBuilder(fileList,
				"/shared/osprey/cqs-data/clueweb_index/index/" + args[0] + "/forward/");
		builder.buildIndex();
	}
}
