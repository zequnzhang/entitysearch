package index.builder;

import index.IndexDoc;
import index.reader.ForwardTextIndexReader;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import extraction.EntityInfo;
import extraction.RawFBEntity;
import extraction.reader.FreebaseEntityReader;

import util.BufferedFileReader;
import util.ByteList;
import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;
import util.TrieReader;

public class ForwardFBIndexBuilder {

	ArrayList<String> fileList;
	String forwardIndexPath;
	String fbTriePath;

	public ForwardFBIndexBuilder(ArrayList<String> fileList, String fbTriePath,
			String forwardIndexPath) {
		this.fileList = fileList;
		this.forwardIndexPath = forwardIndexPath;
		this.fbTriePath = fbTriePath;
	}

	public void buildIndex() throws IOException, ClassNotFoundException,
			SQLException {
		System.out.println("Reading Trie ...");
		TrieReader reader = new TrieReader(fbTriePath);

		ForwardTextIndexReader textReader = new ForwardTextIndexReader(
				forwardIndexPath);
		ForwardArrayList output = new ForwardArrayList(forwardIndexPath,
				"entity_freebase", true);
		int k = 0;
		for (String filename : fileList) {
			FreebaseEntityReader entityReader = new FreebaseEntityReader(
					filename.replaceAll("text", "annotation"));
			while (entityReader.nextDoc()) {
				if (k >= textReader.docNum()) {
					System.out.println("Early Break ...");
					break;
				}
				IndexDoc indexDoc = textReader.getTokenList(k);
				if (!entityReader.docId().equals(indexDoc.docId)) {
					System.out.println("Error: " + indexDoc.docId + "\t"
							+ entityReader.docId());
					// Bug in GZIPTextReader.... Should be fixed in the future.
					break;
				}

				ArrayList<EntityInfo> infoList = new ArrayList<EntityInfo>();
				HashMap<String, Integer> typeMap = new HashMap<String, Integer>();
				ArrayList<String> typeList = new ArrayList<String>();
				for (RawFBEntity e : entityReader.entityList()) {
					Pair<Integer, Integer> v = indexDoc.tokenList.getPosition(
							e.start, e.end);
					Integer tid = typeMap.get(e.eid);
					if (tid == null) {
						tid = typeMap.size();
						typeMap.put(e.eid, tid);
						typeList.add(e.eid);
					}
					EntityInfo info = new EntityInfo(tid, e.eid, v.first,
							v.second, null);
					infoList.add(info);
				}

				Collections.sort(infoList);
				int pos = 0;
				ByteList byteList = new ByteList();
				byteList.append(NumberUtil.unsignedIntToBytes(infoList.size()));
				for (EntityInfo info : infoList) {
					byteList.append(NumberUtil.unsignedIntToBytes(info.typeId));
					byteList.append(NumberUtil.unsignedIntToBytes(info.pos
							- pos));
					pos = info.pos;
					byteList.append(NumberUtil.unsignedIntToBytes(info.len));
				}
				byteList.append(NumberUtil.unsignedIntToBytes(typeList.size()));
				for (int i = 0; i < typeList.size(); i++) {
					String typeStr = typeList.get(i).substring(1)
							.replaceAll("/", ".");
					byteList.append(typeStr.getBytes());
					byteList.add((byte) 0);
					String valStr = reader.get(typeStr);
					// System.out.println(typeStr + "  -->  " + valStr);
					if (valStr == null) {
						byteList.append(NumberUtil.unsignedIntToBytes(0));
					} else {
						String[] ss = valStr.split("\t");
						byteList.append(NumberUtil
								.unsignedIntToBytes(ss.length));
						for (int j = 0; j < ss.length; j++)
							byteList.append(NumberUtil.unsignedIntToBytes(
									Integer.parseInt(ss[j]), 2));
					}
				}
				output.append(byteList.toBytes());

				if (k % 10000 == 0)
					System.out.println(k);
				k++;
			}
		}
		textReader.close();
		output.close();
	}

	public static void main(String[] args) throws IOException,
			ClassNotFoundException, SQLException {
		// ArrayList<String> fileList = new ArrayList<>();
		// fileList.add("D:/workspace/program/EntityIndex/testdata/data/text/90.gz");
		// fileList.add("D:/workspace/program/EntityIndex/testdata/data/text/91.gz");
		// ForwardFBIndexBuilder builder = new ForwardFBIndexBuilder(fileList,
		// "D:/workspace/program/EntityIndex/testdata/index/forward/");
		// builder.buildIndex();

		BufferedFileReader reader = new BufferedFileReader(
				"./config_file/keyword_index/" + args[0]);
		ArrayList<String> fileList = new ArrayList<>();
		String line;
		while ((line = reader.readLine()) != null) {
			fileList.add(line);
		}
		reader.close();

		ForwardFBIndexBuilder builder = new ForwardFBIndexBuilder(
				fileList,
				"/shared/osprey/cqs-data/clueweb_index/data/general/freebase_entity_type_trie",
				"/shared/osprey/cqs-data/clueweb_index/index/" + args[0]
						+ "/forward/");
		builder.buildIndex();
	}

}
