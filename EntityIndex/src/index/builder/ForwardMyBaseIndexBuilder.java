package index.builder;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import index.TextTokenList;
import index.reader.ForwardTextIndexReader;
import util.BufferedFileReader;
import util.ByteList;
import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;
import util.TrieReader;

public class ForwardMyBaseIndexBuilder {

	String mbTriePath;
	String forwardIndexPath;

	public ForwardMyBaseIndexBuilder(String mbTriePath, String forwardIndexPath) {
		this.mbTriePath = mbTriePath;
		this.forwardIndexPath = forwardIndexPath;
	}

	public int wordNum(String str) {
		int n = 0;
		for (int i = 0; i < str.length(); i++)
			if (str.charAt(i) == ' ')
				n++;
		return n + 1;
	}

	public void buildIndex() throws IOException, ClassNotFoundException,
			SQLException {
		HashMap<String, Integer> v = new HashMap<String, Integer>();
		BufferedFileReader r = new BufferedFileReader(
				"./general/frequent_entity_category");
		String line;
		int k = 0;
		while ((line = r.readLine()) != null) {
			v.put(line.split("\t")[0], k);
			k++;
		}
		r.close();

		System.out.println("Reading Trie ...");
		TrieReader reader = new TrieReader(mbTriePath);

		ForwardTextIndexReader textReader = new ForwardTextIndexReader(
				forwardIndexPath);
		ForwardArrayList output = new ForwardArrayList(forwardIndexPath,
				"entity_mybase", true);
		for (int docId = 0; docId < textReader.docNum(); docId++) {
			if (docId % 10000 == 0)
				System.out.println(docId);
			TextTokenList tl = textReader.getTokenList(docId).tokenList;
			int pos = 0, len;
			Integer id;
			String str, cat;
			HashMap<String, Integer> catMap = new HashMap<String, Integer>();
			ArrayList<String> catList = new ArrayList<String>();
			ArrayList<EntityMention> mentionList = new ArrayList<>();
			int pre = 0;
			while (pos < tl.size()) {
				StringBuffer buf = new StringBuffer();
				for (int j = 0; j < 6 && pos + j < tl.size(); j++)
					buf.append(tl.get(pos + j).text.toLowerCase() + " ");
				Pair<String, String> p = reader.getPrefix(buf.toString());
				if (p == null) {
					pos++;
					continue;
				}
				str = p.first;
				cat = p.second;
				len = wordNum(str);

				if (pos + len <= pre) {
					pos++;
					continue;
				}
				id = v.get(str);
				if (id == null) {
					id = catMap.get(str);
					if (id == null) {
						id = v.size() + catMap.size();
						catMap.put(str, id);
						catList.add(cat);
					}
				}
				mentionList.add(new EntityMention(id, pos, len));
				pre = pos + len;
				pos++;
			}

			ByteList byteList = new ByteList();
			// System.out.println(docId+"\t"+mentionList.size());
			byteList.append(NumberUtil.unsignedIntToBytes(mentionList.size(), 2));
			pos = 0;
			for (EntityMention m : mentionList) {
				byteList.append(NumberUtil.unsignedIntToBytes(m.id, 2));
				byteList.append(NumberUtil.unsignedIntToBytes(m.pos - pos));
				byteList.append(NumberUtil.unsignedIntToBytes(m.len));
				pos = m.pos;
			}
			byteList.append(NumberUtil.unsignedIntToBytes(catList.size()));
			for (String c : catList) {
				String[] ss = c.split(",");
				byteList.append(NumberUtil.unsignedIntToBytes(ss.length));
				for (int i = 0; i < ss.length; i++) {
					byteList.append(NumberUtil.unsignedIntToBytes(
							Integer.parseInt(ss[i]), 2));
				}
			}
			output.append(byteList.toBytes());
		}
		output.close();
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, SQLException {
		ForwardMyBaseIndexBuilder builder = new ForwardMyBaseIndexBuilder(
				"/shared/osprey/cqs-data/clueweb_index/data/general/mybase_trie",
				"/shared/osprey/cqs-data/clueweb_index/index/" + args[0] + "/forward/");
		builder.buildIndex();
	}

	class EntityMention {
		int id;
		int pos;
		int len;

		EntityMention(int id, int pos, int len) {
			this.id = id;
			this.pos = pos;
			this.len = len;
		}
	}

}
