package index.builder;

import index.IndexTerm;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Payload;
import org.apache.lucene.index.IndexWriter.MaxFieldLength;
import org.apache.lucene.store.LockObtainFailedException;

public class LuceneIndexBuilder {

	IndexWriter writer;
	DummyAnalyzer analyzer;

	public LuceneIndexBuilder(String indexPath) throws IOException {
		analyzer = new DummyAnalyzer();
		writer = new IndexWriter(indexPath,
				analyzer, true, MaxFieldLength.LIMITED);
		writer.setRAMBufferSizeMB(100);
	}

	public void addDocument(ArrayList<IndexTerm> terms) throws IOException {
		analyzer.setTokenStream(new ListTokenStream(terms));
		Document doc = new Document();
		doc.add(new Field("Content", "...", Store.NO, Index.ANALYZED,
				TermVector.NO));
		writer.addDocument(doc);
	}

	public void close() throws IOException {
		writer.close();
	}
}

class DummyAnalyzer extends Analyzer {

	TokenStream tokenStream;

	public void setTokenStream(TokenStream ts) {
		this.tokenStream = ts;
	}

	@Override
	public TokenStream tokenStream(String arg0, Reader arg1) {
		// Dummy Function
		return tokenStream;
	}
}

class ListTokenStream extends TokenStream {
	ArrayList<IndexTerm> list;
	int prePos, j;

	public ListTokenStream(ArrayList<IndexTerm> list) {
		this.list = list;
		prePos = -1;
		j = 0;
	}

	public Token next() throws IOException {
		if (j >= list.size())
			return null;

		Token token = new Token();
		IndexTerm term = list.get(j);
		token.setTermText(term.text);
		assert(term.pos>=prePos);
		token.setPositionIncrement(term.pos - prePos);
		prePos = term.pos;
		if (term.bytes != null) {
			Payload p = new Payload(term.bytes);
			token.setPayload(p);
		}
		j++;
		return token;
	}
}
