package index.builder;

import index.IndexTerm;
import index.TextTokenList;
import index.reader.ForwardTextIndexReader;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.index.TermPositions;

import util.BufferedFileReader;
import util.PorterStemmer;

public class KeywordIndexBuilder {

	String indexPath;
	String forwardPath;

	public KeywordIndexBuilder(String forwardPath, String indexPath) {
		this.forwardPath = forwardPath;
		this.indexPath = indexPath;
	}

	public void run() throws IOException, ClassNotFoundException, SQLException {
		PorterStemmer stemmer = new PorterStemmer();
		HashSet<String> stopwords = new HashSet<>();
		BufferedFileReader stopReader = new BufferedFileReader("./general/stoplist");
		String line;
		while ((line=stopReader.readLine())!=null) {
			stopwords.add(stemmer.stem(line));			
		}
		stopReader.close();
		
		
		ForwardTextIndexReader textReader = new ForwardTextIndexReader(
				forwardPath);
		LuceneIndexBuilder indexBuilder = new LuceneIndexBuilder(indexPath
				+ "/keyword/");
		for (int i = 0; i < textReader.docNum(); i++) {
			if (i % 10000 == 0)
				System.out.println(i);
			TextTokenList list = textReader.getTokenList(i).tokenList;
			ArrayList<IndexTerm> termList = new ArrayList<IndexTerm>();
			for (int j = 0; j < list.size(); j++) {
				IndexTerm term = new IndexTerm();
				term.bytes = null;
				term.text = list.get(j).text.toLowerCase();
				term.pos = j;
				termList.add(term);
				
				// stemmed word
				String s = stemmer.stem(term.text);
				if (!stopwords.contains(s)) {
					term = new IndexTerm();
					term.bytes = null;
					term.text = "#"+s;
					term.pos = j;
					termList.add(term);	
				}

				// keyword phrase
				if (j != list.size() - 1) {
					term = new IndexTerm();
					term.pos = j;
					term.text = list.get(j).text.toLowerCase() + " "
							+ list.get(j + 1).text.toLowerCase();
					term.bytes = null;
					termList.add(term);
				}
			}
			indexBuilder.addDocument(termList);
		}
		indexBuilder.close();
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
//		KeywordIndexBuilder builder = new KeywordIndexBuilder("/shared/osprey/cqs-data/clueweb_index/index/" + args[0] + "/forward/", "/shared/osprey/cqs-data/clueweb_index/index/" + args[0] + "/");
//		builder.run();
		
		KeywordIndexBuilder builder = new KeywordIndexBuilder("./testdata/index/forward/", "./testdata/index/");
		builder.run();
		
//		IndexReader reader = IndexReader.open("./testdata/index/keyword");
//		TermPositions tp = reader.termPositions(new Term("Content", "#home"));
//		while (tp.next()){
//			for (int i = 0;i<tp.freq();i++)
//				System.out.println(tp.doc()+"\t"+tp.nextPosition());
//		}		
	}

}
