package index.builder;

import java.io.IOException;

import util.ByteList;
import util.ForwardArrayList;
import util.NumberUtil;

public class CombineForwardIndex {

	public static void main(String[] args) throws IOException {
		String indexPath = "./testdata/index/";
		if (args.length!=0)
			indexPath = args[0];
		ForwardArrayList l1 = new ForwardArrayList(indexPath + "forward/", "text", false);
		ForwardArrayList l2 = new ForwardArrayList(indexPath + "forward/", "entity_ner", false);
		ForwardArrayList l3 = new ForwardArrayList(indexPath + "forward/", "entity_mybase", false);
		ForwardArrayList nl = new ForwardArrayList(indexPath + "forward/", "combined", true);
		for (int i = 0 ;i < l1.size();i++) {
			if (i%10000==0)
				System.out.println(i);
			ByteList bl = new ByteList();
			byte[] bs;
			bs = l1.get(i);
			bl.append(NumberUtil.intToBytes(bs.length));
			bl.append(bs);
			bs = l2.get(i);
			bl.append(NumberUtil.intToBytes(bs.length));
			bl.append(bs);
			bs = l3.get(i);
			bl.append(NumberUtil.intToBytes(bs.length));
			bl.append(bs);
			nl.append(bl.toBytes());
		}
		l1.close();
		l2.close();
		l3.close();
		nl.close();
	}

}
