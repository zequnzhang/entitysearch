package index;

import java.util.ArrayList;
import java.util.Arrays;

import util.Pair;

public class TextTokenList extends ArrayList<TextToken> {
	public int[] offset;
	
	//Given startOffset and endOffset, return startPosition and length
	public Pair<Integer,Integer> getPosition(int startOffset, int endOffset){
		int sp=Arrays.binarySearch(offset, startOffset);
		int ep=Arrays.binarySearch(offset, endOffset);
		if (sp<0) sp=-(sp+1)-1;
		if (sp<0) sp=0;
		if (ep<0) ep=-(ep+1);
		return new Pair<Integer,Integer>(sp,ep-sp);
	}
	
	public String getText(int pos, int len){
		if (pos<0 || pos>=size() || pos+len>size()) return null;
		StringBuffer result=new StringBuffer();
		for (int i=0;i<len;i++){
			TextToken t=get(pos+i);
			result.append(get(pos+i).text);
			if (i!=len-1){
				for (int j=0;j<get(pos+i+1).startOffset-t.startOffset-t.text.length();j++)
					result.append(' ');
			}
		}
		return result.toString().replaceAll(" {2,}", " ");
	}
	
	public String getSeperatedText(int pos, int len){
		if (pos<0 || pos>=size() || pos+len>size()) return null;
		StringBuffer result=new StringBuffer();
		for (int i=0;i<len;i++){
			if (i!=0) result.append(" ");
			result.append(get(pos+i).text);
		}
		return result.toString();
	}
	
	public String getSeperatedText(){
		return getSeperatedText(0, size());
	}
	
	public String toString(){
		String result="";
		for (int i=0;i<size();i++){
			result+=get(i)+" | ";
		}
		return result;
	}
}
