package index;


public class IndexTextToken extends TextToken {
	public static final int ALPHANUM          = 0;
	public static final int APOSTROPHE        = 1;
	public static final int ACRONYM           = 2;
	public static final int COMPANY           = 3;
	public static final int EMAIL             = 4;
	public static final int HOST              = 5;
	public static final int NUM               = 6;
	public static final int CJ                = 7;
	public static final int SYMBOL            = 8;

	public int length;
	public int type;

	public IndexTextToken(int type, int startOffset, String text){
		super(text,startOffset);
		this.type=type;
		this.length=text.length();
	}
	
	public String toString(){
		return text;
	}
}
