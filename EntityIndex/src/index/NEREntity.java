package index;

public class NEREntity extends Entity {
	public String type;

	public NEREntity(String type, int pos, int len) {
		super(pos, len);
		this.type = type;
	}

	public String toString() {
		return type + "\t" + pos + "\t" + len;
	}
}
