package index;

public class FBEntity extends Entity {

	public FBEntityRef ref;

	public FBEntity(FBEntityRef ref, int pos, int len) {
		super(pos, len);
		this.ref = ref;
	}
	
	public String toString() {
		String r = "";
		r+=ref.entityID + "\t" + pos + "\t" + len;
		for (int i = 0; i < ref.types.length; i++)
			r += "\t"+ref.types[i];
		return r;
		
	}
}
