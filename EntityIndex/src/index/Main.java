package index;

import index.builder.ForwardNERIndexBuilder;
import index.builder.ForwardTextIndexBuilder;
import index.reader.ForwardNERIndexReader;
import index.reader.ForwardTextIndexReader;
import index.tokenizer.DoCQSTokenizer;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermPositions;

import extraction.EntityInfo;
import extraction.reader.GZIPTextReader;

import util.BufferedFileReader;
import util.ForwardArrayList;
import util.PorterStemmer;

public class Main {
	public static void buildForwardTextIndex(ArrayList<String> fileList,
			String indexPath) throws IOException, ClassNotFoundException,
			SQLException {
		ForwardTextIndexBuilder builder = new ForwardTextIndexBuilder(indexPath);
		DoCQSTokenizer tokenizer = new DoCQSTokenizer();
		for (String filename : fileList) {
			GZIPTextReader tr = new GZIPTextReader(filename);
			while (tr.nextDoc()) {
				builder.addDocument(tr.docId(), tr.url(),
						tokenizer.parseText(tr.content()));
			}
		}
		builder.close();
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, SQLException {
//		ForwardTextIndexReader reader = new ForwardTextIndexReader(
//				"./testdata/index/forward/");
//		IndexDoc doc = reader.getTokenList(4);
//		System.out.println(doc.docId + "\t" + doc.url);
//		for (int i = 0; i < doc.tokenList.size(); i++) {
//			System.out.println(i + "\t" + doc.tokenList.get(i));
//		}

//		ForwardNERIndexReader reader = new ForwardNERIndexReader(
//				"./testdata/index/forward/");
//		ArrayList<EntityInfo> l = reader.getEntityList(4);
//		for (EntityInfo info:l) {
//			System.out.println(info);
//		}

		// IndexReader reader = IndexReader.open("./testdata/index/keyword/");
		// TermPositions tp = reader.termPositions(new Term("Content",
		// "china"));
		// while (tp.next()) {
		// for (int i= 0;i<tp.freq();i++){
		// System.out.println(tp.doc()+"\t"+tp.nextPosition());
		// }
		// }

		// ArrayList<String> fileList = new ArrayList<>();
		// fileList.add("D:/workspace/program/EntityIndex/testdata/data/text/90.gz");
		// fileList.add("D:/workspace/program/EntityIndex/testdata/data/text/91.gz");
		// buildForwardTextIndex(fileList,
		// "D:/workspace/program/EntityIndex/testdata/index/forward/");

		// String config_path = null;
		// String index_path_prefix = null;
		// ArrayList<String> fileList = new ArrayList<String>();
		// BufferedFileReader reader =new BufferedFileReader(config_path +
		// args[0]);
		// String line;
		// while ((line=reader.readLine())!=null) {
		// fileList.add(line);
		// }
		// reader.close();
		// String index_path = index_path_prefix + args[0] + "/forward/";
		// if (!new File(index_path).exists()) {
		// new File(index_path).mkdirs();
		// }
		// buildForwardTextIndex(fileList, index_path + args[0] + "/forward/");
	}
}
