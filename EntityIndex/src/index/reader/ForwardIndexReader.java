package index.reader;

import index.FBEntity;
import index.FBEntityRef;
import index.FullDoc;
import index.NEREntity;
import index.TextToken;
import index.TextTokenList;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import util.BufferedFileReader;
import util.ExtStringMap;
import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;
import util.StringMap;

public class ForwardIndexReader {
	public static final String[] TYPE_LIST = new String[] { "#organization",
			"#date", "#percent", "#person", "#location", "#money", "#time",
			"#email", "#number" };

	ForwardArrayList forwardList = null;
	StringMap termMap = null;
	String path;
	ExtStringMap<Integer> docIdMap;
	ArrayList<FBEntityRef> refList;

	public ForwardIndexReader(String indexPath) throws IOException,
			ClassNotFoundException, SQLException {
		this.path = indexPath;
		forwardList = new ForwardArrayList(path , "combined", false, true);
		termMap = new StringMap("./general/term", false);
		docIdMap = new ExtStringMap<Integer>(path + "/doc_id.db", false, 40,
				"doc", "id", new Integer(0));

		refList = new ArrayList<FBEntityRef>();
		BufferedFileReader reader = new BufferedFileReader(
				"./general/frequent_entity_category");
		String line;
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t")[1].split(",");
			FBEntityRef r = new FBEntityRef();
			r.entityID = null;
			r.types = new int[ss.length];
			for (int i = 0; i < ss.length; i++)
				r.types[i] = Integer.parseInt(ss[i]);
			refList.add(r);
		}
		reader.close();
	}

	public void parseTextBytes(byte[] bytes, int startOffset, int endOffset,
			FullDoc fullDoc) {
		TextTokenList tokenList = new TextTokenList();
		fullDoc.tokenList = tokenList;
		int p = startOffset;
		int offset = 0;
		Pair<String, Integer> info;
		info = termMap.decompressString(bytes, p, 2);
		fullDoc.docId = info.first;
		p += info.second;
		info = termMap.decompressString(bytes, p, 2);
		fullDoc.url = info.first;
		p += info.second;
		while (p < endOffset) {
			info = termMap.decompressString(bytes, p, 2);
			p += info.second;
			Pair<Integer, Integer> info1 = NumberUtil.unsignedBytesToInt(bytes,
					p);
			p += info1.second;
			offset += info1.first;
			TextToken token = new TextToken(info.first, offset);
			tokenList.add(token);
		}
		tokenList.offset = new int[tokenList.size()];
		for (int i = 0; i < tokenList.size(); i++)
			tokenList.offset[i] = tokenList.get(i).startOffset;
	}

	public void parseNerBytes(byte[] bytes, int startOffset, int endOffset,
			FullDoc fullDoc) {
		int k = startOffset;
		Pair<Integer, Integer> p;

		ArrayList<NEREntity> resList = new ArrayList<>();
		fullDoc.nerEntities = resList;
		int typeId, pos = 0, len;
		while (k < endOffset) {
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			typeId = p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			pos += p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			len = p.first;
			k += p.second;
			resList.add(new NEREntity(TYPE_LIST[typeId], pos, len));
		}
	}

	public void parseMybaseBytes(byte[] bytes, int startOffset,
			int endOffset, FullDoc fullDoc) {
		int k = startOffset;
		Pair<Integer, Integer> p;
		ArrayList<FBEntity> resList = new ArrayList<>();
		fullDoc.fbEntities = resList;

		p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
		int n = p.first;
		k += p.second;
		int id, pos = 0, len;
		HashMap<Integer, FBEntityRef> refMap = new HashMap<>();
		for (int i = 0; i < n; i++) {
			p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
			id = p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			pos += p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			len = p.first;
			k += p.second;
			FBEntityRef r;
			if (id < refList.size()) {
				r = refList.get(id);
			} else {
				r = refMap.get(id - refList.size());
				if (r == null) {
					r = new FBEntityRef();
					refMap.put(id - refList.size(), r);
				}
			}
			resList.add(new FBEntity(r, pos, len));
		}

		p = NumberUtil.unsignedBytesToInt(bytes, k);
		k += p.second;
		for (int i = 0; i < refMap.size(); i++) {
			FBEntityRef r = refMap.get(i);
			r.entityID = null;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			k += p.second;
			r.types = new int[p.first];
			for (int j = 0; j < r.types.length; j++) {
				p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
				r.types[j] = p.first;
				k += p.second;
			}
		}
	}

	public FullDoc getDoc(int doc) throws IOException {
		byte[] bytes = forwardList.get(doc);
		if (bytes == null)
			return null;
		FullDoc res = new FullDoc();
		int p = 0;
		int n = NumberUtil.bytesToInt(bytes, p);
		parseTextBytes(bytes, p + 4, p + n + 4, res);
		p += n + 4;
		n = NumberUtil.bytesToInt(bytes, p);
		parseNerBytes(bytes, p + 4, p + n + 4, res);
		p += n + 4;
		n = NumberUtil.bytesToInt(bytes, p);
		parseMybaseBytes(bytes, p + 4, p + n + 4, res);
		return res;
	}

	public int docNum() throws IOException {
		return forwardList.size();
	}

	public void close() throws IOException, SQLException,
			ClassNotFoundException {
		forwardList.close();
		docIdMap.close();
	}

	public Integer getPositionByDocId(String docId) throws SQLException,
			ClassNotFoundException, IOException {
		return docIdMap.get(docId);
	}
	
	public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException {
		ForwardIndexReader reader = new ForwardIndexReader("./testdata/index/forward/");
		System.out.println(reader.docNum());
		System.out.println(reader.getDoc(43207));
		
	}

}
