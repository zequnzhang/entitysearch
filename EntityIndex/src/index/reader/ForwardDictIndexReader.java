package index.reader;

import index.FBEntity;
import java.io.IOException;
import java.util.ArrayList;

public abstract class ForwardDictIndexReader {

	public abstract ArrayList<FBEntity> getEntityList(int doc)
			throws IOException;

	public abstract int size() throws IOException;

	public abstract void close() throws IOException;
}
