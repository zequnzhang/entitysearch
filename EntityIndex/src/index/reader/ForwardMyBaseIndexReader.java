package index.reader;

import index.FBEntity;
import index.FBEntityRef;
import index.TextTokenList;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.stream.events.EntityReference;

import util.BufferedFileReader;
import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;

public class ForwardMyBaseIndexReader {

	String indexPath;
	ForwardArrayList dataList;
	ArrayList<FBEntityRef> refList;

	public ForwardMyBaseIndexReader(String indexPath) throws IOException {
		this.indexPath = indexPath;
		dataList = new ForwardArrayList(indexPath, "entity_mybase", false);
		refList = new ArrayList<FBEntityRef>();
		BufferedFileReader reader = new BufferedFileReader(
				"./general/frequent_entity_category");
		String line;
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t")[1].split(",");
			FBEntityRef r = new FBEntityRef();
			r.entityID = null;
			r.types = new int[ss.length];
			for (int i = 0; i < ss.length; i++)
				r.types[i] = Integer.parseInt(ss[i]);
			refList.add(r);
		}
		reader.close();
	}

	public ArrayList<FBEntity> getEntityList(int doc) throws IOException {
		byte[] bytes = dataList.get(doc);
		int k = 0;
		Pair<Integer, Integer> p;
		ArrayList<FBEntity> resList = new ArrayList<>();

		p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
		int n = p.first;
		k += p.second;
		int id, pos = 0, len;
		HashMap<Integer, FBEntityRef> refMap = new HashMap<>();
		for (int i = 0; i < n; i++) {
			p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
			id = p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			pos += p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			len = p.first;
			k += p.second;
			FBEntityRef r;
			if (id < refList.size()) {
				r = refList.get(id);
			} else {
				r = refMap.get(id - refList.size());
				if (r == null) {
					r = new FBEntityRef();
					refMap.put(id - refList.size(), r);
				}
			}
			resList.add(new FBEntity(r, pos, len));
		}

		p = NumberUtil.unsignedBytesToInt(bytes, k);
		k += p.second;
		for (int i = 0; i < refMap.size(); i++) {
			FBEntityRef r = refMap.get(i);
			r.entityID = null;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			k += p.second;
			r.types = new int[p.first];
			for (int j = 0; j < r.types.length; j++) {
				p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
				r.types[j] = p.first;
				k += p.second;
			}
		}
		return resList;
	}

	public int size() throws IOException {
		return dataList.size();
	}

	public void close() throws IOException {
		dataList.close();
	}

	public static void main(String[] args) throws IOException,
			ClassNotFoundException, SQLException {
		ForwardMyBaseIndexReader reader = new ForwardMyBaseIndexReader("./testdata/index/forward/");
		System.out.println(reader.getEntityList(70).size());
//		ForwardTextIndexReader textReader = new ForwardTextIndexReader(
//				"./testdata/index/forward/");
//		ForwardMyBaseIndexReader reader = new ForwardMyBaseIndexReader(
//				"./testdata/index/forward/");
//		TextTokenList tl = textReader.getTokenList(10000).tokenList;
//		for (int i = 0; i < tl.size(); i++)
//			System.out.println(i + ":\t" + tl.get(i).text);
//		ArrayList<FBEntity> l = reader.getEntityList(10000);
//		for (FBEntity e : l)
//			System.out.println(e.pos+"\t"+e.len+"\t"+tl.getSeperatedText(e.pos, e.len));
//		reader.close();
	}

}
