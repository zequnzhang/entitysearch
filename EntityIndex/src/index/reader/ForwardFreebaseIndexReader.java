package index.reader;

import index.FBEntity;
import index.FBEntityRef;
import index.TextTokenList;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;

public class ForwardFreebaseIndexReader extends ForwardDictIndexReader{

	String indexPath;
	ForwardArrayList dataList;

	public ForwardFreebaseIndexReader(String indexPath) throws IOException {
		this.indexPath = indexPath;
		dataList = new ForwardArrayList(indexPath, "entity_freebase", false);
	}

	public ArrayList<FBEntity> getEntityList(int doc) throws IOException {
		byte[] bytes = dataList.get(doc);
		int k = 0;
		Pair<Integer, Integer> p;
		ArrayList<FBEntity> resList = new ArrayList<>();

		p = NumberUtil.unsignedBytesToInt(bytes, k);
		int n = p.first;
		k += p.second;
		int typeId, pos = 0, len;
		HashMap<Integer, FBEntityRef> refMap = new HashMap<>();
		for (int i = 0; i < n; i++) {
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			typeId = p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			pos += p.first;
			k += p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			len = p.first;
			k += p.second;
			FBEntityRef r = refMap.get(typeId);
			if (r == null) {
				r = new FBEntityRef();
				refMap.put(typeId, r);
			}
			resList.add(new FBEntity(r, pos, len));
		}

		p = NumberUtil.unsignedBytesToInt(bytes, k);
		k += p.second;
		for (int i = 0; i < refMap.size(); i++) {
			FBEntityRef r = refMap.get(i);
			int t = k;
			while (bytes[t] != 0)
				t++;
			r.entityID = new String(bytes, k, t - k);
			k = t + 1;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			k += p.second;
			r.types = new int[p.first];
			for (int j = 0; j < r.types.length; j++) {
				p = NumberUtil.unsignedBytesToInt(bytes, k, 2);
				r.types[j] = p.first;
				k += p.second;
			}
		}
		return resList;
	}

	public int size() throws IOException {
		return dataList.size();
	}

	public void close() throws IOException {
		dataList.close();
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, SQLException {

	}

}
