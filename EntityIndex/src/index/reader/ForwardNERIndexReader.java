package index.reader;

import index.NEREntity;

import java.io.IOException;
import java.util.ArrayList;

import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;

public class ForwardNERIndexReader {

	String indexPath;
	ForwardArrayList dataList;
	public static final String[] TYPE_LIST = new String[] { "#organization",
			"#date", "#percent", "#person", "#location", "#money", "#time",
			"#email", "#number" };

	public ForwardNERIndexReader(String indexPath) throws IOException {
		this.indexPath = indexPath;
		dataList = new ForwardArrayList(indexPath, "entity_ner", false);
	}

	public ArrayList<NEREntity> getEntityList(int doc) throws IOException {
		byte[] bytes = dataList.get(doc);
		int k = 0;
		Pair<Integer, Integer> p;
		
		ArrayList<NEREntity> resList = new ArrayList<>();
		int typeId, pos = 0, len;
		while (k < bytes.length) {
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			typeId = p.first;		
			k+=p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			pos += p.first;
			k+=p.second;
			p = NumberUtil.unsignedBytesToInt(bytes, k);
			len = p.first;
			k+=p.second;			
			resList.add(new NEREntity(TYPE_LIST[typeId], pos, len));			
		}
		return resList;
	}
	
	public int size() throws IOException {
		return dataList.size();
	}
	
	public void close() throws IOException {
		dataList.close();
	}

}
