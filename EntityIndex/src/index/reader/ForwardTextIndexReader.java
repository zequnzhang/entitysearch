package index.reader;

import index.IndexDoc;
import index.TextToken;
import index.TextTokenList;
import java.io.IOException;
import java.sql.SQLException;

import util.ExtStringMap;
import util.ForwardArrayList;
import util.NumberUtil;
import util.Pair;
import util.StringMap;

public class ForwardTextIndexReader {
	
	// This reader does not obtains 

	ForwardArrayList forwardList = null;
	StringMap termMap = null;
	String path;
	ExtStringMap<Integer> docIdMap;

	public ForwardTextIndexReader(String indexPath) throws IOException, ClassNotFoundException, SQLException {
		this.path = indexPath;
		forwardList = new ForwardArrayList(path, "text", false);
		termMap = new StringMap("./general/term", false);
		docIdMap = new ExtStringMap<Integer>(path + "/doc_id.db", false, 40, "doc", "id", new Integer(0));
	}

	public IndexDoc getTokenList(int doc) throws IOException {
		IndexDoc res = new IndexDoc();
		TextTokenList tokenList = new TextTokenList();
		res.tokenList = tokenList;
		byte[] bytes = forwardList.get(doc);
		if (bytes == null)
			return null;
		int p = 0;
		int offset = 0;
		Pair<String, Integer> info;
		info = termMap.decompressString(bytes, p, 2);
		res.docId = info.first;
		p += info.second;
		info = termMap.decompressString(bytes, p, 2);
		res.url = info.first;
		p += info.second;
		while (p < bytes.length) {
			info = termMap.decompressString(bytes, p, 2);
			p += info.second;
			Pair<Integer, Integer> info1 = NumberUtil.unsignedBytesToInt(bytes,
					p);
			p += info1.second;
			offset += info1.first;
			TextToken token = new TextToken(info.first, offset);
			tokenList.add(token);
		}
		tokenList.offset = new int[tokenList.size()];
		for (int i = 0; i < tokenList.size(); i++)
			tokenList.offset[i] = tokenList.get(i).startOffset;
		return res;
	}

	public int docNum() throws IOException {
		return forwardList.size();
	}
	
	public void close() throws IOException, SQLException, ClassNotFoundException {
		forwardList.close();
		docIdMap.close();
	}
	
	public Integer getPositionByDocId (String docId) throws SQLException, ClassNotFoundException, IOException{
		return docIdMap.get(docId);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
		ForwardTextIndexReader reader = new ForwardTextIndexReader("./testdata/index/"); 
		TextTokenList tl = reader.getTokenList(5855).tokenList;
		System.out.println(tl);
	}

}
