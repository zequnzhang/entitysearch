package index;

import java.io.Serializable;

public class TextToken implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6525238789773709757L;
	public String text;
	public int startOffset;
	public TextToken(String text, int startOffset) {
		super();
		this.text = text;
		this.startOffset = startOffset;
	}
	public String toString(){
		return text+" , "+startOffset;
	}
}
