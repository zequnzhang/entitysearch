package extraction;

public class Entity implements Comparable<Entity> {
	public String type;
	public int startOffset;
	public int endOffset;

	public Entity(String type, int startOffset, int endOffset) {
		this.type = type;
		this.startOffset = startOffset;
		this.endOffset = endOffset;
	}

	public Entity(Entity e) {
		this.type = e.type;
		this.startOffset = e.startOffset;
		this.endOffset = e.endOffset;
	}

	public String toString() {
		return "[" + type + "," + startOffset + "," + endOffset + "]";
	}

	public int compareTo(Entity e) {
		if (e.startOffset != startOffset)
			return startOffset - e.startOffset;
		return endOffset - e.endOffset;
	}
}
