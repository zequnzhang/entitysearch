package extraction.number;
import extraction.Entity;
%%
%class NumberExtractorImpl
%unicode
%pack
%char
%caseless
%function getNextToken
%type Entity

%{
  static final String NUMBER_TAG="number";
%}

WHITE_SPACE			=	([\t\f]|" ")+
DIGITAL_NUMBER		=	[0-9]+(("."|",")[0-9]+)?(","[0-9]+)?
PRE_WORD_NUMBER		=	"one"|"two"|"three"|"four"|"five"|"six"|"seven"|"eight"|"nine"|"ten"|"eleven"|"twelve"|"thirteen"|"fourteen"|"fifteen"|"sixteen"|"seventeen"|"eighteen"|"nineteen"|"twenty"|"thirty"|"forty"|"fifty"|"sixty"|"seventy"|"eighty"|"ninety"
NON_PRE_WORD_NUMBER	=	"hundred"|"thousand"|"million"|"billion"|"trillion"
SINGLE_NUMBER		=	{DIGITAL_NUMBER}|{PRE_WORD_NUMBER}
WORD_NUMBER			=	{PRE_WORD_NUMBER}|{NON_PRE_WORD_NUMBER}
P					=	"-"|{WHITE_SPACE}|({WHITE_SPACE}"and"{WHITE_SPACE})
NUMBER_PHRASE		=	{SINGLE_NUMBER}({P}{WORD_NUMBER})*

%%
<YYINITIAL>	{
{NUMBER_PHRASE}		{	return new Entity(NUMBER_TAG,yychar,yychar+yytext().length());	}
.					{	}
}
