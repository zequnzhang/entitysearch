package extraction.number;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;

import extraction.Entity;
import extraction.EntityExtractor;

public class NumberExtractor extends EntityExtractor{
	NumberExtractorImpl eximpl;
	public NumberExtractor(){
		eximpl=new NumberExtractorImpl(new StringReader(""));
	}
	
	public ArrayList<Entity> extractEntities(String text) throws IOException {
		ArrayList<Entity> entityList=new ArrayList<Entity>();
		eximpl.yyreset(new StringReader(text));
		Entity e;
		while ((e=eximpl.getNextToken())!=null)
			entityList.add(e);
		System.out.println("..."+entityList.size());
		return entityList;
	}

	public String[] getEntityTypes() {
		return new String[]{"number"};
	}
	
	public static void main(String[] args) throws IOException{
//		ArrayList<Entity> e=new NumberExtractor().extractEntities("Centurionwater purge and trap autosampler. ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ Scientific Equipment Source 1895 Clements Rd., Unit 127 Pickering, ON, L1W 3V5 Canada Tel: 905-231-0930 Fax: 905-231-0293 ~ \") ~ ~ ~ ~ ~ ~   ~ ~ ~ ~ ~~ ~~ NEW           Centurionwater purge and trap autosampler. Scientific Equipment Source  now carries the NEW Centurionwater purge           and trap autosampler. The Centurion was developed by EST to give           maximum sampling flexibility at an affordable price. Customers wanted           more reliability, internal standard programmability, high sample           throughput solutions and an easy to us interface. The Centurion delivers on all of these and more. Features and Benefits 100 40ml VOA vial sample positions Easy to use WindowsCE operating system Dual concentrator mode for maximum sample throughput Processes and delivers samples to two different               concentrators on a single GC or even two separate GCs Samples can be injected into a single GC every 13 minutes or               at the rate of the GC Cycle time, whichever is greater Up to three individually programmable internal standards Hot water rinse cycle of entire sample pathway for improved               data quality Rugged and dependable X-Y-Z platform without sample vial               handling Particulate laden samples undisturbed Eliminates downtime caused by &quot;lost vial&quot; errors Fiber optic vial sensor 30 method storage capability Foam detected Rinse Cycle (Requires Encon purge and trap               concentrator) When the Encon detects a foamed sample, the Centurion can be           programmed to perform a series of extra hot water rinse cycles to           ensure that the system is clean and ready to continue. Interfaces to any purge and trap concentrator Auto correct feature to continue sequence when errors are               encountered Electronic error log to track and record errors On-board help Menus including flow diagrams and               troubleshooting charts Easy to use Input/Output Control Panels for troubleshooting Easy access Plumbing Module Door for quick removal of sample               loop Priority sample Optional dilution capability Optional vial cooling Serviced and supported nationally by EST sister company,               Professional Technical Services (PTS) and in Canada by Infinity               Instruments and Scientific Equipment Source . There were two major principles behind the development of the           Centurion autosampler. One, it had to be the most robust, reliable           system on the market and; two, it had to outperform any other           autosampler in both functionality and analytical performance. The           Centurion redefines automation and productivity. Scientific Equipment Source  can help you with your purge and trap           autosampler and concentrator needs including installation, training,           warranty repair and after sales technical support. SES also offers a           variety of purchase options including leasing and rent of equipment. So weather your looking for a complete instrument, parts support or           service call Techlabs today and let us show you how we can help. &nbsp; ~~ ~~ ~~ ~ ~ ~ &nbsp; If you are a bargain hunter please give us a call, we always have instruments in stock, which have not been refurbished and can be sold as is in working condition. Scientific Equipment Source provides service and training on site in most of central Ontario this includes neighboring cities Ajax, Whitby, Pickering, Oshawa, Toronto, Mississauga. We can also provide training and courses on site on chromatograph (GC), HPLC in the Durham region; this includes Pickering, Ajax, Whitby, Oshawa. For other customers we can be also flexible on arrangements. For the rest of customer any equipment for repair has to be sent back by a courier.");
//		for (int i=0;i<e.size();i++){
//			System.out.println(e.get(i));
//		}
	}
}
