package extraction.reader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.zip.GZIPInputStream;

public class RawTextReader {
	BufferedInputStream reader;
	public String url;
	public String docId;
	static public byte[] content = new byte[1000000];
	public int docLen;
	
	public RawTextReader(String fileName) throws IOException {
		reader = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fileName)));
	}
	
	private String readLine() throws IOException {
		StringBuffer buf = new StringBuffer();
		int v;
		while ((v=reader.read())!=-1) {
			if (v=='\n') break;
			buf.append((char)v);			
		}
		if (v==-1 && buf.length()==0) return null;
		return buf.toString();
	}

	public boolean nextDoc() throws IOException {
		if (reader == null)
			return false;
		String line;
		url = null;
		docId = null;
		while ((line=readLine()) != null) {
			if (line.startsWith("WARC-TREC-ID:")) {
				docId = line.substring(14);
			}
			if (line.startsWith("WARC-Target-URI:"))
				url = line.substring(17);
			if (line.startsWith("WARC-Identified-Payload-Type:"))
				break;
		}
		if (docId == null) {
			reader.close();
			return false;
		}
		line = readLine();
		docLen = Integer.parseInt(line.split(" ")[1]);
		reader.read();
		reader.read(content, 0, docLen);
		if (url == null)
			url = "";
		return true;
	}

	public static void main(String[] args) throws IOException {
	}
}
