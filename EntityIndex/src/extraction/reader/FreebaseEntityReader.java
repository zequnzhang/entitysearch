package extraction.reader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

import extraction.EntityInfo;
import extraction.RawFBEntity;

public class FreebaseEntityReader {
	String docId;
	ArrayList<RawFBEntity> entityList;
	BufferedReader reader;
	String line;

	public FreebaseEntityReader(String fileName) throws IOException {
		reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(
				new FileInputStream(fileName))));
		line = reader.readLine();
		entityList = new ArrayList<>();
	}

	public boolean nextDoc() throws IOException {
		docId = null;
		entityList.clear();
		if (line == null)
			return false;
		docId = line;
		while ((line = reader.readLine()) != null) {
			if (!line.startsWith("\t"))
				break;
			RawFBEntity e = new RawFBEntity();
			String[] ss = line.split("\t");
			e.start = Integer.parseInt(ss[1]);
			e.end = Integer.parseInt(ss[2]);
			e.eid = ss[3];
			e.entity = ss[4];
			entityList.add(e);
		}
		return true;
	}

	public String docId() {
		return docId;
	}

	public ArrayList<RawFBEntity> entityList() {
		return entityList;
	}

}
