package extraction.reader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.zip.GZIPInputStream;

import extraction.Entity;
import extraction.EntityInfo;
import extraction.RawFBEntity;

public class RawEntityReader {

	BufferedReader reader;
	private String nextDoc, nextEntity, nextEid;
	private int nextStart, nextEnd;

	public String doc;
	public ArrayList<RawFBEntity> infoList;

	public RawEntityReader(String file) throws IOException {
		reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(
				new FileInputStream(file)), "utf-8"));
		infoList = new ArrayList<>();
		readLine();
	}

	private void readLine() throws IOException {
		if (reader == null)
			return;
		String line;
		line = reader.readLine();
		if (line == null) {
			nextDoc = null;
			nextEntity = null;
			nextEid = null;
			nextStart = -1;
			nextEnd = -1;
			reader.close();
			reader=null;
		} else {
			String[] ss = line.split("\t");
			nextDoc = ss[0];
			nextEntity = ss[2];
			nextEid = ss[7];
			nextStart = Integer.parseInt(ss[3]);
			nextEnd = Integer.parseInt(ss[4]);
		}
	}

	public boolean nextDoc() throws IOException {
		if (nextDoc == null)
			return false;
		infoList.clear();
		doc = nextDoc;
		while (nextDoc != null) {
			RawFBEntity e = new RawFBEntity();
			e.entity = nextEntity;
			e.start = nextStart;
			e.end = nextEnd;
			e.eid = nextEid;
			infoList.add(e);
			readLine();
			if (!doc.equals(nextDoc))
				break;
		}
		Collections.sort(infoList, new Comparator<RawFBEntity>() {
			@Override
			public int compare(RawFBEntity o1, RawFBEntity o2) {
				return o1.start-o2.start;
			}
		});
		return true;
	}
	
	public static void main(String[] args) throws IOException {
		RawEntityReader reader = new RawEntityReader("./testdata/data/entity/04.anns.tsv.gz");
		while (reader.nextDoc()) {
			System.out.println(reader.doc);
		}
	}

}
