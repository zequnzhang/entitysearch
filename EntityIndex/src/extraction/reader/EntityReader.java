package extraction.reader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

import extraction.Entity;
import extraction.EntityInfo;

public class EntityReader {
	
	String line;
	BufferedReader reader;
	ArrayList<Entity> entityList;
	String docId;
	
	public EntityReader(String fileName) throws IOException {
		reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(fileName))));
		line = reader.readLine();
		docId = null;		
		entityList=new ArrayList<Entity>();
	}
	
	public boolean nextDoc() throws IOException {
		docId = null;
		entityList.clear();
		if (line == null || line.length()==0) return false;
		docId = line;
		while ((line=reader.readLine())!=null) {
			if (!line.startsWith("\t")) break;
			String[] ss = line.split("\t");
			Entity e = new Entity(ss[3], Integer.parseInt(ss[1]), Integer.parseInt(ss[2]));
			entityList.add(e);
		}
		return true;
	}
	
	public String doc() {
		return docId;
	}
	
	public ArrayList<Entity> entities() {
		return entityList;
	}

	public static void main(String[] args) {

	}

}
