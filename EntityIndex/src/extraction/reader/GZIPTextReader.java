package extraction.reader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.zip.GZIPInputStream;

import util.BufferedFileReader;

//FIXME Something Wrong for This Class. It returns Fewer Documents...
public class GZIPTextReader {
	String fileName;
	BufferedReader reader;
	String line;
	String docId, url, content;

	public GZIPTextReader(String fileName) throws IOException {
		this.fileName = fileName;
		reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(
				new FileInputStream(fileName))));
	}

	public boolean nextDoc() throws IOException {
		if (reader == null)
			return false;
		url = null;
		content = null;
		docId = null;
		line = reader.readLine();
		if (line != null) {
			String[] ss = line.split("\t");
			if (ss.length == 2) {
				// Might not equals to 2....
				docId = ss[0];
				url = ss[1];
				content = reader.readLine();
			}
		}
		if (docId == null) {
			reader.close();
			reader = null;
			return false;
		}
		return true;
	}

	public String url() {
		return url;
	}

	public String content() {
		return content;
	}

	public String docId() {
		return docId;
	}

	public static void main(String[] args) throws IOException {
	}
}
