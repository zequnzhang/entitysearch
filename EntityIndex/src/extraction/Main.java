package extraction;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.zip.GZIPOutputStream;

import util.TrieReader;

import extraction.email.EmailExtractor;
import extraction.number.NumberExtractor;
import extraction.reader.GZIPTextReader;

public class Main {

	public static void main(String[] args) throws IOException {
		TrieReader r1 = new TrieReader("D:/workspace/program/EntityIndex/testdata/data/general/freebase_entity_type_trie");
		TrieReader r2 = new TrieReader("D:/workspace/program/EntityIndex/testdata/data/general/mybase_trie");
		System.out.println("Done!");
		
//		String srcFile = args[0];
//		String emailTarFile = srcFile.replaceAll("text", "email");
//		String numberTarFile = srcFile.replaceAll("text", "number");
//		String tarPath = emailTarFile.substring(0, emailTarFile.lastIndexOf('/'));
//		if (!new File(tarPath).exists())
//			new File(tarPath).mkdirs();
//		tarPath = numberTarFile.substring(0, numberTarFile.lastIndexOf('/'));
//		if (!new File(tarPath).exists())
//			new File(tarPath).mkdirs();
//		
//		GZIPTextReader textReader = new GZIPTextReader(srcFile);		
//		PrintStream emailOutput = new PrintStream(new GZIPOutputStream(
//				new BufferedOutputStream(new FileOutputStream(emailTarFile))));
//		PrintStream numberOutput = new PrintStream(new GZIPOutputStream(
//				new BufferedOutputStream(new FileOutputStream(numberTarFile))));
//		EmailExtractor emailExtractor = new EmailExtractor();
//		NumberExtractor numberExtractor = new NumberExtractor();
//		while (textReader.nextDoc()) {
//			System.out.println(textReader.docId());
//			ArrayList<Entity> entityList = emailExtractor.extractEntities(textReader.content());
//			emailOutput.println(textReader.docId());
//			for (Entity e:entityList) {
//				emailOutput.println("\t"+e.startOffset+"\t"+e.endOffset+"\t#"+e.type+"\t"+textReader.content().substring(e.startOffset, e.endOffset));
//			}
//			
//			entityList = numberExtractor.extractEntities(textReader.content());
//			numberOutput.println(textReader.docId());
//			for (Entity e:entityList) {
//				numberOutput.println("\t"+e.startOffset+"\t"+e.endOffset+"\t#"+e.type+"\t"+textReader.content().substring(e.startOffset, e.endOffset));
//			}
//		}
//		emailOutput.close();
//		numberOutput.close();
		
//		String path = "./testdata/data/text/04.gz";
//		System.out.println(path.substring(0, path.lastIndexOf('/')));
		
//		String srcFile, tarPath;
//		
//		GZIPTextReader textReader = new GZIPTextReader("./testdata/data/text/04.gz");
//		NERExtractor extractor = new NERExtractor();
//		while (textReader.nextDoc()) {
//			ArrayList<Entity> entityList = extractor.extractEntities(textReader.content);
//			for (Entity e:entityList){
//				System.out.println(e+":"+textReader.content.substring(e.startOffset, e.endOffset));
//			}
//		}
//		String textPath = "./testdata/data/text/";
//		String entityPath = "./testdata/data/entity/";
//		File[] files = new File(textPath).listFiles();
//		for (int i = 0; i < files.length;i++) {
//			String filename = files[i].getName();
//			if (!filename.endsWith("gz")) continue;
//			System.out.println(filename);
//			GZIPTextReader textReader = new GZIPTextReader(textPath + filename);
//			FreebaseEntityReader entityReader = new FreebaseEntityReader(entityPath + filename);
//			while (textReader.nextDoc()) {
//				entityReader.nextDoc();
//				System.out.println(textReader.docId+"\t"+textReader.url + "\t"+entityReader.entityList.size());
//				for (EntityInfo e: entityReader.entityList) {
//					System.out.println("\t"+e.entity  +"\t"+textReader.content.substring(e.start,e.end));
//				}
//			}
//		}
	}

}
