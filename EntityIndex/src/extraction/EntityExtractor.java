package extraction;

import java.io.IOException;
import java.util.ArrayList;

public abstract class EntityExtractor {
	public abstract ArrayList<Entity> extractEntities(String text) throws IOException;
	public abstract String[] getEntityTypes();
}
