package extraction;
import java.io.IOException;
import java.io.Serializable;

public class EntityInfo implements Comparable<EntityInfo> , Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -301567188434986429L;
	public int pos;
	public int len;
	public String type;
	public int typeId;
	public String val;
	public EntityInfo(int typeId, String type, int pos, int len, String val){
		this.pos=pos;
		this.len=len;
		this.typeId=typeId;
		this.type = type;
		this.val=val;
	}	
	public String toString(){
		String s=type+","+pos+","+len+"("+val+")";
		return s;
	}
	
	public int compareTo(EntityInfo o) {
		if (pos!=o.pos) return pos-o.pos;
		if (len!=o.len) return len-o.len;
		if (typeId!=o.typeId) return typeId-o.typeId;
		return 0;
	}	
	
}