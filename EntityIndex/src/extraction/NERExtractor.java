package extraction;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetEndAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.AnswerAnnotation;
import extraction.Entity;
import extraction.EntityExtractor;
import extraction.reader.GZIPTextReader;

public class NERExtractor extends EntityExtractor{
	final static String PERSON_TAG="person";
	final static String LOCATION_TAG="location";
	final static String ORGANIZATION_TAG="organization";
	final static String TIME_TAG = "time";
	final static String MONEY_TAG = "money";
	final static String PERCENT_TAG = "percent";
	final static String DATE_TAG = "date";
	
	String serializedClassifier = "./lib/english.muc.7class.distsim.crf.ser.gz";
	AbstractSequenceClassifier classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
	public NERExtractor(){
		
	}
	
	public ArrayList<Entity> extractEntities(String text) {
		ArrayList<Entity> result=new ArrayList<Entity>();
		List<List<CoreLabel>> labels=classifier.classify(text);
		for (List<CoreLabel> sentences:labels){
			Entity pre=null;
			for (CoreLabel label:sentences){
				String type=typeMapping(label);
				if (pre!=null){
					if (type==pre.type)
						pre.endOffset=label.get(CharacterOffsetEndAnnotation.class);
					else{
						result.add(pre);
						pre=getEntity(label);
					}
				}
				else{
					pre=getEntity(label);							
				}						
			}
		}		
		return result;
	}
	private String typeMapping(CoreLabel label){
		String t=label.getString(AnswerAnnotation.class);
		if (t.equals("LOCATION")) return LOCATION_TAG;
		if (t.equals("ORGANIZATION")) return ORGANIZATION_TAG;
		if (t.equals("PERSON")) return PERSON_TAG;
		if (t.equals("TIME")) return TIME_TAG;
		if (t.equals("MONEY")) return MONEY_TAG;
		if (t.equals("PERCENT")) return PERCENT_TAG;
		if (t.equals("DATE")) return DATE_TAG;
		return null;
	}
	private Entity getEntity(CoreLabel label){
		String type=typeMapping(label);
		if (type==null) return null;
		Entity entity=new Entity(type, label.get(CharacterOffsetBeginAnnotation.class), label.get(CharacterOffsetEndAnnotation.class));
		return entity;
	}

	public String[] getEntityTypes() {
		return new String[]{LOCATION_TAG, ORGANIZATION_TAG, PERSON_TAG, TIME_TAG, MONEY_TAG, PERCENT_TAG, DATE_TAG};
	}
	
	public static void main(String[] args) throws IOException {
		String srcFile = args[0];
		String tarFile = srcFile.replaceAll("text", "ner");
		String tarPath = tarFile.substring(0, srcFile.lastIndexOf('/'));
		
		GZIPTextReader textReader = new GZIPTextReader(srcFile);
		if (!new File(tarPath).exists())
			new File(tarPath).mkdirs();
		
		PrintStream entityOutput = new PrintStream(new GZIPOutputStream(
				new BufferedOutputStream(new FileOutputStream(tarFile))));		
		NERExtractor extractor = new NERExtractor();
		while (textReader.nextDoc()) {
			System.out.println(textReader.docId());
			ArrayList<Entity> entityList = extractor.extractEntities(textReader.content());
			entityOutput.println(textReader.docId());
			for (Entity e:entityList) {
				entityOutput.println("\t"+e.startOffset+"\t"+e.endOffset+"\t#"+e.type+"\t"+textReader.content().substring(e.startOffset, e.endOffset));
			}
		}
		entityOutput.close();
	}
}
