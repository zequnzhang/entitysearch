package extraction.email;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;

import extraction.Entity;
import extraction.EntityExtractor;

public class EmailExtractor extends EntityExtractor{
	EmailExtractorImpl eximpl;
	public EmailExtractor(){
		eximpl=new EmailExtractorImpl(new StringReader(""));
	}
	
	public ArrayList<Entity> extractEntities(String text) throws IOException {
		ArrayList<Entity> entityList=new ArrayList<Entity>();
		eximpl.yyreset(new StringReader(text));
		Entity e;
		while ((e=eximpl.getNextToken())!=null)
			entityList.add(e);
		return entityList;
	}

	public String[] getEntityTypes() {
		return new String[]{"email"};
	}
}
