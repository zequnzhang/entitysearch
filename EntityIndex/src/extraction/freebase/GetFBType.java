package extraction.freebase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import util.BufferedFileReader;
import util.BufferedPrintStream;
import util.TrieReader;

public class GetFBType {

	public static String processData(ArrayList<String> cats, TrieReader tr) {
		HashSet<Integer> idSet = new HashSet<Integer>();
		for (String cat : cats) {
			String s = tr.get(cat);
			if (s == null)
				continue;
			String[] v = s.split("\t");
			for (int i = 0; i < v.length; i++)
				idSet.add(Integer.parseInt(v[i]));
		}
		ArrayList<Integer> idList = new ArrayList<>();
		idList.addAll(idSet);
		Collections.sort(idList);
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < idList.size(); i++) {
			if (i != 0)
				buf.append(",");
			buf.append(idList.get(i));
		}
		return buf.toString();
	}

	public static void main(String[] args) throws IOException {
		
		String triePath = "./testdata/data/general/freebase_entity_type_trie";
		String oldMyBase = "./testdata/data/general/mybase";
		String newMyBase = "./testdata/data/general/mybase_filtered";
		
		TrieReader tr = new TrieReader(triePath);

		HashSet<String> filteredEntity = new HashSet<String>();
		BufferedFileReader reader = new BufferedFileReader(
				"./testdata/filtered_entity");
		String line;
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t");
			filteredEntity.add(ss[0]);
		}
		reader.close();

		int k = 0;
		reader = new BufferedFileReader(oldMyBase);
		BufferedPrintStream output = new BufferedPrintStream(newMyBase);
		String cat = "";
		ArrayList<String> cats = new ArrayList<String>();
		while ((line = reader.readLine()) != null) {
			k++;
			if (k % 100000 == 0)
				System.out.println(k);
			String[] ss = line.split("\t");
			if (ss[0].equals(cat)) {
				cats.add(ss[1]);
			} else {
				if (!filteredEntity.contains(cat)) {
					String typeStr = processData(cats, tr);
					if (!typeStr.equals(""))
						output.println(cat + "\t" + typeStr);
				}
				cat = ss[0];
				cats.clear();
				cats.add(ss[1]);
			}
		}
		if (!filteredEntity.contains(cat)) {
			String typeStr = processData(cats, tr);
			if (!typeStr.equals(""))
				output.println(cat + "\t" + typeStr);
		}
		output.close();
		reader.close();

	}

}
