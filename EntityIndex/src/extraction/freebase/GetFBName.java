package extraction.freebase;

import index.TextTokenList;
import index.tokenizer.DoCQSTokenizer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import util.BufferedFileReader;
import util.BufferedPrintStream;

public class GetFBName {

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		BufferedPrintStream output = new BufferedPrintStream(
				"./testdata/name2id4extract");
		String[] files = new String[] { "./testdata/5.gz", "./testdata/37.gz" };
		
		DoCQSTokenizer tokenizer = new DoCQSTokenizer();
		for (String fileName : files) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new GZIPInputStream(new FileInputStream(fileName))));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] ss = line.split("\t");
				String id = ss[0].substring(
						"<http://rdf.freebase.com/ns/".length(),
						ss[0].length() - 1);
				String name = ss[2];
				ss = name.split("@");
				if (ss.length != 2)
					continue;
				if (!ss[1].equals("en") && !ss[1].equals("en-US")
						&& !ss[1].equals("en-GB"))
					continue;
				name = ss[0].substring(1, ss[0].length() - 1);
				
				if (!id.startsWith("m."))
					continue;
				if (name.startsWith("http"))
					continue;
				name = name.replaceAll("\\\\\"", "\"");
				StringBuffer buf = new StringBuffer();
				boolean p = true, t = true;
				for (int i = 0 ; i < name.length();i++) {
					if (!(name.charAt(i) >= 32 && name.charAt(i) <= 126)) {
						p = false;
						break;												
					}
					if (name.charAt(i)== '(')
						t = false;
					if (t)
						buf.append(name.charAt(i));
					if (name.charAt(i)== ')')
						t = true;
				}
				if (!p) continue;
				TextTokenList tl = tokenizer.parseText(buf.toString()); 
				if (tl.size() == 0 || tl.size()>6) 
					continue;
				name = tl.getSeperatedText();
				if (name == null)
					continue;
				int ln = 0;
				name = name.toLowerCase();
				for (int i = 0 ;i <name.length();i++) {
					if (name.charAt(i)>='a' && name.charAt(i)<='z')
						ln++;
				}
				if (ln == 0)
					continue;
				output.println(name + "\t" + id);
			}
			reader.close();
		}
		output.close();
	}
}
