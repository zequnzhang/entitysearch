package extraction.freebase;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import edu.stanford.nlp.util.Comparators;

import util.BufferedFileReader;
import util.BufferedPrintStream;

public class SortTest {
	public static void main(String[] args) throws IOException {
		String path = "/shared/osprey/cqs-data/clueweb_index/data/";
//		String path = "./testdata/";
		byte[][] storage = new byte[36500000][];
		BufferedFileReader reader = new BufferedFileReader(path + "mybase");
		BufferedPrintStream output = new BufferedPrintStream(path + "mybase_ordered");
		String line;
		int k = 0;
		while ((line = reader.readLine()) != null) {
			storage[k] = line.getBytes();
			k++;
			if (k % 100000 == 0)
				System.out.println(k);
		}
		reader.close();
		System.out.println("Ordering ... ");
		Arrays.sort(storage, new Comparator<byte[]>() {
			@Override
			public int compare(byte[] arg0, byte[] arg1) {
				if (arg0 == null && arg1 == null)
					return 0;
				if (arg0 == null)
					return -1;
				if (arg1 == null)
					return 1;
				int i = 0;
				for (i = 0; i < arg0.length && i < arg1.length; i++) {
					if (arg0[i] < arg1[i])
						return -1;
					if (arg0[i] > arg1[i])
						return 1;
				}
				if (arg0.length < arg1.length)
					return -1;
				if (arg0.length > arg1.length)
					return 1;
				return 0;
			}
		});
		for (int i = 0; i < storage.length; i++) {
			if (storage[i] == null)
				continue;
			output.println(new String(storage[i]));
		}
		output.close();
	}
}
