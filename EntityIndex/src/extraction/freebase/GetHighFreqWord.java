package extraction.freebase;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import util.BufferedFileReader;
import util.BufferedPrintStream;
import util.PairList;

public class GetHighFreqWord {
	
	public static void getFreqWord() throws IOException {
		BufferedFileReader r = new BufferedFileReader("./general/term_df");
		BufferedPrintStream o = new BufferedPrintStream("./general/very_frequent_word");
		r.readLine();
		String line;
		while ((line=r.readLine())!=null) {
			String[] ss = line.split("\t");
			if (!ss[0].startsWith("#") && Integer.parseInt(ss[1])>10000) 
				o.println(line);			
		}
		r.close();		
		o.close();		
	}

	public static void main(String[] args) throws IOException {
		BufferedFileReader r = new BufferedFileReader("./general/very_frequent_word");
		r.readLine();
		String line;
		HashMap<String, Integer> freqMap = new HashMap<>();
		while ((line=r.readLine())!=null) {
			String[] ss = line.split("\t");
			freqMap.put(ss[0], Integer.parseInt(ss[1]));
		}
		r.close();
		
		r = new BufferedFileReader("./testdata/filtered_entity");
		HashSet<String> filteredEntity = new HashSet<String>();
		while ((line=r.readLine())!=null) {
			String[] ss = line.split("\t");
			filteredEntity.add(ss[0]);
		}
		r.close();
		
		HashSet<String> used = new HashSet<String>();		
		r = new BufferedFileReader("./testdata/data/general/mybase_raw");
		while ((line=r.readLine())!=null) {
			String[] ss = line.split("\t");
			if (freqMap.containsKey(ss[0]) && !filteredEntity.contains(ss[0]))
				used.add(ss[0]);
		}
		r.close();
		
		PairList<String, Integer> plist = new PairList<>();
		for (String s:used){
			plist.add(s, freqMap.get(s));
		}
		plist.sort(PairList.SECOND_DESC);
		
		BufferedPrintStream o = new BufferedPrintStream("./testdata/frequent_entity");
		for (int i = 0 ;i<30000 && i < plist.size();i++) {
			o.println(plist.getFirst(i));
		}
		o.close();
	}

}
