package extraction;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import extraction.reader.RawEntityReader;
import extraction.reader.RawTextReader;

public class ExtractFreebaseEntity {

	String srcTextPath, srcEntityPath, tarTextPath, tarEntityPath;

	public ExtractFreebaseEntity(String srcTextPath, String srcEntityPath,
			String tarTextPath, String tarEntityPath) {
		this.srcTextPath = srcTextPath;
		this.srcEntityPath = srcEntityPath;
		this.tarTextPath = tarTextPath;
		new File(tarTextPath).mkdirs();
		this.tarEntityPath = tarEntityPath;
		new File(tarEntityPath).mkdirs();
	}

	private boolean check(int k, int len) {
		return k >= 0 && k < len;
	}

	private int adjustOffset(byte[] str, int len, int startOffset, int endOffset) {
		int[] offsets = new int[] { 0, -1, 1, -2, 2, -3, 3, -4, 4, -5, 5, -6, 6 };
		int minp = Integer.MAX_VALUE;
		int mink = Integer.MAX_VALUE;
		for (int i = 0; i < offsets.length; i++) {
			int offset = offsets[i];
			int ns = startOffset + offset, ne = endOffset + offset;
			if (ns < 0 || ne > len)
				continue;
			int p = 0;
			if (check(ns - 1, len) && Character.isLetter(str[ns - 1]))
				p++;
			if (check(ns, len) && str[ns] == ' ')
				p++;
			if (check(ne, len) && Character.isLetter(str[ne]))
				p++;
			if (check(ne - 1, len) && str[ne - 1] == ' ')
				p++;
			if (p < minp) {
				minp = p;
				mink = offset;
			}
		}
		return mink;
	}

	public void process() throws IOException {
		Pattern p = Pattern
				.compile("\\[zmwzmw_beg_([^\\]]*)\\](.*?)\\[zmwzmw_end_([^\\]]*)\\]");

		File[] files = new File(srcTextPath).listFiles();
		for (int i = 0; i < files.length; i++) {
			String filename = files[i].getName();
			if (!filename.endsWith("gz"))
				continue;
			RawTextReader textReader = new RawTextReader(srcTextPath + filename);
			String fid = filename.split("\\.")[0];
			RawEntityReader entityReader = new RawEntityReader(srcEntityPath
					+ fid + ".anns.tsv.gz");
			PrintStream textOutput = new PrintStream(new GZIPOutputStream(
					new BufferedOutputStream(new FileOutputStream(tarTextPath
							+ fid + ".gz"))));
			PrintStream entityOutput = new PrintStream(new GZIPOutputStream(
					new BufferedOutputStream(new FileOutputStream(tarEntityPath
							+ fid + ".gz"))));

			StringBuffer buf;
			int pos;

			entityReader.nextDoc();
			while (textReader.nextDoc()) {
				String content = new String(textReader.content, 0,
						textReader.docLen);

				if (textReader.docId.equals(entityReader.doc)) {
					System.out.println(textReader.docId);
					buf = new StringBuffer();
					pos = 0;
					for (RawFBEntity e : entityReader.infoList) {
						int o = adjustOffset(textReader.content,
								textReader.docLen, e.start, e.end);
						e.start += o;
						e.end += o;
						if (!(e.start >= pos && e.end <= textReader.docLen))
							continue;
						buf.append(new String(textReader.content, pos, e.start
								- pos));
						buf.append("[zmwzmw_beg_" + e.eid + "]");
						buf.append(new String(textReader.content, e.start,
								e.end - e.start));
						buf.append("[zmwzmw_end_" + e.entity + "]");
						pos = e.end;
					}
					if (textReader.docLen - pos > 0)
						buf.append(new String(textReader.content, pos,
								textReader.docLen - pos));
					content = buf.toString();
					entityReader.nextDoc();
				}

				textOutput.println(textReader.docId + "\t" + textReader.url);
				content = HTMLParser.parse(content);
				Matcher m = p.matcher(content);
				buf = new StringBuffer();
				pos = 0;

				entityOutput.println(textReader.docId);
				while (m.find()) {
					buf.append(content.substring(pos, m.start()));
					entityOutput.print("\t" + buf.length());
					buf.append(m.group(2));
					entityOutput.println("\t" + buf.length() + "\t"
							+ m.group(1) + "\t" + m.group(3));
					pos = m.end();
				}
				buf.append(content.substring(pos, content.length()));
				textOutput.println(buf);
			}
			textOutput.close();
			entityOutput.close();
		}
	}

	public static void main(String[] args) throws IOException {
		// String oriTextPath = "./testdata/data/ori_text/";
		// String oriEntityPath = "./testdata/data/ori_entity/";
		// String textPath = "./testdata/data/text/";
		// String entityPath = "./testdata/data/entity/";

		String oriTextPath = args[0];
		String oriEntityPath = args[1];
		String textPath = args[2];
		String entityPath = args[3];
		ExtractFreebaseEntity e = new ExtractFreebaseEntity(oriTextPath,
				oriEntityPath, textPath, entityPath);
		e.process();
	}

}
