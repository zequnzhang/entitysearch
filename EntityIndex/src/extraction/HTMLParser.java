package extraction;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLParser {
	static Pattern script_pattern = Pattern.compile("<script[^>]*>(.*?)</script>", Pattern.CASE_INSENSITIVE);
	static Pattern style_pattern = Pattern.compile("<style[^>]*>(.*?)</style>", Pattern.CASE_INSENSITIVE);
	static Pattern tag_pattern = Pattern.compile("<[^>]+>");
	static Pattern space_pattern = Pattern.compile("\\s+");
	static Pattern nbsp_pattern = Pattern.compile("&[a-z]*;");
	public static String parse(String text) {
		int k = text.indexOf("Content-Length");
		if (k!=-1){
			while (k<text.length() && text.charAt(k)!='\n')
				k++;
			text = text.substring(k);
		}
		StringBuffer buf = new StringBuffer(text);
		for (int i = 0; i < buf.length();i++) {
			if (buf.charAt(i)=='\n' || buf.charAt(i)=='\t' || buf.charAt(i)=='\r') {
				buf.setCharAt(i, ' ');
			} else if (!(buf.charAt(i)>=32 && buf.charAt(i)<=126)) {
				buf.setCharAt(i, '?');
			}
		}
		text = buf.toString();
		Matcher m = style_pattern.matcher(text);
		text = m.replaceAll(" ");
		m = script_pattern.matcher(text);
		text = m.replaceAll(" ");
		m = tag_pattern.matcher(text);
		text = m.replaceAll(" ");
		m = nbsp_pattern.matcher(text);
		text = m.replaceAll(" ");
		m = space_pattern.matcher(text);
		text = m.replaceAll(" ");
		return text;
	}
}
