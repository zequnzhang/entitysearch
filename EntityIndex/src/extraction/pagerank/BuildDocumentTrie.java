package extraction.pagerank;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import util.BufferedPrintStream;
import util.TrieBuilder;

import extraction.freebase.BuildTrieTree;

public class BuildDocumentTrie {

	public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {		
//		String forwardPath = "./testdata/index/forward/";
		String forwardPath = "/shared/osprey/cqs-data/clueweb_index/index/" + args[0] + "/forward/";
		
		System.out.println("Output Doc ID List ...");
		BufferedPrintStream output = new BufferedPrintStream(forwardPath + "doc_id.list");
		Class.forName("org.sqlite.JDBC");
		Connection conn = DriverManager.getConnection("jdbc:sqlite:" + forwardPath + "doc_id.db");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM data order by doc");
		while (rs.next()) {
			output.println(rs.getString(1) + "\t" + rs.getInt(2));
		}
		rs.close();
		stmt.close();
		conn.close();
		output.close();
		
		System.out.println("Build Trie ...");
		TrieBuilder builder = new TrieBuilder(forwardPath + "doc_id.list", forwardPath + "doc_id.trie");
		builder.build();
		new File(forwardPath + "doc_id.list").delete();
	}

}
