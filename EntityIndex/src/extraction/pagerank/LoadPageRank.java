package extraction.pagerank;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;

import util.BufferedFileReader;
import util.BufferedPrintStream;
import util.ExtStringMap;
import util.TrieReader;

public class LoadPageRank {

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
		String forwardPath = "/shared/osprey/cqs-data/clueweb_index/index/" + args[0] + "/forward/";
		String pagerankPath = "/shared/osprey/cqs-data/ClueWeb09-En-PRranked.txt";
//		String forwardPath = "./testdata/index/forward/";
//		String pagerankPath = "./testdata/pagerank";
		
		ExtStringMap<Integer> db = new ExtStringMap<Integer>(forwardPath + "doc_id.db", false, -1,
				"doc", "id", new Integer(0));
		int num = db.size();
		db.close();
		
		TrieReader trieReader = new TrieReader(forwardPath + "doc_id.trie");		
		float[] pagerank = new float[num];
		Arrays.fill(pagerank, 0);
		BufferedFileReader reader = new BufferedFileReader(pagerankPath);
		int k = 0;
		String line;
		while ((line=reader.readLine())!=null) {
			String[] strs = line.split("\t");
			String id = trieReader.get(strs[0]);
			if (id==null)
				continue;
			pagerank[Integer.parseInt(id)] += Float.parseFloat(strs[1]);
			k++;
			if (k%10000 == 0)
				System.out.println(k);
		}
		trieReader.close();
		reader.close();
		
		BufferedPrintStream output = new BufferedPrintStream(forwardPath + "pagerank.list");
		for (int i = 0 ;i < num;i++) {
			output.println(pagerank[i]);
		}
		output.close();
	}

}
