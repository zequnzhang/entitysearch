package others;

import java.io.IOException;
import java.sql.SQLException;

import util.BufferedFileReader;
import util.ExtStringMap;
import util.PorterStemmer;

public class IDFMap {

	public final static int MAX_LEN = 40;
	public final static String DEFAULT_KEY = "#DEFAULT#";

	public static void buildMap() throws IOException, ClassNotFoundException,
			SQLException {
		BufferedFileReader reader = new BufferedFileReader("./general/term_df");
		ExtStringMap<Double> idfMap = new ExtStringMap<Double>(
				"./general/idf_map.db", true, MAX_LEN, new Double(0.0));
		String line;
		int docNum = Integer.parseInt(reader.readLine());
		idfMap.add(DEFAULT_KEY, Math.log(1.0 * docNum));
		int k = 0;
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t");
			String key = ss[0];
			if (key.length() > MAX_LEN)
				key = key.substring(0, MAX_LEN);
			idfMap.add(key, Math.log(1.0 * docNum / Double.parseDouble(ss[1])));
			k += 1;
			if (k % 100000 == 0)
				System.out.println(k);
		}
		idfMap.buildIndex();
		reader.close();
		idfMap.close();
	}
	
	ExtStringMap<Double> idfMap;
	PorterStemmer stem;
	public IDFMap() throws ClassNotFoundException, SQLException {
		idfMap = new ExtStringMap<Double>(
				"./general/idf_map.db", false, MAX_LEN, new Double(0.0));
		stem = new PorterStemmer();
	}
	
	public double idf(String key) throws SQLException {
		Double v = idfMap.get(key);
		if (v==null)
			return idfMap.get(DEFAULT_KEY).doubleValue();
		return v.doubleValue();
	}
	
	public double idf(String word1, String word2) throws SQLException {
		return idf(word1+" "+word2);
	}
	
	public double stemIdf(String word) throws SQLException {
		return idf("#"+stem.stem(word));
	}
	
	
	public void close() throws SQLException {
		idfMap.close();
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, SQLException {
//		IDFMap.buildMap();
		IDFMap idfMap= new IDFMap();
		System.out.println(idfMap.stemIdf("university"));
	}

}
