package others;

import java.io.IOException;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermEnum;

import util.BufferedPrintStream;

public class RetrieveDF {
	public static void main(String[] args) throws CorruptIndexException, IOException {
		BufferedPrintStream output = new BufferedPrintStream("./general/term_df");
		IndexReader reader = IndexReader.open("./testdata/index/keyword/");
		int totalNum = reader.numDocs();
		output.println(totalNum);
		TermEnum te = reader.terms();
		while (te.next()) {
			int freq = te.docFreq();
			if (freq<totalNum/100000) continue;
			output.println(te.term().text() + "\t" + freq);
		}
		reader.close();
		output.close();
	}
}
