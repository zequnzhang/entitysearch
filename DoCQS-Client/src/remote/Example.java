package remote;

import java.rmi.Naming;
import java.util.ArrayList;
import java.util.HashSet;


public class Example {
	
	public static void patternExample() throws Exception{
		System.out.println("xxx");
		QueryHandler handler=new QueryHandler("harrier01.cs.illinois.edu");
		PatternStream patternStream=handler.getPatternStream("[(uiuc|illionis) #person]<10>",1000,10000);
		System.out.println("xxx");
		
		HashSet<Long> docSet = new HashSet<Long>();
		DataItem[][] items;
		while ((items=patternStream.read())!=null){
			for (DataItem[] item:items){
				for (int j=0;j<item.length;j++) {
					System.out.print(item[j]+" ");
					docSet.add(item[j].doc);
				}
				System.out.println();
			}
			//Add a time interval here to avoid over-frequent query. 
			Thread.sleep(1000);
		}
		patternStream.close();
		
//		ArrayList<Long> docList = new ArrayList<Long>();
//		docList.addAll(docSet);
//		DocumentStream documentStream=handler.getDocumentStream(docList);
//		System.out.println("xxx");
//		
//		TextTokens[] tokens;
//		int num = 0;
//		while ((tokens=documentStream.read())!=null){
//			for (TextTokens token:tokens) {
//				System.out.println(token.doc);
//				num ++;
//			}
//			Thread.sleep(200);
//		}
//		documentStream.close();
//		System.out.println(num);

		
	}
	
	public static void documentExample() throws Exception {
		System.out.println("xxx");
		QueryHandler handler=new QueryHandler("harrier01.cs.illinois.edu");
		
		ArrayList<Long> docList = new ArrayList<Long>();
		docList.add(5000318140l);
		DocumentStream documentStream=handler.getDocumentStream(docList);
		System.out.println("xxx");
		
		TextTokens[] tokens;
		int num = 0;
		while ((tokens=documentStream.read())!=null){
			for (TextTokens token:tokens) {
				System.out.println(token.doc+":"+token.tokens);
				num ++;
			}
			Thread.sleep(200);
		}
		documentStream.close();
		System.out.println(num);
	}

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		patternExample();
//		documentExample();

	}

}
