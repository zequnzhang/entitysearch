package util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class TrieReader {

	char[] strData;
	int[] strPt;

	int[] leafData;
	int[] leafPt;

	char[] valData;
	int[] valPt;

	int nodeNum;

	public TrieReader(String dataFile) throws IOException {
		BufferedInputStream r = new BufferedInputStream(new FileInputStream(
				dataFile));
		byte[] buf = new byte[4];
		r.read(buf);
		nodeNum = NumberUtil.bytesToInt(buf, 0);
		int strLen = 0, leafLen = 0, valLen = 0;
		for (int i = 0; i < nodeNum; i++) {
			while (r.read() != 0)
				strLen++;

			while (r.read() != 0)
				valLen++;

			r.read(buf);
			int leafNum = NumberUtil.bytesToInt(buf, 0);
			leafLen += leafNum;
			for (int j = 0; j < leafNum; j++) {
				r.read(buf);
			}
		}
		r.close();

		strData = new char[strLen];
		strPt = new int[nodeNum + 1];

		leafData = new int[leafLen];
		leafPt = new int[nodeNum + 1];

		valData = new char[valLen];
		valPt = new int[nodeNum + 1];

		r = new BufferedInputStream(new FileInputStream(dataFile));
		r.read(buf);
		int strK = 0, leafK = 0, valK = 0;
		int v;
		for (int i = 0; i < nodeNum; i++) {
			strPt[i] = strK;
			while ((v = r.read()) != 0) {
				strData[strK] = (char) v;
				strK++;
			}

			valPt[i] = valK;
			while ((v = r.read()) != 0) {
				valData[valK] = (char) v;
				valK++;
			}

			leafPt[i] = leafK;
			r.read(buf);
			int leafNum = NumberUtil.bytesToInt(buf, 0);
			for (int j = 0; j < leafNum; j++) {
				r.read(buf);
				leafData[leafK] = NumberUtil.bytesToInt(buf, 0);
				leafK++;
			}
		}
		r.close();
		strPt[nodeNum] = strK;
		leafPt[nodeNum] = leafK;
		valPt[nodeNum] = valK;
	}

	private int strLen(int pid) {
		return strPt[pid + 1] - strPt[pid];
	}

	public String get(String key) {
		int pid = nodeNum - 1;
		int k = 0;
		while (true) {
			int strLen = strLen(pid);
			k += strLen;
			Integer nid = search(pid, key, k);
			if (nid == null)
				return null;
			if (strLen(nid) == key.length() - k) {
				if (valPt[nid + 1] - valPt[nid] == 0)
					return null;
				return new String(valData, valPt[nid], valPt[nid + 1]
						- valPt[nid]);
			}
			pid = nid;
		}
	}

	public Pair<String, String> getPrefix(String key) {
		int pid = nodeNum - 1;
		int k = 0;
		Pair<String, String> res = null;
		while (true) {
			int strLen = strLen(pid);
			k += strLen;
			Integer nid = search(pid, key, k);
			if (nid == null)
				return res;

			int ln = strLen(nid);
			if ((k + ln == key.length() || key.charAt(k + ln) == ' ')
					&& valPt[nid + 1] - valPt[nid] != 0) {
				res = new Pair<String, String>(key.substring(0, k + ln), new String(
						valData, valPt[nid], valPt[nid + 1] - valPt[nid]));
			}

			if (ln == key.length() - k)
				return res;
			pid = nid;
		}
	}

	private char getChar(int pid, int k) {
		return strData[strPt[pid] + k];
	}

	private Integer search(int pid, String key, int k) {
		int b = leafPt[pid];
		int e = leafPt[pid + 1];
		while (e > b) {
			int m = (b + e) / 2;
			int i = 0;
			// System.out
			// .println(m + "\t" + new String(leafs[m].str) + "\t" + key);
			int ll = strLen(leafData[m]);
			for (i = 0; i < ll && i + k < key.length(); i++) {
				if (getChar(leafData[m], i) != key.charAt(i + k))
					break;
			}
			if (i == ll)
				return leafData[m];
			if (i + k == key.length())
				return null;
			if (getChar(leafData[m], i) < key.charAt(i + k))
				b = m + 1;
			else
				e = m;
		}
		return null;
	}

	public void close() {
		strData = null;
		strPt = null;
		leafData = null;
		leafPt = null;
		valData = null;
		valPt = null;

	}

	public static void main(String[] args) throws IOException {
		TrieReader reader = new TrieReader(
				"./testdata/data/general/mybase_trie");
		System.out.println(reader.getPrefix("world war ii"));
		
//		BufferedPrintStream output = new BufferedPrintStream(
//				"./general/frequent_entity_category");
//		BufferedFileReader r = new BufferedFileReader(
//				"./testdata/frequent_entity");
//		String line;
//		while ((line = r.readLine()) != null) {
//			String val = reader.get(line);
//			if (val != null)
//				output.println(line + "\t" + val);
//		}
//		r.close();
//		output.close();

	}
}
