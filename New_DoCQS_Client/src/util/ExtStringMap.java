package util;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class ExtStringMap<T> {
	Connection conn;
	Statement stmt;
	PreparedStatement ps;
	String keyColName;
	String valColName;
	String type = null;

	// Supporting INT, TEXT, REAL for Integer, String, Double.
	static final String INT_VAL = "INT";
	static final String STR_VAL = "TEXT";
	static final String REAL_VAL = "REAL";

	public ExtStringMap(String fileName, boolean createNew, int maxKeyLen,
			String keyColName, String valColName, T exampleValue)
			throws ClassNotFoundException, SQLException {
		String typeStr = null;
		if (exampleValue instanceof Integer)
			typeStr = "INT";
		else if (exampleValue instanceof Double)
			typeStr = "REAL";
		else if (exampleValue instanceof String)
			typeStr = "TEXT";
		else
			throw new ClassNotFoundException("Unsupported Types.");

		if (createNew) {
			File docIdFile = new File(fileName);
			if (docIdFile.exists())
				docIdFile.delete();
		}
		this.keyColName = keyColName;
		this.valColName = valColName;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:" + fileName);
		conn.setAutoCommit(false);
		if (createNew) {
			stmt = conn.createStatement();
			stmt.execute("CREATE TABLE data(" + keyColName + " CHAR("
					+ maxKeyLen + "), " + valColName + " " + typeStr + ");");
			stmt.close();
		}
		ps = conn.prepareStatement("INSERT INTO data(" + keyColName + ", "
				+ valColName + ") VALUES(?, ?)");
	}

	public ExtStringMap(String fileName, boolean createNew, int maxKeyLen,
			T exampleValue) throws ClassNotFoundException, SQLException {
		this(fileName, createNew, maxKeyLen, "key", "val", exampleValue);
	}

	public ExtStringMap(String fileName, T exampleValue)
			throws ClassNotFoundException, SQLException {
		this(fileName, false, -1, "key", "val", exampleValue);
	}

	int num = 0;

	public void add(String key, T value) throws SQLException {
		ps.setString(1, key);
		ps.setObject(2, value);
		ps.addBatch();
		num++;
		if (num % 10000 == 0)
			ps.executeBatch();
	}

	public void buildIndex() throws SQLException {
		stmt = conn.createStatement();
		stmt.execute("CREATE INDEX idx ON data(" + keyColName + ");");
		stmt.close();
	}

	public HashMap<String, T> get(ArrayList<String> idList) throws SQLException {
		HashMap<String, T> result = new HashMap<>();
		stmt = conn.createStatement();
		StringBuffer buf = new StringBuffer("SELECT " + keyColName + ", "
				+ valColName + " FROM data WHERE");
		for (int i = 0; i < idList.size(); i++) {
			String c = keyColName + "='" + idList.get(i) + "'";
			if (i == 0)
				buf.append(" " + c);
			else
				buf.append(" OR " + c);
		}
//		System.out.println(buf);
		ResultSet rs = stmt.executeQuery(buf.toString());
		while (rs.next()) {
			result.put(rs.getString(1), (T) rs.getObject(2));
		}
		rs.close();
		stmt.close();
		return result;
	}

	public T get(String id) throws SQLException {
		ArrayList<String> idList = new ArrayList<>();
		idList.add(id);
		return get(idList).get(id);
	}

	public int size() throws SQLException {
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM data;");
		rs.next();
		int s = rs.getInt(1);
		rs.close();
		stmt.close();
		return s;
	}

	public void close() throws SQLException {
		ps.executeBatch();
		conn.setAutoCommit(true);
		conn.close();
	}

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException {
		ExtStringMap<String> db = new ExtStringMap<>("./testdata/test.db",
				true, 10, "key", "val", new String(""));
		db.add("123", "here");
		db.add("456", "this");
		db.add("789", "stupid");
		db.buildIndex();
		db.close();

		db = new ExtStringMap<>("./testdata/test.db", false, 10, "key", "val",
				new String(""));
		System.out.println(db.get("a"));
		// System.out.println(db.size());
		db.close();
	}
}
