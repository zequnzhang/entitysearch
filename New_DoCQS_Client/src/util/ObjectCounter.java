package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ObjectCounter<T> extends HashMap<T,Integer>{
	private static final long serialVersionUID = 1L;
	int total;
	public ObjectCounter(){
		total=0;
	}
	public void addTerm(T obj, int increment){
		Integer freq=get(obj);
		if (freq==null){
			freq=0;
		}
		put(obj, freq+increment);		
		total+=increment;
	}
	public void addTerm(T obj){
		addTerm(obj,1);
	}

	public PairList<T,Integer> getSortedList(){
		PairList<T,Integer> list=new PairList<T,Integer>(this);
		list.sort(PairList.SECOND_DESC);
		return list;
	}
	public int getFrequence(String word){
		Integer freq=get(word);
		if (freq==null) freq=0;
		return freq;
	}
	public int total(){
		return total;		
	}
	
	public void truncate(int num){
		if (size()<num) return;
		PairList<T,Integer> result=getSortedList();
		total=0;
		clear();
		for (int i=0;i<result.size() && i<num;i++){
			total+=result.getSecond(i);
			put(result.getFirst(i),result.getSecond(i));
		}
	}

}
