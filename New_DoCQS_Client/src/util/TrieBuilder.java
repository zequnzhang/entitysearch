package util;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashSet;

public class TrieBuilder {

	String dataFile;
	String treeFile;
	int nodeId;

	public TrieBuilder(String dataFile, String treeFile) {
		// Require Sorted Files!!!
		this.dataFile = dataFile;
		this.treeFile = treeFile;
		this.nodeId = 0;
	}

	public void writeToDisk(TrieNode node, BufferedOutputStream output)
			throws IOException {
		for (TrieNode n : node.list)
			if (n.id == -1)
				writeToDisk(n, output);

		output.write(node.s.getBytes());
		output.write(0);
		
		int k = 0;
		for (String val : node.valSet) {
			if (k != 0)
				output.write('\t');
			output.write(val.getBytes());
			k++;
		}
		output.write(0);
		
		output.write(NumberUtil.intToBytes(node.list.size()));
		for (int i = 0; i < node.list.size(); i++) {
			output.write(NumberUtil.intToBytes(node.list.get(i).id));
		}

		node.id = nodeId;
		nodeId++;
		node.s = null;
		node.valSet.clear();
		node.valSet = null;
		node.list.clear();
		node.list = null;
	}

	public void build() throws IOException {
		BufferedFileReader reader = new BufferedFileReader(dataFile);
		BufferedOutputStream output = new BufferedOutputStream(
				new FileOutputStream(treeFile));
		output.write(NumberUtil.intToBytes(0));
		String line;
		ArrayList<TrieNode> activeNodes = new ArrayList<TrieNode>();
		TrieNode node = new TrieNode("");
		activeNodes.add(node);
		String lastStr = "";
		int lineNum = 0;
		while ((line = reader.readLine()) != null) {
			if (lineNum%100000==0)
				System.out.println(lineNum);
			lineNum++;
			String[] ss = line.split("\t");
			String key = ss[0];
			String val = ss[1];
			if (lastStr.equals(key)) {
				activeNodes.get(activeNodes.size() - 1).valSet.add(val);
			} else if (key.startsWith(lastStr)) {
				node = new TrieNode(key.substring(lastStr.length()));
				node.valSet.add(val);
				activeNodes.get(activeNodes.size() - 1).list.add(node);
				activeNodes.add(node);
			} else {
				int i = 0, j = 0, k = 0;
				while (i < activeNodes.size()) {
					while (j >= activeNodes.get(i).s.length()) {
						j = 0;
						i++;
					}
					if (activeNodes.get(i).s.charAt(j) == key.charAt(k)) {
						j++;
						k++;
					} else
						break;
				}

				node = new TrieNode(key.substring(k));
				node.valSet.add(val);
				if (j == 0) {
					activeNodes.get(i - 1).list.add(node);
					for (int l = activeNodes.size() - 1; l >= i; l--) {
						writeToDisk(activeNodes.get(l), output);
						activeNodes.remove(l);
					}
					activeNodes.add(node);
				} else {
					TrieNode newNode = new TrieNode(
							activeNodes.get(i).s.substring(0, j));
					activeNodes.get(i).s = activeNodes.get(i).s.substring(j);
					newNode.list.add(activeNodes.get(i));
					newNode.list.add(node);
					activeNodes.get(i - 1).list
							.remove(activeNodes.get(i - 1).list.size()-1);
					activeNodes.get(i - 1).list.add(newNode);
					for (int l = activeNodes.size() - 1; l >= i; l--) {
						writeToDisk(activeNodes.get(l), output);
						activeNodes.remove(l);
					}
					activeNodes.add(newNode);
					activeNodes.add(node);

				}
			}
			lastStr = key;
		}
		for (int i = activeNodes.size() - 1; i >= 0; i--) {
			writeToDisk(activeNodes.get(i), output);
			activeNodes.remove(i);
		}
		reader.close();
		output.close();
		RandomAccessFile f = new RandomAccessFile(treeFile, "rw");
		f.seek(0);
		f.write(NumberUtil.intToBytes(nodeId));
		f.close();
	}

	public static void main(String[] args) throws IOException {
		TrieBuilder builder = new TrieBuilder("./testdata/data/general/mybase_type",
				"./testdata/data/general/mybase_type_trie");
		builder.build();
	}

	class TrieNode {
		TrieNode(String s) {
			this.s = s;
			this.id = -1;
			valSet = new HashSet<String>();
			list = new ArrayList<TrieNode>();
		}

		int id;
		String s;
		HashSet<String> valSet;
		ArrayList<TrieNode> list;
	}
}
