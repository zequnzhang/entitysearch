package tool;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.DocInfo;
import remote.node.interfaces.NodeDocumentStreamInterface;
import remote.node.interfaces.NodeQueryHandlerInterface;

public class NodeQueryHandlerFactory {
	// Time out for one rmi query call.
	static final int RMI_QUERY_TIME_OUT = 5000;
	static final int NODE_NUM = 19;

	static NodeQueryHandlerInterface[] handlers = null;

	static {
		System.setProperty("sun.rmi.transport.tcp.responseTimeout",
				RMI_QUERY_TIME_OUT + "");
		handlers = new NodeQueryHandlerInterface[NODE_NUM];
	}

	public static NodeQueryHandlerInterface getHandler(int nodeId)
			throws Exception {
		if (handlers[nodeId] == null) {
			handlers[nodeId] = (NodeQueryHandlerInterface) Naming
					.lookup("//osprey" + (nodeId + 1)
							+ ".cs.illinois.edu:1100/query");
		}
		return handlers[nodeId];
	}

	public static void main(String[] args) throws Exception {
		ArrayList<Integer> l = new ArrayList<>();
		l.add(3583157);
		NodeDocumentStreamInterface inf = NodeQueryHandlerFactory.getHandler(7)
				.getNodeDocumentStream(l);
		ArrayList<DocInfo> docs;
		while ((docs = inf.read()) != null) {
			for (DocInfo d : docs) {
				for (int i = 0 ;i < d.tokens.length;i++)
					System.out.println(i + "\t" + d.tokens[i]);
			}
		}
	}

}
