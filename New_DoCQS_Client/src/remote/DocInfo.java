package remote;

import java.io.Serializable;

public class DocInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4730892881279231320L;
	public int cluster;
	public long id;
	public float pagerank;
	public String docId;
	public String url;
	public String[] tokens;
	public EntityInfo[] entities;

	public String toString() {
		StringBuffer r = new StringBuffer();
		r.append("Doc ID: " + docId + "\n");
		r.append("URL: " + url + "\n");
		r.append("PageRank: " + pagerank + "\n");
		r.append("Content: (" + tokens.length + " Tokens)");
		for (int i = 0; i < tokens.length && i < 10; i++) {
			r.append(" " + tokens[i]);
		}
		r.append("...\n");
		for (int i = 0; i < entities.length && i < 20; i++) {
			r.append(entities[i]+"\n");
		}
		return r.toString();
	}
}
