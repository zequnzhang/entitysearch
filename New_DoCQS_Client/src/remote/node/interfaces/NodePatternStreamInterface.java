package remote.node.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.TokenItem;

public interface NodePatternStreamInterface extends Remote {
	public ArrayList<TokenItem[]> read() throws RemoteException;
	public void close() throws RemoteException;
}
