package remote;

import java.io.Serializable;

public class WordFreq implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6673973177682894478L;
	public int cluster;
	public long doc;
	public float pagerank;
	public int docLen;
	public int[] freqs;

	public String toString() {
		String r = doc + ", " + pagerank + ", " + docLen + ": ";
		for (int i = 0; i < freqs.length; i++)
			r += " " + freqs[i];
		return r;

	}
}
