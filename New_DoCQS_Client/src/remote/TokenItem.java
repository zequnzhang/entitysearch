package remote;

import java.io.IOException;
import java.io.Serializable;

public class TokenItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5924306445223230447L;

	public TokenItem(long doc, int pos,int len, String text, String schema){
		this.doc=doc;
		this.pos=pos;
		this.len=len;
		this.text=text;
		this.schema=schema;
	}
	
	public long doc;
	public int pos;
	public int len;
	public String text;
	public String schema;
	
	public String getText() throws IOException{
		return text;
	}

	public String toString() {
		return "Item("+doc+","+pos+","+len+","+text+")";
	}
	public boolean equals(TokenItem item){
		if (doc==item.doc && pos==item.pos && len==item.len) return true;
		return false;
	}
}
