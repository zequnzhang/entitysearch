package index;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;

import conf.Constant;
import index.reader.DocLengthReader;
import index.reader.ForwardIndexReader;
import index.reader.PageRankReader;

public class IndexReaderManager {
	// Put things static in this class.

	static IndexReader invertedTextIndexReader;
	static ForwardIndexReader forwardIndexReader;
	static PageRankReader pageRankReader;
	static DocLengthReader docLenReader;

	static {
		forwardIndexReader = null;
		invertedTextIndexReader = null;
		pageRankReader = null;
		docLenReader = null;
	}
	
	public static void init() throws IOException {
		getForwardIndexReader();
		getInvertedTextIndexReader();
		getPageRankReader();
		getDocLenReader();
	}

	public static String getSnippet(int doc, int pos, int len) throws IOException {
		TextTokenList list = getForwardIndexReader().getDoc(doc).tokenList;
		return list.getText(pos, len);
	}

	public static ForwardIndexReader getForwardIndexReader()
			throws IOException {
		try {
			if (forwardIndexReader == null)
				forwardIndexReader = new ForwardIndexReader(
						Constant.INDEX_LOCATION + "/forward/");

			return forwardIndexReader;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static IndexReader getInvertedTextIndexReader() throws IOException {
		if (invertedTextIndexReader == null) {
			invertedTextIndexReader = IndexReader.open(Constant.INDEX_LOCATION
					+ "/keyword/");
		}
		return invertedTextIndexReader;
	}

	public static PageRankReader getPageRankReader() throws IOException {
		if (pageRankReader == null) 
			pageRankReader = new PageRankReader(docNum());
		return pageRankReader;
	}

	public static int docNum() throws IOException {
		return getForwardIndexReader().docNum();
	}
	
	public static DocLengthReader getDocLenReader() throws IOException {
		if (docLenReader == null)
			docLenReader = new DocLengthReader(docNum());
		return docLenReader;
	}

//	public void close() throws IOException {
//		try {
//		if (forwardTextIndexReader != null)
//			forwardTextIndexReader.close();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		if (invertedTextIndexReader != null)
//			invertedTextIndexReader.close();
//		if (forwardNERIndexReader != null)
//			forwardNERIndexReader.close();
//		if (forwardFBIndexReader != null)
//			forwardFBIndexReader.close();
//		if (forwardMyBaseIndexReader != null)
//			forwardMyBaseIndexReader.close();
//	}
}
