package index;

import java.util.ArrayList;

public class FullDoc {
	public String docId;
	public String url;
	public TextTokenList tokenList;
	public ArrayList<NEREntity> nerEntities;
	public ArrayList<FBEntity> fbEntities;

	public String toString() {
		String r = "";
		r += "Doc ID: " + docId + "\n";
		r += "URL: " + url + "\n";
		r += "Content: " + tokenList.size() + " Tokens.\n";
		r += nerEntities.size() + " NER Entities.\n";
		r += fbEntities.size() + " FB Entities.\n";
		return r;
	}
}
