package index.tokenizer;

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

WARNING: if you change StandardTokenizerImpl.jflex and need to regenerate
      the tokenizer, only use Java 1.4 !!!
      This grammar currently uses constructs (eg :digit:, :letter:) whose 
      meaning can vary according to the JRE used to run jflex.  See
      https://issues.apache.org/jira/browse/LUCENE-1126 for details.
      For current backwards compatibility it is needed to support
      only Java 1.4 - this will change in Lucene 3.1.

*/

import org.apache.lucene.analysis.Token;
import index.IndexTextToken;

%%

%class DoCQSTokenizerImpl
%unicode
%function nextToken
%pack
%char
%type IndexTextToken

%{

public final int yychar()
{
    return yychar;
}

%}

THAI       = [\u0E00-\u0E59]

// basic word: a sequence of digits & letters (includes Thai to enable ThaiAnalyzer to function)
ALPHANUM   = ({LETTER}|{THAI}|[:digit:])+

// internal apostrophes: O'Reilly, you're, O'Reilly's
// use a post-filter to remove possessives
APOSTROPHE =  {ALPHA} ("'" {ALPHA})+

// acronyms: U.S.A., I.B.M., etc.
// use a post-filter to remove dots
ACRONYM    =  {LETTER} "." ({LETTER} ".")+

// company names like AT&T and Excite@Home.
COMPANY    =  {ALPHA} ("&"|"@") {ALPHA}

// email addresses
EMAIL      =  {ALPHANUM} (("."|"-"|"_") {ALPHANUM})* "@" {ALPHANUM} (("."|"-") {ALPHANUM})+

// hostname
HOST       =  {ALPHANUM} ((".") {ALPHANUM})+

// floating point, serial, model numbers, ip addresses, etc.
// every other segment must have at least one digit
NUM        = ({ALPHANUM} {P} {HAS_DIGIT}
           | {HAS_DIGIT} {P} {ALPHANUM}
           | {ALPHANUM} ({P} {HAS_DIGIT} {P} {ALPHANUM})+
           | {HAS_DIGIT} ({P} {ALPHANUM} {P} {HAS_DIGIT})+
           | {ALPHANUM} {P} {HAS_DIGIT} ({P} {ALPHANUM} {P} {HAS_DIGIT})+
           | {HAS_DIGIT} {P} {ALPHANUM} ({P} {HAS_DIGIT} {P} {ALPHANUM})+)

// punctuation
P	         = ("_"|"-"|"/"|"."|",")

// at least one digit
HAS_DIGIT  = ({LETTER}|[:digit:])* [:digit:] ({LETTER}|[:digit:])*

ALPHA      = ({LETTER})+

// From the JFlex manual: "the expression that matches everything of <a> not matched by <b> is !(!<a>|<b>)"
LETTER     = !(![:letter:]|{CJ})

// Chinese and Japanese (but NOT Korean, which is included in [:letter:])
CJ         = [\u3100-\u312f\u3040-\u309F\u30A0-\u30FF\u31F0-\u31FF\u3300-\u337f\u3400-\u4dbf\u4e00-\u9fff\uf900-\ufaff\uff65-\uff9f]

WHITESPACE = \r\n | [ \r\n\t\f]

SYMBOL = ("$"|"!"|"("|")"|"-"|"~"|"@"|"#"|"%"|"^"|"&"|"*"|"<"|">"|"?")

%%

{ALPHANUM}                                                     { return new IndexTextToken(IndexTextToken.ALPHANUM,yychar(),yytext()); }
{APOSTROPHE}                                                   { return new IndexTextToken(IndexTextToken.APOSTROPHE,yychar(),yytext()); }
{ACRONYM}                                                      { return new IndexTextToken(IndexTextToken.ACRONYM,yychar(),yytext()); }
{COMPANY}                                                      { return new IndexTextToken(IndexTextToken.COMPANY,yychar(),yytext()); }
{EMAIL}                                                        { return new IndexTextToken(IndexTextToken.EMAIL,yychar(),yytext()); }
{HOST}                                                         { return new IndexTextToken(IndexTextToken.HOST,yychar(),yytext()); }
{NUM}                                                          { return new IndexTextToken(IndexTextToken.NUM,yychar(),yytext()); }
{CJ}                                                           { return new IndexTextToken(IndexTextToken.CJ,yychar(),yytext()); }
{SYMBOL}													   { return new IndexTextToken(IndexTextToken.SYMBOL,yychar(),yytext()); }

/** Ignore the rest */
. | {WHITESPACE}                                               { /* ignore */ }
