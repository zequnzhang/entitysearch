package index.tokenizer;

public class IndexTerm implements Comparable<IndexTerm>{
	public String text;
	public int pos;
	public byte[] bytes;
	public int compareTo(IndexTerm o) {
		return pos-o.pos;
	}
	
	public String toString(){
		return text+"("+pos+")";
	}
}
