package index.tokenizer;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import index.TextToken;
import index.TextTokenList;

public class DoCQSTokenizer {
	
	DoCQSTokenizerImpl scanner;
	
	public DoCQSTokenizer(){
		scanner=new DoCQSTokenizerImpl(new StringReader(""));		
	}
	
	public TextTokenList parseText(String text) throws IOException{
		text=filterText(text);
//		System.out.println(text);
		StringReader reader=new StringReader(text);
		TextTokenList tokenList=new TextTokenList();
		scanner.yyreset(reader);
		TextToken t=null;
		while((t=scanner.nextToken())!=null){
			tokenList.add(t);
		}
		tokenList.offset=new int[tokenList.size()];
		for (int i=0;i<tokenList.size();i++)
			tokenList.offset[i]=tokenList.get(i).startOffset;
		return tokenList;
	}
	
	private String filterText(String text){
		text=text.replaceAll("&[a-z]{5};", "       ");
		text=text.replaceAll("&[a-z]{4};", "      ");
		text=text.replaceAll("&[a-z]{3};", "     ");
		text=text.replaceAll("&[a-z]{2};", "    ");
		text=text.replaceAll("~", " ");
		return text;
	}

	public static void main(String[] args) throws IOException {
		// 1
//		TextReader r=new TextReader();
//		r.nextFile();
//		for (int i=0;i<1000;i++)
//			r.nextDoc();
//		DoCQSTokenizer tokenizer=new DoCQSTokenizer();
//		String c=r.getDocContent();
//		System.out.println(c);	
//		System.out.println("--------------");
//		System.out.println(tokenizer.parseText(c));
		
	}

}
