package index.reader;

import java.io.IOException;

import util.BufferedFileReader;
import conf.Constant;

public class DocLengthReader {
	int docNum;
	int[] lens;

	public DocLengthReader(int docNum) throws IOException {
		this.docNum = docNum;
		this.lens = new int[docNum];
		BufferedFileReader reader = new BufferedFileReader(
				Constant.INDEX_LOCATION + "/forward/length.list");
		String line;
		int k = 0;
		while ((line = reader.readLine()) != null && k < docNum) {
			this.lens[k] = Integer.parseInt(line);
			k++;
		}

		reader.close();
	}
	
	public int getDocLength(int doc) {
		return lens[doc];
	}
	
	public void close() {
		docNum = -1;
		lens = null;
	}
}
