package index.reader;

import java.io.IOException;

import conf.Constant;
import util.BufferedFileReader;

public class PageRankReader {
	int docNum;
	float[] ranks;

	public PageRankReader(int docNum) throws IOException {
		this.docNum = docNum;
		this.ranks = new float[docNum];
		BufferedFileReader reader = new BufferedFileReader(
				Constant.INDEX_LOCATION + "/forward/pagerank.list");
		String line;
		int k = 0;
		while ((line = reader.readLine()) != null && k < docNum) {
			this.ranks[k] = Float.parseFloat(line);
			k++;
		}

		reader.close();
	}
	
	public float getPageRank(int doc) {
		return ranks[doc];
	}
	
	public void close() {
		docNum = -1;
		ranks = null;
	}

}
