package test;

import index.IndexReaderManager;
import index.TextTokenList;
import index.tokenizer.DoCQSTokenizer;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.index.TermPositions;

import conf.Constant;

import exception.DoCQSException;

import query.PatternParserManager;
import query.tree.reg.RegDataInfo;
import query.tree.reg.RegNode;
import util.BufferedFileReader;
import util.BufferedPrintStream;
import util.Counter;
import util.Pair;
import util.PairList;
import util.SampleList;

public class QueryTest {
	
	public static final int ENTITY_NUM=80;

	public static void main(String[] args) throws IOException {		
//		BufferedFileReader reader=new BufferedFileReader("/home/zhou18/workspace/EntityResolution/data-tmp/birth/data");
//		String line=null;
//		SampleList<Pair<String, String>> sampleList=new SampleList<Pair<String, String>>(1000000);
//		while ((line=reader.readLine())!=null){
//			String[] strs=line.split("\t");
//			if (strs.length!=2) continue;
//			String query=strs[0];
//			strs=strs[1].split(",");
//			if (strs.length==0) continue;
//			String answer=strs[0].trim().toLowerCase();
//			sampleList.addItemWithSampling(new Pair<String, String>(query, answer));
//		}
//		reader.close();
//		int total=0;
//		
//		PrintStream output=new PrintStream("/home/zhou18/workspace/EntityResolution/data-tmp/birth/output");
//		for (int p=0;p<sampleList.size();p++){
//			System.out.println(p);
//			String query=sampleList.get(p).first;
//			ArrayList<RegDataInfo> items=new ArrayList<RegDataInfo>();
//			try {
//				items = queryData("{"+query+"}^q");
//			} catch (DoCQSException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			output.println(query+"\t"+items.size());			
//			total++;
//		}
//		output.close();
	}
	
//	public static ArrayList<RegDataInfo> queryData(String query) throws DoCQSException, IOException{
//		ArrayList<RegDataInfo> infoList=new ArrayList<RegDataInfo>();
//		for (int i=0;i<Constant.INDEX_NUM;i++){
//			IndexReaderManager manager=new IndexReaderManager(i);
//			RegNode node=PatternParserManager.parsePatternToNode(manager, query);
//			while (node.nextDoc()){
//				ArrayList<RegDataInfo> list=node.getDataInfoList();
//				infoList.addAll(list);
//			}
//			manager.close();			
//		}
//		return infoList;
//	}

}
