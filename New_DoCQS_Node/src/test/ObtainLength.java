package test;

import conf.Constant;
import util.BufferedPrintStream;
import index.IndexReaderManager;
import index.reader.ForwardIndexReader;

public class ObtainLength {

	public static void main(String[] args) throws Exception {
		Constant.INDEX_LOCATION = args[0];
		ForwardIndexReader indexReader = IndexReaderManager
				.getForwardIndexReader();
		BufferedPrintStream output = new BufferedPrintStream(
				Constant.INDEX_LOCATION + "/forward/length.list");
		for (int i = 0; i < indexReader.docNum(); i++) {
			if (i%10000 == 0)
				System.out.println(i);
			output.println(indexReader.getDoc(i).tokenList.size());
		}
		output.close();
	}

}
