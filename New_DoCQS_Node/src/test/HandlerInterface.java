package test;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HandlerInterface extends Remote{
	public void test() throws RemoteException;
}
