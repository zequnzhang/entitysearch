package test;

import java.io.IOException;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermPositions;

import util.ForwardArrayList;
import conf.Constant;

public class Test {

	public static void main(String[] args) throws CorruptIndexException, IOException {
		Constant.INDEX_LOCATION = "/shared/osprey/cqs-data/clueweb_index/index/0/";
		IndexReader reader = IndexReader.open(Constant.INDEX_LOCATION+"/keyword/");
		TermDocs tp = reader.termDocs(new Term("Content", "kevin"));
		while(tp.next()) {
			System.out.println(tp.doc()+"\t"+tp.freq());
		}
		reader.close();
		tp.close();
	}

}
