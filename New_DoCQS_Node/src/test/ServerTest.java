package test;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.DocInfo;
import remote.TokenItem;
import remote.Word;
import remote.WordFreq;
import remote.node.impls.NodeWordStream;
import remote.node.interfaces.NodeDocumentStreamInterface;
import remote.node.interfaces.NodePatternStreamInterface;
import remote.node.interfaces.NodeQueryHandlerInterface;
import remote.node.interfaces.NodeWordStreamInterface;

public class ServerTest {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, InterruptedException{
		NodeQueryHandlerInterface handler=(NodeQueryHandlerInterface)Naming.lookup("//localhost:1100/query");
		
//		Word[] words = new Word[3];
//		words[0] = new Word("UIUC", false, true);
//		words[1] = new Word("university", true, true);
//		words[2] = new Word("computer science", false, false);
//		NodeWordStreamInterface stream = handler.getNodeWordStream(words);
//		ArrayList<WordFreq> itemList;
//		while ((itemList = stream.read())!=null) {
//			System.out.println(itemList.size());
//			for (WordFreq f:itemList)
//				System.out.println(f);			
//			Thread.sleep(100);
//		}

//		NodePatternStreamInterface stream=handler.getNodePatternStream("[kevin #mybase.153]<10> ");
//		ArrayList<TokenItem[]> itemList;
//		while ((itemList=stream.read())!=null){
//			System.out.println(itemList.size());
//			Thread.sleep(1000);
//			for (TokenItem[] items:itemList){
//				for (int i=0;i<items.length;i++)
//					System.out.print(items[i]+" ");
//				System.out.println();
//			}
//		}
//		stream.close();
		
		ArrayList<Integer> docList=new ArrayList<Integer>();
		for (int i=3000;i<4000;i++){
			docList.add(i*9);
		}
		int nn=0;
		NodeDocumentStreamInterface stream=handler.getNodeDocumentStream(docList);
		ArrayList<DocInfo> infoList = new ArrayList<DocInfo>();
		while ((infoList=stream.read())!=null){
			for (DocInfo info:infoList) {
				nn++;
				System.out.println(info);
			}
			Thread.sleep(100);
		}
		System.out.println(nn);
		
//		NodePatternStreamInterface stream=handler.getNodePatternStream("[Kevin #email]<30>");
//		ArrayList<DataItem[]> items;
//		while ((items=stream.read())!=null){
//			System.out.println(items.size());
//		}
//		stream.close();
	}

}
