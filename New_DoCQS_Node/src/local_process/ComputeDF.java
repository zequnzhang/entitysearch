package local_process;

import index.FullDoc;
import index.reader.ForwardIndexReader;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Random;

import util.BufferedPrintStream;
import util.Counter;
import util.PairList;
import util.PorterStemmer;

public class ComputeDF {
	public static void main(String[] args) throws IOException,
			ClassNotFoundException, SQLException {
		ForwardIndexReader reader = new ForwardIndexReader(
				"/shared/osprey/cqs-data/clueweb_index/index/0/forward/");
		Random rand = new Random();
		int docid = 0;
		PorterStemmer stemmer = new PorterStemmer();
		Counter wordCnt = new Counter();
		for (int i = 0; i < 500000; i++) {
			if (i % 1000 == 0)
			    System.out.println(i);
			if (docid >= reader.docNum())
				break;
			FullDoc doc = reader.getDoc(docid);
			HashSet<String> wordSet = new HashSet<>();
			for (int j = 0; j < doc.tokenList.size(); j++) {
				String w = stemmer
						.stem(doc.tokenList.get(j).text.toLowerCase());
				wordSet.add(w);
				if (j != doc.tokenList.size() - 1)
					w += " "
							+ stemmer.stem(doc.tokenList.get(j + 1).text
									.toLowerCase());
				wordSet.add(w);
			}
			for (String w : wordSet)
				wordCnt.addTerm(w);
			docid += rand.nextInt(10)+1;
			if (wordCnt.size() > 1500000)
				wordCnt.truncate(1000000);
		}

		BufferedPrintStream output = new BufferedPrintStream("./data/term_df");
		PairList<String, Integer> sorted = wordCnt.getSortedList();
		for (int i = 0; i < sorted.size(); i++)
			output.println(sorted.getFirst(i) + "\t" + sorted.getSecond(i));
		output.close();
	}
}
