package local_process;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.HashSet;

import util.BufferedFileReader;
import util.BufferedPrintStream;

public class GetInvolvedQuery {

	public static void main(String[] args) throws IOException {
		BufferedFileReader reader = new BufferedFileReader(
				"./data/sample_by_category");
		String line;
		line = reader.readLine();
		HashSet<Integer> qidSet = new HashSet<Integer>();
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t");
			qidSet.add(Integer.parseInt(ss[0]));
			int n = Integer.parseInt(ss[2]) + Integer.parseInt(ss[3])
					+ Integer.parseInt(ss[4]);
			for (int i = 0; i < n; i++)
				reader.readLine();
		}
		reader.close();

		reader = new BufferedFileReader(
				"D:/workspace/program/RelationLearner/data/search_data");
		BufferedPrintStream output = new BufferedPrintStream(
				"./data/involved_query");
		int k = 0;
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t");
			String content = reader.readLine();
			reader.readLine();
			if (qidSet.contains(k)) {
				output.println("@\t" + k + "\t" + ss[3]);
				output.println(content);
			}
			k++;
		}
		reader.close();
		output.close();

	}

}
