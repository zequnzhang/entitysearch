package local_process;

import index.IndexReaderManager;
import index.reader.DocLengthReader;
import index.reader.PageRankReader;

import java.io.IOException;

import util.BufferedFileReader;
import util.BufferedPrintStream;
import conf.Constant;

public class GetDocInfo {

	public static void main(String[] args) throws IOException {
		Constant.INDEX_LOCATION = "/shared/osprey/cqs-data/clueweb_index/index/"
				+ args[0] + "/";

		PageRankReader pr = new PageRankReader(IndexReaderManager.docNum());
		DocLengthReader dr = new DocLengthReader(IndexReaderManager.docNum());
		BufferedPrintStream output = new BufferedPrintStream("./data/doc_info/"
				+ args[0]);
		BufferedFileReader reader = new BufferedFileReader("./data/docid");
		String line;
        int k = 0;
		while ((line = reader.readLine()) != null) {
            if (k%10000==0)
                System.out.println(k);
            k += 1;
			String[] ss = line.split("\t");
			if (ss[0].equals(args[0])) {
				int doc = Integer.parseInt(ss[1]);
				output.println(doc + "\t" + pr.getPageRank(doc) + "\t"
						+ dr.getDocLength(doc));
			}
		}
		reader.close();
		output.close();
		pr.close();
		dr.close();
	}

}
