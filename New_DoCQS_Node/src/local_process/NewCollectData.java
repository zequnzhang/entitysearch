package local_process;

import index.FullDoc;
import index.IndexReaderManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import util.BufferedFileReader;
import util.BufferedZipPrintStream;
import util.Counter;
import util.PorterStemmer;

import conf.Constant;

public class NewCollectData {

	public static void main(String[] args) throws IOException {
		int cid = Integer.parseInt(args[0]);

		Constant.INDEX_LOCATION = "/shared/osprey/cqs-data/clueweb_index/index/"
				+ cid + "/";
		HashMap<String, Integer> wordMap = new HashMap<String, Integer>();
		BufferedFileReader reader = new BufferedFileReader("./data/termfreq");
		String line;
		int k = 0;
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t");
			wordMap.put(ss[0], k);
			k++;
		}
		reader.close();

		PorterStemmer stemmer = new PorterStemmer();
		BufferedZipPrintStream output = new BufferedZipPrintStream(
				"/shared/osprey/cqs-data/collected_data/sample/" + cid + ".gz");
		reader = new BufferedFileReader("./data/sample_by_category");
		reader.readLine();
		int lineNum = 0;
		while ((line = reader.readLine()) != null) {
			lineNum++;
			System.out.println(lineNum);
			String[] ss = line.split("\t");
			String qid = ss[0];
			String cat = ss[1];
			int n = Integer.parseInt(ss[2]) + Integer.parseInt(ss[3])
					+ Integer.parseInt(ss[4]);
			ArrayList<Integer> idList = new ArrayList<Integer>();
			for (int i = 0; i < n; i++) {
				ss = reader.readLine().split("\t");
				int c = Integer.parseInt(ss[1]);
				int doc = Integer.parseInt(ss[2]);
				if (c == cid)
					idList.add(doc);
			}
			output.println("@\t" + qid + "\t" + cat + "\t" + idList.size());
			for (int i = 0; i < idList.size(); i++) {
				int doc = idList.get(i);
				int len = IndexReaderManager.getDocLenReader()
						.getDocLength(doc);
				double pagerank = IndexReaderManager.getPageRankReader()
						.getPageRank(doc);
				Counter cnt = new Counter();
				FullDoc fullDoc = IndexReaderManager.getForwardIndexReader()
						.getDoc(doc);
				for (int j = 0; j < fullDoc.tokenList.size(); j++) {
					String word = stemmer.stem(fullDoc.tokenList.get(j).text
							.toLowerCase());
					Integer wid = wordMap.get(word);
					if (wid == null)
						continue;
					cnt.addTerm("" + wid);
				}
				output.print("#\t" + doc + "\t" + len + "\t" + pagerank);
				for (String key : cnt.keySet()) {
					output.print("\t" + key + ":" + cnt.getFrequence(key));
				}
				output.println();
			}
		}
		output.close();
		reader.close();
	}
}
