package local_process;

import index.IndexReaderManager;
import index.TextTokenList;
import index.reader.ForwardIndexReader;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.GZIPOutputStream;

import conf.Constant;
import util.BufferedFileReader;
import util.ObjectCounter;

public class CollectData {

	public static final String entityFilePath = "./data/search_data";
	public HashMap<String, ArrayList<Entity>> inverted;
	public ArrayList<Entity> entityList;
	public String outputFile;

	public CollectData(String outputFile) {
		this.outputFile = outputFile;
	}

	public void readEntities() throws IOException {
		System.out.println("Read Entities ...");
		entityList = new ArrayList<Entity>();
		BufferedFileReader reader = new BufferedFileReader(entityFilePath);
		String line;
		int k = 0;
		while ((line = reader.readLine()) != null) {
			if (k % 100000 == 0)
				System.out.println(k);
			Entity e = new Entity();
			e.name = line.split("\t")[3];
			entityList.add(e);
			e.id = k;
			line = reader.readLine();
			e.words = line.substring(2).split("\t");
			line = reader.readLine();
			e.attrs = line.substring(2).split("\t");
			k++;
		}
		reader.close();
	}

	public void buildIndex() {
		System.out.println("Build Inverted Index ...");
		inverted = new HashMap<>();
		for (Entity e : entityList) {
			e.wordNum = 0;
			for (int i = 0; i < e.words.length; i++) {
				String w = e.words[i];
				if (w.split(" ").length != 1)
					continue;

				e.wordNum++;
				ArrayList<Entity> el = inverted.get(w);
				if (el == null) {
					el = new ArrayList<>();
					inverted.put(w, el);
				}
				el.add(e);
			}
		}
	}

	public void checkDocument() throws IOException {
		ForwardIndexReader manager = IndexReaderManager.getForwardIndexReader();
		int docNum = manager.docNum();
		PrintStream output = new PrintStream(new GZIPOutputStream(
				new BufferedOutputStream(new FileOutputStream(outputFile))));

		for (int i = 0; i < docNum; i++) {
            if (i%1000 == 0)
    			System.out.println(i);
			TextTokenList tl = manager.getDoc(i).tokenList;
			ObjectCounter<Entity> entityCnt = new ObjectCounter<>();
			ObjectCounter<String> strCnt = new ObjectCounter<>();
			HashSet<String> singleWords = new HashSet<>();
			for (int j = 0; j < tl.size(); j++) {
				singleWords.add(tl.get(j).text);
				StringBuffer buf = new StringBuffer();
				for (int k = 0; k < 6 && j + k < tl.size(); k++) {
					if (k != 0)
						buf.append(" ");
					buf.append(tl.get(j + k).text);
					strCnt.addTerm(buf.toString());
				}
			}
			for (String w : singleWords) {
				if (!inverted.containsKey(w))
					continue;
				for (Entity e : inverted.get(w))
					entityCnt.addTerm(e);
			}
			ArrayList<Entity> elist = new ArrayList<Entity>();
			for (Entity e : entityCnt.keySet()) {
				if (entityCnt.getFrequence(e) >= 1.0 * e.wordNum * 0.7)
					elist.add(e);
			}
			output.println(i + "\t" + elist.size());
			for (Entity e : elist) {
				output.print("\t" + e.id);
				for (int j = 0; j < e.words.length; j++) {
					int f = strCnt.getFrequence(e.words[j]);
					if (f > 0)
						output.print("\t" + j + ":" + f);
				}
				for (int j = 0; j < e.attrs.length; j++) {
					int f = strCnt.getFrequence(e.attrs[j]);
					if (f > 0)
						output.print("\t@" + j + ":" + f);
				}
				output.println();
			}
		}

		output.close();

	}

	public void run() throws IOException {
		readEntities();
		buildIndex();
		checkDocument();
	}

	public static void main(String[] args) throws IOException {
		String id = args[0];
		Constant.INDEX_LOCATION = "/shared/osprey/cqs-data/clueweb_index/index/"
				+ id + "/";
		CollectData c = new CollectData(
				"/shared/osprey/cqs-data/collected_data/" + id + ".gz");
//		CollectData c = new CollectData("./data/1.gz");
		c.run();

	}

}

class Entity {
	String name;
	int id;
	int wordNum;
	String[] words;
	String[] attrs;
}
