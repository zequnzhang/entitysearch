package local_process;

import index.FBEntity;
import index.FullDoc;
import index.IndexReaderManager;
import index.NEREntity;
import index.reader.ForwardIndexReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.zip.GZIPOutputStream;

import util.BufferedFileReader;
import util.BufferedZipPrintStream;
import conf.Constant;

public class GetDocs {

	public static void main(String[] args) throws IOException {
		Constant.INDEX_LOCATION = "/shared/osprey/cqs-data/clueweb_index/index/"
				+ args[0] + "/";

		BufferedFileReader reader = new BufferedFileReader("./data/docid");
		String line;
		HashSet<Integer> docSet = new HashSet<Integer>();
		while ((line = reader.readLine()) != null) {
			String[] ss = line.split("\t");
			if (ss[0].equals(args[0])) {
				int doc = Integer.parseInt(ss[1]);
				docSet.add(doc);
			}
		}
		reader.close();
		ArrayList<Integer> docList = new ArrayList<Integer>();
		docList.addAll(docSet);
		Collections.sort(docList);

		BufferedZipPrintStream output = new BufferedZipPrintStream(
				"/shared/osprey/cqs-data/collected_data/" + args[0] + ".gz");
		ForwardIndexReader r = IndexReaderManager.getForwardIndexReader();
		int k = 0;
		for (Integer docid : docList) {
			if (k % 1000 == 0)
				System.out.println(k);
			FullDoc doc = r.getDoc(docid);
			output.println(docid + "\t" + doc.docId + "\t" + doc.url);
			output.println(doc.tokenList.getSeperatedText());
			output.println(doc.nerEntities.size());
			for (NEREntity e : doc.nerEntities)
				output.println(e.pos + "\t" + e.len + "\t" + e.type);
			output.println(doc.fbEntities.size());
			for (FBEntity e : doc.fbEntities) {
				output.print(e.pos + "\t" + e.len);
				for (int i = 0; i < e.ref.types.length; i++)
					output.print("\t" + e.ref.types[i]);
				output.println();
			}
			k++;
		}
		output.close();
	}

}
