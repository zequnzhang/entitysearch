package remote.node.impls;

import index.Entity;
import index.FBEntity;
import index.FullDoc;
import index.IndexReaderManager;
import index.NEREntity;
import index.reader.ForwardIndexReader;
import index.reader.PageRankReader;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import remote.DocInfo;
import remote.EntityInfo;
import remote.EntityTypeMapping;
import remote.node.interfaces.NodeDocumentStreamInterface;
import util.Pair;

public class NodeDocumentStream extends UnicastRemoteObject implements
		NodeDocumentStreamInterface, Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4659571140675035963L;
	private static final int BUFFER_SIZE = 600;
	private static final int WAKEUP_TIMES = 10;

	ArrayList<Integer> docList;
	DataRetriever<DocInfo> retriever;

	public NodeDocumentStream(ArrayList<Integer> docList)
			throws RemoteException {
		super();
		this.docList = docList;
		this.retriever = new DocumentRetriever(docList);
	}

	@Override
	public ArrayList<DocInfo> read() throws RemoteException {
		return retriever.read();
	}

	@Override
	public void close() throws RemoteException {
		retriever.close();
	}

	@Override
	public void run() {
		retriever.run();
	}

	class DocumentRetriever extends DataRetriever<DocInfo> {
		ArrayList<Integer> docList;

		DocumentRetriever(ArrayList<Integer> docList) {
			super(BUFFER_SIZE, WAKEUP_TIMES);
			this.docList = docList;
		}

		@Override
		public void run() {
			System.out.println("Start retriving " + docList.size() + " documents.");
			try {
				Collections.sort(docList);
				ForwardIndexReader indexReader = IndexReaderManager
						.getForwardIndexReader();
				PageRankReader prReader = IndexReaderManager.getPageRankReader();
				for (Integer doc : docList) {
					FullDoc fullDoc = indexReader.getDoc(doc);
					if (fullDoc == null)
						continue;
					DocInfo info = new DocInfo();
					info.id = doc;
					info.docId = fullDoc.docId;
					info.pagerank = prReader.getPageRank(doc);
					info.url = fullDoc.url;
					info.tokens = new String[fullDoc.tokenList.size()];
					for (int j = 0; j < fullDoc.tokenList.size(); j++)
						info.tokens[j] = fullDoc.tokenList.get(j).text;
					HashMap<Pair<Integer, Integer>, ArrayList<Integer>> l = new HashMap<>();
					for (int c = 0; c < 3; c++) {
						ArrayList<Entity> entityList = new ArrayList<>();
						if (c == 0)
							entityList.addAll(fullDoc.nerEntities);
						else if (c == 1)
							entityList.addAll(fullDoc.fbEntities);
						for (Entity entity : entityList) {
							Pair<Integer, Integer> p = new Pair<Integer, Integer>(
									entity.pos, entity.len);
							ArrayList<Integer> t = l.get(entity);
							if (t == null) {
								t = new ArrayList<>();
								l.put(p, t);
							}
							if (c == 0)
								t.add(EntityTypeMapping
										.getId(((NEREntity) entity).type));
							else if (c == 1) {
								FBEntity fe = (FBEntity) entity;
								for (int i = 0; i < fe.ref.types.length; i++)
									t.add(EntityTypeMapping
											.getMybaseTypeId(fe.ref.types[i]));
							}
						}
					}
					
					info.entities = new EntityInfo[l.size()];
					int k = 0;
					for (Pair<Integer, Integer> p : l.keySet()) {
						ArrayList<Integer> t = l.get(p);
						EntityInfo o = new EntityInfo();
						o.pos = p.first;
						o.len = p.second;
						o.types = new int[t.size()];
						int kk = 0;
						for (Integer v : t) {
							o.types[kk] = v;
							kk++;
						}
						info.entities[k] = o;
						k++;
					}
					Arrays.sort(info.entities);

					if (!addData(info)) {
						complete();
						return;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			complete();
		}

	}

}
