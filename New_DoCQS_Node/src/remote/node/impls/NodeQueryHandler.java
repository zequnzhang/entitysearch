package remote.node.impls;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import remote.Word;
import remote.node.interfaces.NodeDocumentStreamInterface;
import remote.node.interfaces.NodePatternStreamInterface;
import remote.node.interfaces.NodeQueryHandlerInterface;
import remote.node.interfaces.NodeWordStreamInterface;

public class NodeQueryHandler extends UnicastRemoteObject implements NodeQueryHandlerInterface {
	
	public NodeQueryHandler() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7395951109345746536L;

	@Override
	public NodePatternStreamInterface getNodePatternStream(String pattern)
			throws RemoteException {
		NodePatternStream patternStream=new NodePatternStream(pattern);
		Thread thread=new Thread(patternStream);
		thread.start();
		return patternStream;
	}

	@Override
	public NodeDocumentStreamInterface getNodeDocumentStream(ArrayList<Integer> docList)
			throws RemoteException {
		NodeDocumentStream documentStream=new NodeDocumentStream(docList);
		Thread thread=new Thread(documentStream);
		thread.start();
		return documentStream;
	}
	
	@Override	
	public NodeWordStreamInterface getNodeWordStream(Word[] words) throws RemoteException {
		NodeWordStream wordStream = new NodeWordStream(words);
		Thread thread = new Thread(wordStream);
		thread.start();
		return wordStream;
	}

	@Override
	public void ping() throws RemoteException {
	}

}
