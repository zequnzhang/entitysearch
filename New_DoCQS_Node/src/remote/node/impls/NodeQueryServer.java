package remote.node.impls;

import index.IndexReaderManager;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import conf.Constant;

public class NodeQueryServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length>=1)
			Constant.INDEX_LOCATION = args[0];
		try{
			IndexReaderManager.init();
			LocateRegistry.createRegistry(1100);
			NodeQueryHandler handler=new NodeQueryHandler();
			Naming.rebind("rmi://localhost:1100/query", handler);
			System.out.println("Query Server is ready.");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
