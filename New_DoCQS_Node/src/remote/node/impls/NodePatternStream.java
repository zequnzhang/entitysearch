package remote.node.impls;

import index.IndexReaderManager;
import index.Item;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import query.PatternParserManager;
import query.tree.reg.RegDataInfo;
import query.tree.reg.RegNode;
import remote.TokenItem;
import remote.node.interfaces.NodePatternStreamInterface;

public class NodePatternStream extends UnicastRemoteObject implements
		NodePatternStreamInterface, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5681128251918706976L;

	private static final int BUFFER_SIZE = 10000;
	private static final int WAKEUP_TIMES = 10;
	PatternRetriever retriever;

	public NodePatternStream(String pattern) throws RemoteException {
		super();
		this.retriever = new PatternRetriever(pattern);
	}

	@Override
	public ArrayList<TokenItem[]> read() throws RemoteException {
		return retriever.read();
	}

	@Override
	public void close() throws RemoteException {
		retriever.close();
	}

	@Override
	public void run() {
		retriever.run();
	}

	class PatternRetriever extends DataRetriever<TokenItem[]> {
		String pattern;

		PatternRetriever(String pattern) {
			super(BUFFER_SIZE, WAKEUP_TIMES);
			this.pattern = pattern;
		}

		@Override
		public void run() {
			System.out.println("Start Pattern Query " + pattern);
			try {
				RegNode node = PatternParserManager.parsePatternToNode(pattern);
				while (node.nextDoc()) {
					for (RegDataInfo info : node.getDataInfoList()) {
						TokenItem[] items = new TokenItem[info.entities.size()];
						for (int i = 0; i < items.length; i++) {
							Item item = info.entities.get(i);
							items[i] = new TokenItem(item.doc, item.pos,
									item.len, item.text,
									info.infos.get(i).schema);
						}
						if (!addData(items)) {
							complete();
							return;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			complete();
		}
	}

}
