package remote.node.impls;

import index.IndexReaderManager;
import index.reader.DocLengthReader;
import index.reader.PageRankReader;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import remote.Word;
import remote.WordFreq;
import remote.node.interfaces.NodeWordStreamInterface;

public class NodeWordStream extends UnicastRemoteObject implements
		NodeWordStreamInterface, Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9085464112898420402L;

	private static final int BUFFER_SIZE = 100000;
	private static final int WAKEUP_TIMES = 10;
	public final static IndexReaderManager manager = new IndexReaderManager();
	WordRetriever retriever;

	public NodeWordStream(Word[] words) throws RemoteException {
		super();
		this.retriever = new WordRetriever(words);
	}

	@Override
	public void run() {
		retriever.run();
	}

	@Override
	public ArrayList<WordFreq> read() throws RemoteException {
		return retriever.read();
	}

	@Override
	public void close() throws RemoteException {
		retriever.close();
	}

	class WordRetriever extends DataRetriever<WordFreq> {
		Word[] words;

		WordRetriever(Word[] words) {
			super(BUFFER_SIZE, WAKEUP_TIMES);
			this.words = words;
		}

		@Override
		public void run() {
			System.out.print("Start Word Query: ");
			for (int i = 0; i < words.length; i++) {
				if (i != 0)
					System.out.print(", ");
				System.out.print(words[i]);
			}
			System.out.println();

			try {
				PageRankReader prReader = manager.getPageRankReader();
				DocLengthReader docLenReader = manager.getDocLenReader();
						
				TermDocs[] tps = new TermDocs[words.length];
				for (int i = 0; i < words.length; i++)
					tps[i] = manager.getInvertedTextIndexReader().termDocs(
							new Term("Content", words[i].word));
				int[] docs = new int[words.length];
				for (int i = 0; i < docs.length; i++)
					docs[i] = -1;
				int currentDoc = -1;
				while (true) {
					int mini = Integer.MAX_VALUE;
					for (int i = 0; i < docs.length; i++) {
						if (currentDoc == docs[i]) {
							if (tps[i].next())
								docs[i] = tps[i].doc();
							else
								docs[i] = Integer.MAX_VALUE;
						}
						if (docs[i] < mini)
							mini = docs[i];
					}
					if (mini == Integer.MAX_VALUE)
						break;
					currentDoc = mini;

					boolean p = true;
					for (int i = 0; i < docs.length; i++) {
						int f = 0;
						if (docs[i] == currentDoc)
							f = tps[i].freq();
						if (words[i].mustAppear && f == 0) {
							p = false;
							break;
						}
					}
					if (!p)
						continue;

					WordFreq wordFreq = new WordFreq();
					wordFreq.doc = currentDoc;
					wordFreq.freqs = new int[words.length];
					wordFreq.pagerank = prReader.getPageRank(currentDoc);
					wordFreq.docLen = docLenReader.getDocLength(currentDoc);
					for (int i = 0; i < docs.length; i++) {
						if (docs[i] == currentDoc)
							wordFreq.freqs[i] = tps[i].freq();
						else
							wordFreq.freqs[i] = 0;
					}
					if (!addData(wordFreq)) {
						complete();
						return;
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			complete();
		}
	}
}
