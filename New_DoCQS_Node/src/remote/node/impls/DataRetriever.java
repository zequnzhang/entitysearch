package remote.node.impls;

import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.SynBuffer;

public abstract class DataRetriever<T> {
	private int bufferSize;
	private int wakeUpTimes;
	private boolean stop;
	private SynBuffer<T> buffer;
	private int total;

	DataRetriever(int bufferSize, int wakeUpTimes) {
		this.bufferSize = bufferSize;
		this.wakeUpTimes = wakeUpTimes;
		this.buffer = new SynBuffer<>();
		stop = false;
		total = 0;
	}

	public abstract void run();

	public boolean addData(T data) {
		if (stop)
			return false;
		int size = buffer.size();
		if (size >= bufferSize) {
			// If the buffer is full, go to sleep, wake up for
			// *WAKEUP_TIMES*, if still full, stop;
			int time = 0;
			for (; time < wakeUpTimes; time++) {
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();		System.out.println("Stream is closed");
				}
				size = buffer.size();
				if (size < bufferSize)
					break;
			}
			if (time >= wakeUpTimes) {
				System.out.println("Time out, close the stream.");
				stop = true;
				buffer.setNull();
				return false;
			}
			if (stop)
				return false;
		}
		buffer.add(data);
		total ++;
		return true;
	}

	public ArrayList<T> read() throws RemoteException {
		ArrayList<T> items = buffer.read();
		if (stop)
			buffer.setNull();
		return items;
	}
	
	public void complete() {
		stop = true;
		System.out.println("Retrieve " + total +" items.");
	}

	public void close() {
		stop = true;
		System.out.println("Stream is closed");
	}
}
