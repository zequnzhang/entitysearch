package remote.node.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.Word;

public interface NodeQueryHandlerInterface extends Remote{
	public NodePatternStreamInterface getNodePatternStream(String pattern) throws RemoteException;
	public NodeDocumentStreamInterface getNodeDocumentStream(ArrayList<Integer> docList) throws RemoteException;
	public NodeWordStreamInterface getNodeWordStream(Word[] words) throws RemoteException;
	public void ping() throws RemoteException;
}
