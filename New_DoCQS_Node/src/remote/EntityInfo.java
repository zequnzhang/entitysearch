package remote;

import java.io.Serializable;

public class EntityInfo implements Serializable, Comparable<EntityInfo>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6998380570779258016L;
	
	public int[] types;
	public int pos;
	public int len;
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(pos + ", "+len+":");
		for (int i = 0 ;i<types.length;i++)
			buf.append(" " + types[i]);
		return buf.toString();
	}

	@Override
	public int compareTo(EntityInfo o) {
		if (pos==o.pos)
			return len-o.len;
		return pos-o.pos;
	}
}
