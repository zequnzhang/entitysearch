package remote;

import java.io.Serializable;

import util.PorterStemmer;

public class Word implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5456504753023266719L;

	public static PorterStemmer stemmer = new PorterStemmer();
	public String word;
	public boolean mustAppear;

	public Word(String word, boolean stemmed, boolean mustAppear) {
		this.word = word;
		this.word = this.word.toLowerCase();
		if (stemmed)
			this.word = "#" + stemmer.stem(word);
		this.mustAppear = mustAppear;
	}

	public Word(String word1, String word2, boolean mustAppear) {
		this.word = word1 + " " + word2;
		this.word = this.word.toLowerCase();
		this.mustAppear = mustAppear;
	}

	public String toString() {
		return "[" + word + ", " + mustAppear + "]";
	}
}
