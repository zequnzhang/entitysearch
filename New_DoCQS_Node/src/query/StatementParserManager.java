package query;

import index.IndexReaderManager;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import exception.DoCQSException;

import query.parser.reg.SingleToken;
import query.parser.reg.Token;
import query.parser.stmt.AndCondition;
import query.parser.stmt.Condition;
import query.parser.stmt.EntityItem;
import query.parser.stmt.FuncCondition;
import query.parser.stmt.FuncExpr;
import query.parser.stmt.OrCondition;
import query.parser.stmt.QueryStatement;
import query.parser.stmt.Statement;
import query.parser.stmt.StatementLexer;
import query.parser.stmt.StatementParser;
import query.parser.stmt.StringExprImpl;
import query.parser.stmt.ViewStatement;
import query.plan.IndexPointer;
import query.plan.SimpleQueryPlan;
import query.tree.reg.RegNode;
import query.tree.reg.SingleRegNode;
import query.tree.stmt.Node;

public class StatementParserManager {
	static StatementLexer statementLexer=null;
	static StatementParser statementParser=null;
	public static Node parseQuery(String q, HashMap<String,String> viewStr) throws DoCQSException, IOException {
		Statement s=null;
		QueryStatement queryStatement;
		HashMap<String,ViewStatement> viewMap=new HashMap<String,ViewStatement>();
		
		if (statementLexer==null){
			statementLexer=new StatementLexer(new StringReader(q));
			statementParser=new StatementParser(statementLexer);
		}
		else
			statementLexer.yyreset(new StringReader(q));
		try {
			s=(Statement)statementParser.parse().value;
		} catch (Exception e) {
			throw new DoCQSException("Parser Exception: "+e.getMessage());
		}
//		System.out.println(s);
		if (!(s instanceof QueryStatement))
			throw new DoCQSException("Parser Exception: Query should be a select statement");
		queryStatement=(QueryStatement)s;
				
		for (EntityItem item:queryStatement.fromEntityItems){
			String view=null;
			if ((view=viewStr.get(item.oriEntity.substring(1)))!=null)
				viewMap.put(item.oriEntity,parseViewStatement(view));
		}
				
		queryStatement.rewriteQuery(viewMap);
		HashMap<FuncCondition, Token> patternMap=new HashMap<FuncCondition, Token>();
		parsePattern(queryStatement.whereCondition,patternMap);
		ArrayList<ArrayList<Token>> patternList=deriveTokenTreeSearcher(queryStatement.whereCondition,patternMap);
		
		if (patternList.size()==1 && patternList.get(0).size()==0)
			throw new DoCQSException("The statement does not contain patterns.");
		
		for (Token t:patternMap.values()){
			t.rewriteQuery(viewMap);
		}
		
		HashMap<FuncCondition, RegNode> patternRegMap=new HashMap<FuncCondition, RegNode>();
		ArrayList<RegNode> regNodeList=new ArrayList<RegNode>();
		for (FuncCondition func:patternMap.keySet()){
			RegNode node=patternMap.get(func).translate();
			regNodeList.add(node);
			patternRegMap.put(func, node);
		}
		Node node=queryStatement.translate(patternRegMap);
		
		//Calculate query plan;
		HashMap<SingleRegNode, IndexPointer> qp=new SimpleQueryPlan().computeQueryPlan(regNodeList);
		for (SingleRegNode regNode:qp.keySet())
			regNode.setIndexPointer(qp.get(regNode));
		return node;
	}
	
	static void parsePattern(Condition c, HashMap<FuncCondition,Token> patternMap) throws DoCQSException {
		if (c instanceof AndCondition){
			parsePattern(((AndCondition) c).c1,patternMap);
			parsePattern(((AndCondition) c).c2,patternMap);
		}
		if (c instanceof OrCondition){
			parsePattern(((OrCondition) c).c1,patternMap);
			parsePattern(((OrCondition) c).c2,patternMap);
		}
		if (c instanceof FuncCondition){
			FuncExpr funcExpr=((FuncCondition)c).funcExpr;
			if (funcExpr.funcID.toLowerCase().equals("pattern")
				&& funcExpr.paraList.size()==1
				&& funcExpr.paraList.get(0) instanceof StringExprImpl){
				String pattern=((StringExprImpl)(funcExpr.paraList.get(0))).str;
				Token t=PatternParserManager.parsePatternToToken(pattern);
				patternMap.put((FuncCondition)c, t);
			}
		}
	}
	
	public static ViewStatement parseViewStatement(String v) throws DoCQSException {
		Statement s;
		if (statementLexer==null){
			statementLexer=new StatementLexer(new StringReader(v));
			statementParser=new StatementParser(statementLexer);
		}
		else
			statementLexer.yyreset(new StringReader(v));
		try {
			s=(Statement)statementParser.parse().value;
		} catch (Exception e) {
			throw new DoCQSException("Parser Exception: "+e.getMessage());
		}
		if (!(s instanceof ViewStatement))
			throw new DoCQSException("Parser Exception: Data type defintion error.");
		ViewStatement result=(ViewStatement)s;
		deriveTokenTreeSearcher(result.condition, new HashMap<FuncCondition, Token>());
		result.definition=v;
		return result;
	}
	
	static ArrayList<ArrayList<Token>> deriveTokenTreeSearcher(Condition c, HashMap<FuncCondition, Token> patternMap) throws DoCQSException{
		ArrayList<ArrayList<Token>> result=new ArrayList<ArrayList<Token>>();
		if (c instanceof AndCondition){
			for (ArrayList<Token> l1:deriveTokenTreeSearcher(((AndCondition)c).c1,patternMap))
				for (ArrayList<Token> l2:deriveTokenTreeSearcher(((AndCondition)c).c2,patternMap)){
					ArrayList<Token> l=new ArrayList<Token>();
					l.addAll(l1);
					l.addAll(l2);
					result.add(l);
				}
		}
		else if (c instanceof OrCondition){
			result.addAll(deriveTokenTreeSearcher(((OrCondition)c).c1,patternMap));
			result.addAll(deriveTokenTreeSearcher(((OrCondition)c).c2,patternMap));
			Iterator<ArrayList<Token>> iter=result.iterator();
			while (iter.hasNext()){
				ArrayList<Token> l=iter.next();
				if (l.isEmpty()) iter.remove();
			}
		}
		else if (patternMap.get(c)!=null){
			ArrayList<Token> l=new ArrayList<Token>();
			Token t=patternMap.get(c);
			l.add(t);
			result.add(l);
		}
		for (int i=0;i<result.size();i++){
			int j;
			for (j=i+1;j<result.size();j++){
				if (isEqual(result.get(i),result.get(j))){
					result.remove(i);
					i--;
					break;
				}
			}
		}
		if (result.size()==0) result.add(new ArrayList<Token>());
		return result;
	}
	
	static boolean isEqual(ArrayList<Token> tl1,ArrayList<Token> tl2){
		if (tl1.size()==tl2.size()) return false;
		for (int i=0;i<tl1.size();i++)
			if (tl1.get(i)!=tl2.get(i)) return false;
		return true;		
	}

}
