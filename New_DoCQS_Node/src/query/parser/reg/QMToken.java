package query.parser.reg;

import java.util.HashMap;

import query.parser.stmt.ViewStatement;
import query.tree.reg.QMRegNode;
import query.tree.reg.RegNode;

import exception.DoCQSException;

public class QMToken extends Token {
	public int lowerBound,upperBound;
	public QMToken(int lowerBound, int upperBound){
		this.lowerBound=lowerBound;
		this.upperBound=upperBound;
	}
	public Integer getMaxSpan() {
		return upperBound;
	}

	public Integer getMinSpan() {
		return lowerBound;
	}

//	public SerialNode translate() throws IOException, EntityDatabaseException {
//		throw new EntityDatabaseException("Condition Error!");
//	}
	
	public String toString(){
		return "?<"+lowerBound+","+upperBound+">";
	}
	public RegNode translate()
			throws DoCQSException {
		return new QMRegNode(lowerBound,upperBound);
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		//Nothing here.
	}
}
