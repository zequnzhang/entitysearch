package query.parser.reg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import query.parser.stmt.ViewStatement;
import query.tree.reg.RegNode;
import query.tree.reg.SetRegNode;

import util.NumberUtil;
import exception.DoCQSException;

public class SetToken extends Token {
	public ArrayList<Token> tokenList;
	public int windowSize;
	
	public SetToken(ArrayList<Token> tokenList, int windowSize){
		this.tokenList=tokenList;
		this.windowSize=windowSize;
	}
	
	public Integer getMaxSpan() {
		return windowSize;
	}

	public String toString(){
		String result="[";
		for (int i=0;i<tokenList.size();i++)
			result+=tokenList.get(i)+" ";
		result+="]";
		return result;
	}

	public RegNode translate()
			throws DoCQSException {
		ArrayList<RegNode> nodeList=new ArrayList<RegNode>();
		for (int i=0;i<tokenList.size();i++)
			nodeList.add(tokenList.get(i).translate());
		return new SetRegNode(nodeList,windowSize, alias);
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		for (Token t:tokenList)
			t.rewriteQuery(viewMap);
	}
}
