package query.parser.reg;

import java.util.HashMap;

import exception.DoCQSException;

import query.parser.stmt.ViewStatement;
import query.tree.reg.RegNode;


public abstract class Token {
	public abstract void rewriteQuery(HashMap<String,ViewStatement> viewMap);
	public abstract RegNode translate() throws DoCQSException;
	public String alias=null;
}
