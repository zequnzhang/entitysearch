package query.parser.reg;

import java.util.HashMap;

import conf.Constant;

import query.parser.stmt.ViewStatement;
import query.tree.reg.KeywordRegNode;
import query.tree.reg.RegNode;
import query.tree.reg.SingleRegNode;
import exception.DoCQSException;

public class KeywordToken extends SingleToken {
	public String keyword;
	public KeywordToken(String keyword){
		this.keyword=keyword;
//		for (int i=0;i<Constant.STOP_WORDS.length;i++)
//			if (Constant.STOP_WORDS[i].equals(this.keyword))
//				System.err.println("Keyword "+keyword+" is a stop word, should not be included.");
	}

	public String toString(){
		return keyword/*+"[."+hashCode()+".]"*/;
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
	}

	@Override
	public RegNode translate() throws DoCQSException {
		return new KeywordRegNode(this.keyword, alias);
	}

}
