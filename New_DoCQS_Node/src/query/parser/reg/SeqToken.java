package query.parser.reg;

import java.util.ArrayList;
import java.util.HashMap;

import exception.DoCQSException;

import query.parser.stmt.ViewStatement;
import query.tree.reg.RegNode;
import query.tree.reg.SeqRegNode;

public class SeqToken extends Token {
	public ArrayList<Token> tokenList;

	public SeqToken(ArrayList<Token> tokenList) {
		this.tokenList = tokenList;
	}

	public String toString() {
		String result = "{";
		for (int i = 0; i < tokenList.size(); i++)
			result += tokenList.get(i) + " ";
		result += "}";
		return result;
	}
	public RegNode translate() throws DoCQSException {
		ArrayList<RegNode> nodeList = new ArrayList<RegNode>();
		for (int i = 0; i < tokenList.size(); i++)
			nodeList.add(tokenList.get(i).translate());

		return new SeqRegNode(nodeList, alias);
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		for (Token t : tokenList)
			t.rewriteQuery(viewMap);
	}
}
