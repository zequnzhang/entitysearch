package query.parser.reg;

import java.util.ArrayList;
import java.util.HashMap;

import query.parser.stmt.ViewStatement;
import query.tree.reg.OrRegNode;
import query.tree.reg.RegNode;
import exception.DoCQSException;

public class OrToken extends Token {
	public ArrayList<Token> orList;

	public OrToken(ArrayList<Token> orList) {
		this.orList = orList;
	}

	public String toString() {
		String result = "(";
		for (int i = 0; i < orList.size(); i++)
			result += orList.get(i) + "|";
		result += ")";
		return result;
	}

	public RegNode translate() throws DoCQSException {
		ArrayList<RegNode> nodeList = new ArrayList<RegNode>();
		for (int i = 0; i < orList.size(); i++)
			nodeList.add(orList.get(i).translate());
		return new OrRegNode(nodeList, alias);
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		for (Token t : orList)
			t.rewriteQuery(viewMap);
	}
}
