package query.parser.reg;

import java.util.ArrayList;
import java.util.HashMap;

import query.parser.stmt.ViewStatement;
import query.tree.reg.AffiliateNode;
import query.tree.reg.RegNode;
import exception.DoCQSException;

public class AffiliateToken extends Token{

	Token mainToken;
	ArrayList<Token> subTokens;
	int windowSize;
	
	public AffiliateToken(Token mainToken, ArrayList<Token> subTokens, int windowSize){
		this.mainToken=mainToken;
		this.subTokens=subTokens;
		this.windowSize=windowSize;		
	}
	
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		mainToken.rewriteQuery(viewMap);
		for (int i=0;i<subTokens.size();i++)
			subTokens.get(i).rewriteQuery(viewMap);		
	}

	@Override
	public RegNode translate() throws DoCQSException {
		ArrayList<RegNode> subNodes=new ArrayList<RegNode>();
		for (int i=0;i<subTokens.size();i++)
			subNodes.add(subTokens.get(i).translate());
		AffiliateNode node=new AffiliateNode(mainToken.translate(), subNodes, windowSize);
		return node;
	}

}
