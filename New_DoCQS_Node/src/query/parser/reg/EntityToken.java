package query.parser.reg;

import java.io.IOException;
import java.util.HashMap;

import conf.Constant;

import exception.DoCQSException;
import query.parser.stmt.ViewStatement;
import query.tree.reg.EntityRegNode;
import query.tree.reg.InRegNode;
import query.tree.reg.RegNode;

public class EntityToken extends SingleToken {
	public String entity;
	// public Token innerToken;
	public EntityToken(String entity/*, Token innerToken*/){
		this.entity=entity.toLowerCase();
//		this.innerToken=innerToken;
	}	
	public String toString(){
		return entity/*+"[."+hashCode()+".]"*/;
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		if (viewMap.get(entity)!=null){
			ViewStatement v=viewMap.get(entity);
			entity=v.originalEntity;
		}
//		if (innerToken!=null) innerToken.rewriteQuery(viewMap);
	}

	public RegNode translate() throws DoCQSException {
		RegNode srcNode = new EntityRegNode(entity);
		return srcNode;

	}
}
