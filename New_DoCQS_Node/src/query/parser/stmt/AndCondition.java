package query.parser.stmt;

import java.util.HashMap;

import exception.DoCQSException;

import query.parser.reg.Token;
import query.tree.reg.RegNode;
import query.tree.stmt.AndNode;
import query.tree.stmt.BoolNode;
import query.tree.stmt.SerialNode;

public class AndCondition extends Condition {
	public Condition c1,c2;
	public AndCondition(Condition c1, Condition c2){
		this.c1=c1;
		this.c2=c2;
	}
	
	public String toString(){
		String r=c1+" AND "+c2;
		if (weight!=null) r="("+r+")<"+weight+">";
		return r;
	}

	public boolean detectToken(HashMap<FuncCondition, RegNode> patternMap) {
		return c1.detectToken(patternMap) || c2.detectToken(patternMap);
	}

	public SerialNode translate(SerialNode srcNode, HashMap<FuncCondition, RegNode> patternMap) throws DoCQSException {
		boolean p1=c1.detectToken(patternMap),p2=c2.detectToken(patternMap);
		SerialNode r;
		if (p1 && p2)
			r=new AndNode(c1.translate(null, patternMap), c2.translate(null, patternMap));
		else if (p1)
			r=c2.translate(c1.translate(null, patternMap),patternMap);
		else if (p2)
			r=c1.translate(c2.translate(null, patternMap), patternMap);
		else
			r=new AndNode(c1.translate(srcNode, patternMap),c2.translate(srcNode, patternMap));
		if (weight!=null)
			r.setWeight(r.getWeight()*weight);
		return r;
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		c1.rewriteQuery(viewMap);
		c2.rewriteQuery(viewMap);
	}
}
