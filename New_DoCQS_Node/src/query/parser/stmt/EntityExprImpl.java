package query.parser.stmt;

import java.util.HashMap;

import query.tree.stmt.EntityExprNode;
import query.tree.stmt.EntityExprNodeImpl;

public class EntityExprImpl implements EntityExpr{
	public String entityID;
	public EntityExprImpl(String entityID){
		this.entityID=entityID;
	}
	public String toString(){
		return entityID;
	}
	public EntityExprNodeImpl translateExprNode(){
		return new EntityExprNodeImpl(entityID);
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap){
		if (viewMap.get(entityID)!=null)
			entityID=viewMap.get(entityID).originalEntity;			
	}
}
