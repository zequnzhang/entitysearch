package query.parser.stmt;

import java.util.HashMap;

public class EntityItem {
	public String oriEntity;
	public String entity;
	public EntityItem(String entity){
		this.oriEntity=entity;
		this.entity=entity;
	}
	public EntityItem(String entity,String oriEntity){
		this.oriEntity=oriEntity;
		this.entity=entity;
	}
	public String toString(){
		String r=entity;
		if (oriEntity!=null){
			r=oriEntity+" AS "+entity;
		}
		return r;
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap){
		if (viewMap.get(oriEntity)!=null){
			if (entity.equals(oriEntity))
				this.entity=viewMap.get(oriEntity).originalEntity;;
			this.oriEntity=viewMap.get(oriEntity).originalEntity;
		}
	}
}
