package query.parser.stmt;

import query.tree.stmt.BoolExprNode;
import exception.DoCQSException;

public interface BoolExpr extends Expr {
	public BoolExprNode translateExprNode() throws DoCQSException;
}
