package query.parser.stmt;

public class ViewStatement extends Statement {
	public String dataType;
	public String originalEntity;
	public Condition condition;
	public String definition;
	public ViewStatement(String dataType, String originalEntity, Condition condition){
		this.dataType=dataType;
		this.originalEntity=originalEntity;
		this.condition=condition;
	}
}
