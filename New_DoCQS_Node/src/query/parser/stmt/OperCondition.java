package query.parser.stmt;

import java.util.HashMap;

import exception.DoCQSException;

import query.parser.reg.Token;
import query.tree.reg.RegNode;
import query.tree.stmt.BoolExprNode;
import query.tree.stmt.BoolNode;
import query.tree.stmt.OperBoolNode;
import query.tree.stmt.SerialNode;

public class OperCondition extends Condition implements BoolExpr{
	
	public static final int EQ=0;
	public static final int LT=1;
	public static final int GT=2;
	public static final int LET=3;
	public static final int GET=4;
	public static final int NEQ=5;
	public static final String[] operStr={"=","<",">","<=",">=","!="};
	ValExpr e1,e2;
	int oper;
	
	public OperCondition(int oper, ValExpr e1, ValExpr e2){
		this.oper=oper;
		this.e1=e1;
		this.e2=e2;
	}
	
	public String toString(){
		String r=e1+operStr[oper]+e2;
		if (weight!=null) r="("+r+")<"+weight+">";
		return r;
	}

	public boolean detectToken(HashMap<FuncCondition, RegNode> patternMap) {
		return false;
	}

	public SerialNode translate(SerialNode srcNode,
			HashMap<FuncCondition, RegNode> patternMap) throws DoCQSException {
		OperBoolNode r=new OperBoolNode(oper, e1.translateExprNode(), e2.translateExprNode(), srcNode);
		if (weight!=null)
			r.setWeight(weight);
		return r;
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		e1.rewriteQuery(viewMap);
		e2.rewriteQuery(viewMap);
	}

	public BoolExprNode translateExprNode() throws DoCQSException {
		return new OperBoolNode(oper, e1.translateExprNode(), e2.translateExprNode(), null);
	}
}
