package query.parser.stmt;

import java.util.HashMap;

import exception.DoCQSException;

import query.parser.reg.Token;
import query.tree.reg.RegNode;
import query.tree.stmt.OrNode;
import query.tree.stmt.SerialNode;

public class OrCondition extends Condition {
	public Condition c1,c2;
	public OrCondition(Condition c1, Condition c2){
		this.c1=c1;
		this.c2=c2;
	}
	public String toString(){
		String r=c1+" OR "+c2;
		if (weight!=null) r="("+r+")<"+weight+">";
		return r;
	}
	public boolean detectToken(HashMap<FuncCondition, RegNode> patternMap) {
		return c1.detectToken(patternMap) && c2.detectToken(patternMap);
	}
	public SerialNode translate(SerialNode srcNode,
			HashMap<FuncCondition, RegNode> patternMap) throws DoCQSException {
		SerialNode n1,n2;
		if (c1.detectToken(patternMap))
			n1=c1.translate(null, patternMap);
		else
			n1=c1.translate(srcNode, patternMap);
		if (c2.detectToken(patternMap))
			n2=c2.translate(null, patternMap);
		else
			n2=c2.translate(srcNode, patternMap);
		SerialNode r=new OrNode(n1,n2);
		if (weight!=null) r.setWeight(weight);
		return r;
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		c1.rewriteQuery(viewMap);
		c2.rewriteQuery(viewMap);
	}
}
