package query.parser.stmt;

import java.util.ArrayList;
import java.util.HashMap;

import query.tree.reg.RegNode;
import query.tree.stmt.BoolNode;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncBoolNode;
import query.tree.stmt.FuncFuzzyNode;
import query.tree.stmt.SerialNode;
import exception.DoCQSException;

public class FuzzyCondition extends Condition {
	public String funcID;
	public ArrayList<Expr> paraList;
	
	public FuzzyCondition(String funcID, ArrayList<Expr> paraList) {
		this.funcID=funcID;
		this.paraList=paraList;
	}
	
	public boolean detectToken(HashMap<FuncCondition, RegNode> patternMap) {
		return false;
	}

	public SerialNode translate(SerialNode srcNode,
			HashMap<FuncCondition, RegNode> patternMap)
			throws DoCQSException {
		FuncFuzzyNode funcFuzzyNode=null;
		try {
			Class c=Class.forName("query.tree.func.FuzzyFunc"+funcID.substring(1).toUpperCase());
			funcFuzzyNode=(FuncFuzzyNode)c.newInstance();
			ExprNode[] paras=new ExprNode[paraList.size()];
			for (int i=0;i<paraList.size();i++) paras[i]=paraList.get(i).translateExprNode(); 
			funcFuzzyNode.initFunction(srcNode, paras);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DoCQSException("Function "+funcID+" Error!");
		}
		if (!funcFuzzyNode.checkParaTypes()) throw new DoCQSException("Function "+funcID+" Parameters Error.");
		if (weight!=null) funcFuzzyNode.setWeight(weight);
		return funcFuzzyNode;
	}
	public String toString(){
		String r=funcID+"(";
		for (int i=0;i<paraList.size();i++){
			if (i!=0) r+=",";
			r+=paraList.get(i);
		}
		r+=")";
		if (weight!=null) r="("+r+")<"+weight+">";
		return r;
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		for (Expr para:paraList)
			para.rewriteQuery(viewMap);
	}
}
