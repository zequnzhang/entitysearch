package query.parser.stmt;

import java.util.HashMap;
import java.util.HashSet;

import query.tree.stmt.OperValNode;
import exception.DoCQSException;

public class OperExpr implements ValExpr{
	public final static int PLUS=0;
	public final static int MINUS=1;
	public final static int TIMES=2;
	public final static int DIV=3;
	public final static String[] operStr={"+","-","*","/"};
	int oper;
	ValExpr e1,e2;
	
	public OperExpr(int oper, ValExpr e1, ValExpr e2){
		this.oper=oper;
		this.e1=e1;
		this.e2=e2;		
	}
	
	public String toString(){
		return e1+operStr[oper]+e2;
	}

	public OperValNode translateExprNode() throws DoCQSException {
		return new OperValNode(oper,e1.translateExprNode(),e2.translateExprNode());
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		e1.rewriteQuery(viewMap);
		e2.rewriteQuery(viewMap);
	}
}
