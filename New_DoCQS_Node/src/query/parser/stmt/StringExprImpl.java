package query.parser.stmt;

import java.util.HashMap;

import query.tree.stmt.StringExprNode;
import query.tree.stmt.StringExprNodeImpl;

public class StringExprImpl implements StringExpr{
	public String str;
	public StringExprImpl(String str){
		this.str=str;		
	}
	public String toString(){
		return "\""+str+"\"";
	}
	
	public StringExprNode translateExprNode(){
		return new StringExprNodeImpl(str);
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
	}
}
