package query.parser.stmt;

import exception.DoCQSException;
import query.tree.stmt.ValExprNode;

public interface ValExpr extends Expr{
	public ValExprNode translateExprNode() throws DoCQSException;
}
