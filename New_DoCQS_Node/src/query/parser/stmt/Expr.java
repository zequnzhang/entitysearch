package query.parser.stmt;

import java.util.HashMap;
import java.util.HashSet;

import exception.DoCQSException;
import query.tree.DataInfo;
import query.tree.stmt.ExprNode;

public interface Expr {
	public static final int STRING=0;
	public static final int ENTITY=1;
	public static final int NUMBER=2;
	public static final int BOOL=3;
	public ExprNode translateExprNode() throws DoCQSException;
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap);
}
