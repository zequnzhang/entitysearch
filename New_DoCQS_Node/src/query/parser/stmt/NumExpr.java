package query.parser.stmt;

import java.util.HashMap;
import java.util.HashSet;

import query.tree.stmt.NumValNode;

public class NumExpr implements ValExpr{
	String num;

	public NumExpr(String num) {
		this.num = num;
	}

	public String toString(){
		return num;
	}
	public NumValNode translateExprNode() {
		return new NumValNode(Double.parseDouble(num));
	}

	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
	}
	
}
