package query.parser.stmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import exception.DoCQSException;

import query.parser.reg.EntityToken;
import query.tree.reg.EntityRegNode;
import query.tree.reg.PureEntityRegNode;
import query.tree.reg.RegNode;
import query.tree.reg.SingleRegNode;
import query.tree.stmt.BatchNode;
import query.tree.stmt.EntityExprNode;
import query.tree.stmt.EntityExprNodeImpl;
import query.tree.stmt.ExprNode;
import query.tree.stmt.GroupNode;
import query.tree.stmt.Node;
import query.tree.stmt.OrderNode;
import query.tree.stmt.PatternNode;
import query.tree.stmt.SelectBatchNode;
import query.tree.stmt.SelectSerialNode;
import query.tree.stmt.SerialNode;
import query.tree.stmt.UniqueNode;

public class QueryStatement extends Statement{
	public ArrayList<SelectItem> selectItems;
	public ArrayList<EntityItem> fromEntityItems;
	public Condition whereCondition;
	public ArrayList<EntityExprImpl> groupEntity;
	public ValExpr orderExpr;	
	public boolean unique;
	
	public String toString(){
		String result="SELECT ";
		if (unique) result+="UNIQUE ";
		for (SelectItem item:selectItems)
			result+=item+" ";
		result+="\nFROM ";
		for (EntityItem item:fromEntityItems)
			result+=item+" ";
		if (whereCondition!=null) result+="\nWHERE "+whereCondition;
		if (groupEntity!=null) {
			result+="\nGROUP BY ";
			for (EntityExpr expr:groupEntity)
				result+=expr+" ";
		}
		if (orderExpr!=null)
			result+="\nORDER BY "+orderExpr;		
		return result;
	}
	
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap){
		for (SelectItem selectItem:selectItems)
			selectItem.expr.rewriteQuery(viewMap);
		for (EntityItem entityItem:fromEntityItems)
			entityItem.rewriteQuery(viewMap);
		if (whereCondition!=null)
			whereCondition.rewriteQuery(viewMap);
		if (groupEntity!=null)
			for (EntityExpr entityExpr:groupEntity)
				entityExpr.rewriteQuery(viewMap);
		if (orderExpr!=null)
			orderExpr.rewriteQuery(viewMap);
		for (String str:viewMap.keySet()){
			if (whereCondition==null)
				whereCondition=viewMap.get(str).condition;
			else
				whereCondition=new AndCondition(whereCondition,viewMap.get(str).condition);
		}
	}
	
	public Node translate(HashMap<FuncCondition, RegNode> patternMap) throws DoCQSException{
		ArrayList<SingleRegNode> entityNodeList=new ArrayList<SingleRegNode>();
		for (EntityItem item:fromEntityItems){
			EntityToken token=new EntityToken(item.oriEntity);
			SingleRegNode node=new EntityRegNode(token.entity);
			entityNodeList.add(node);
		}
		PatternNode pureEntityPatternNode=new PatternNode(new PureEntityRegNode(entityNodeList));
		
		Node node;
		if (whereCondition==null)
			node=pureEntityPatternNode;
		else
			node=whereCondition.translate(pureEntityPatternNode, patternMap);
		if (unique) node=new UniqueNode(node);
		if (groupEntity!=null){
			ArrayList<EntityExprNodeImpl> nodeList=new ArrayList<EntityExprNodeImpl>();
			for (EntityExprImpl entityExpr:groupEntity)
				nodeList.add(entityExpr.translateExprNode());
			node=new GroupNode(((SerialNode)node),nodeList);			
		}
		if (orderExpr!=null){
			node=new OrderNode(node,orderExpr.translateExprNode());
		}
		ArrayList<String> schemas=new ArrayList<String>();
		ArrayList<ExprNode> nodeList=new ArrayList<ExprNode>();
		for (SelectItem item:selectItems){
			if (item.name==null)
				schemas.add(item.expr.toString());
			else
				schemas.add(item.name);
			nodeList.add(item.expr.translateExprNode());
		}
		if (node instanceof SerialNode)
			return new SelectSerialNode((SerialNode)node,nodeList,schemas);
		else
			return new SelectBatchNode((BatchNode)node,nodeList,schemas);
		
	}
}
