package query.parser.stmt;

import java.util.HashMap;

import exception.DoCQSException;

import query.tree.reg.RegNode;
import query.tree.stmt.BoolNode;
import query.tree.stmt.SerialNode;

public abstract class Condition{
	public Double weight=null;
	public void setWeight(String weightStr){
		weight=Double.parseDouble(weightStr);		
	}
	public abstract boolean detectToken(HashMap<FuncCondition, RegNode> patternMap);
	public abstract SerialNode translate(SerialNode srcNode, HashMap<FuncCondition, RegNode> patternMap) throws DoCQSException;
	public BoolNode translateBoolNode() throws DoCQSException {
		throw new DoCQSException("Unreachable.");
	}
	public abstract void rewriteQuery(HashMap<String, ViewStatement> viewMap);
}
