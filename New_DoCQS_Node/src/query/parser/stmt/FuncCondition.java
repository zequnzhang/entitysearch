package query.parser.stmt;

import java.util.HashMap;
import exception.DoCQSException;
import query.tree.reg.RegNode;
import query.tree.stmt.FuncBoolNode;
import query.tree.stmt.PatternNode;
import query.tree.stmt.SerialNode;

public class FuncCondition extends Condition {

	public FuncExpr funcExpr;
	public FuncCondition(FuncExpr funcExpr){
		this.funcExpr=funcExpr;		
	}
	
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		funcExpr.rewriteQuery(viewMap);
	}

	public boolean detectToken(HashMap<FuncCondition, RegNode> patternMap) {
		return patternMap.containsKey(this);
	}

	public SerialNode translate(SerialNode srcNode,
			HashMap<FuncCondition, RegNode> patternMap)
			throws DoCQSException {
		if (patternMap!=null && patternMap.get(this)!=null){
			SerialNode node=new PatternNode(patternMap.get(this));
			if (weight!=null) node.setWeight(weight);
			return node;
		}
		return new FuncBoolNode(srcNode,funcExpr.translateExprNode());
	}

}
