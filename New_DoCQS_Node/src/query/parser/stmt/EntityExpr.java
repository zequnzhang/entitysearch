package query.parser.stmt;

import exception.DoCQSException;
import query.tree.stmt.EntityExprNode;

public interface EntityExpr extends Expr {
	public EntityExprNode translateExprNode() throws DoCQSException;
}
