package query.parser.stmt;

import exception.DoCQSException;
import query.tree.stmt.StringExprNode;

public interface StringExpr extends Expr {
	public StringExprNode translateExprNode() throws DoCQSException;
}
