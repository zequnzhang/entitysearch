package query.parser.stmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import exception.DoCQSException;

import query.tree.stmt.BoolExprNode;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncNode;
import query.tree.stmt.ValExprNode;

public class FuncExpr implements ValExpr, EntityExpr, StringExpr, BoolExpr{
	
	public String funcID;
	public ArrayList<Expr> paraList;
	public FuncExpr(String funcID, ArrayList<Expr> paraList){
		this.funcID=funcID;
		this.paraList=paraList;		
	}
	public FuncNode translateExprNode() throws DoCQSException{
		FuncNode funcNode=null;
		try {
			Class c=Class.forName("query.tree.func.Func"+funcID.toUpperCase());
			funcNode=(FuncNode)c.newInstance();
			ExprNode[] paras=new ExprNode[paraList.size()];
			for (int i=0;i<paraList.size();i++) paras[i]=paraList.get(i).translateExprNode();
			funcNode.initFunction(paras);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DoCQSException("Function "+funcID+" Error!");
		}
		if (!funcNode.checkParaTypes()) throw new DoCQSException("Function "+funcID+" Parameters Error.");
		return funcNode;
	}
	public String toString(){
		String r=funcID+"(";
		for (int i=0;i<paraList.size();i++){
			if (i!=0) r+=",";
			r+=paraList.get(i);
		}
		r+=")";
		return r;
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap) {
		for (Expr para:paraList)
			para.rewriteQuery(viewMap);
	}

}
