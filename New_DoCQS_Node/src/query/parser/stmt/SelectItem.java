package query.parser.stmt;

import java.util.HashMap;

public class SelectItem {
	public Expr expr;
	public String name;
	
	public SelectItem(Expr expr, String name){
		this.expr=expr;
		this.name=name;
	}
	
	public String toString(){
		if (name==null)
			return expr+"";
		else
			return expr+" AS "+name;
	}
	public void rewriteQuery(HashMap<String, ViewStatement> viewMap){
		expr.rewriteQuery(viewMap);
	}
}
