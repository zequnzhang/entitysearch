package query;

public class ItemInfo {
	public String str;
	public int pos;
	public int startOffset, endOffset;
	public ItemInfo(String str, int pos, int startOffset, int endOffset){
		this.str=str;
		this.pos=pos;
		this.startOffset=startOffset;
		this.endOffset=endOffset;		
	}
	
	public String toString(){
		return str+"(pos: "+pos+" start: "+startOffset+" endOffset: "+endOffset+")";
	}
}
