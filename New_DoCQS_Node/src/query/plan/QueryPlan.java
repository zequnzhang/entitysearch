package query.plan;

import index.IndexReaderManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import query.tree.reg.RegNode;
import query.tree.reg.SingleRegNode;

public interface QueryPlan {
	//Assign SingleRegNode with IndexPointer.
	HashMap<SingleRegNode, IndexPointer> computeQueryPlan(ArrayList<RegNode> node) throws IOException;
}
