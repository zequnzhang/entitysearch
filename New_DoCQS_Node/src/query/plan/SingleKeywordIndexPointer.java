package query.plan;

import java.io.IOException;

import org.apache.lucene.index.Term;

import index.IndexReaderManager;
import index.Item;

public class SingleKeywordIndexPointer extends KeywordIndexPointer{
	
	String keyword;	
	public SingleKeywordIndexPointer(String keyword) throws IOException{
		tp=IndexReaderManager.getInvertedTextIndexReader().termPositions(new Term("Content", keyword));
//		System.out.println(cluster+":"+keyword);
		this.keyword=keyword;
	}
	
	@Override
	Item[] parseData(int doc, int pos, byte[] buffer, int bufferLen) {
		Item[] items=new Item[1];
		items[0]=new Item(doc, pos, 1, keyword, null);
		return items;
	}
	
	public String toString(){
		return "$keyword<"+keyword+">";
	}
}
