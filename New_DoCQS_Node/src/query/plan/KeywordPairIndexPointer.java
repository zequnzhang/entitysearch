package query.plan;

import java.io.IOException;

import org.apache.lucene.index.Term;

import index.IndexReaderManager;
import index.Item;

public class KeywordPairIndexPointer extends KeywordIndexPointer{
	
	String keyword1;
	String keyword2;
	//Point to the first keyword or second.
	boolean getFirst;
	public KeywordPairIndexPointer(String keyword1, String keyword2, boolean getFirst) throws IOException{
		this.keyword1=keyword1;
		this.keyword2=keyword2;
		this.getFirst=getFirst;
		tp=IndexReaderManager.getInvertedTextIndexReader().termPositions(new Term("Content", keyword1+" "+keyword2));
	}

	Item[] parseData(int doc, int pos, byte[] buffer, int bufferLen) {
		Item[] items=new Item[1];
		if (getFirst)
			items[0]=new Item(doc, pos, 1, keyword1, null);
		else
			items[0]=new Item(doc, pos+1, 1, keyword2, null);
		return items;
	}
	
	public String toString(){
		return "$keyword-pair<"+keyword1+","+keyword2+","+(getFirst?"1":"2")+">";
	}

}
