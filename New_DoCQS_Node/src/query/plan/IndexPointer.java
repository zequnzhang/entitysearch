package query.plan;

import index.IndexReaderManager;
import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.index.TermPositions;

public abstract class IndexPointer {
	int currentDoc;
	ArrayList<Item> itemList;
	
	public IndexPointer(){
		currentDoc=-1;
		itemList=null;
	}
	
	public abstract boolean nextDoc() throws IOException;
	public int getCurrentDoc(){
		return currentDoc;
	}
	
	public abstract ArrayList<Item> getDataList() throws IOException;
	
	public abstract void skipTo(int doc) throws IOException;
}
