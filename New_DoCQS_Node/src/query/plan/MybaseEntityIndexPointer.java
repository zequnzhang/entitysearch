package query.plan;

import index.FBEntity;
import index.FullDoc;
import index.IndexReaderManager;
import index.TextTokenList;
import index.Item;
import index.reader.ForwardIndexReader;

import java.io.IOException;
import java.util.ArrayList;

public class MybaseEntityIndexPointer extends IndexPointer {
	String entity;
	ForwardIndexReader indexReader;
	int cid;
	int docNum;

	public MybaseEntityIndexPointer(String entity) throws IOException {
		this.entity = entity;
		this.indexReader = IndexReaderManager.getForwardIndexReader();
		this.docNum = indexReader.docNum();
		cid = -1;
		try {
			cid = Integer.parseInt(entity.split("\\.")[1]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean nextDoc() throws IOException {
		currentDoc++;
		itemList = null;
		if (currentDoc >= docNum)
			return false;
		return true;
	}

	@Override
	public ArrayList<Item> getDataList() throws IOException {
		if (currentDoc == -1 || currentDoc >= docNum)
			return null;
		if (itemList == null) {
			itemList = new ArrayList<Item>();
			FullDoc fullDoc = indexReader.getDoc(currentDoc);
			ArrayList<FBEntity> elist = fullDoc.fbEntities;
			TextTokenList tl = fullDoc.tokenList;
			for (FBEntity e : elist) {
				boolean p = false;
				for (int i = 0; i < e.ref.types.length; i++)
					if (e.ref.types[i] == cid) {
						p = true;
						break;
					}
				if (p) {
					Item item = new Item(currentDoc, e.pos, e.len, tl.getText(
							e.pos, e.len), entity);
					itemList.add(item);
				}
			}
		}
		return itemList;
	}

	@Override
	public void skipTo(int doc) throws IOException {
		currentDoc = doc;
		itemList = null;
	}

}
