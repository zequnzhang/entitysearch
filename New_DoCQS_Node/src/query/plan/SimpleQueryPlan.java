package query.plan;

import index.IndexReaderManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import query.tree.reg.EntityRegNode;
import query.tree.reg.KeywordRegNode;
import query.tree.reg.RegNode;
import query.tree.reg.SeqRegNode;
import query.tree.reg.SingleRegNode;

public class SimpleQueryPlan implements QueryPlan{

	public HashMap<SingleRegNode, IndexPointer> computeQueryPlan(ArrayList<RegNode> nodes) throws IOException {
		HashMap<SingleRegNode, IndexPointer> map=new HashMap<SingleRegNode, IndexPointer>();
		for (RegNode node:nodes){
			checkRegNode(node,map);
		}
		for (SingleRegNode n:map.keySet()){
			n.setIndexPointer(map.get(n));
		}
		return map;
	}
	
	public HashMap<SingleRegNode, IndexPointer> computeQueryPlan(RegNode node) throws IOException {
		HashMap<SingleRegNode, IndexPointer> map=new HashMap<SingleRegNode, IndexPointer>();
		checkRegNode(node,map);
		for (SingleRegNode n:map.keySet()){
			n.setIndexPointer(map.get(n));
		}		
		return map;
	}
	
	
	void checkRegNode(RegNode node, HashMap<SingleRegNode, IndexPointer> map) throws IOException{
		if (node instanceof SeqRegNode){
			checkSeqRegNode((SeqRegNode)node, map);
		}
		else if (node instanceof SingleRegNode){
			checkSingleRegNode((SingleRegNode)node, map);
		}
		else {
			ArrayList<RegNode> list=node.getSubRegNode();
			if (list!=null){
				for (RegNode n:list){
					checkRegNode(n,map);
				}
			}
		}
	}
	
	void checkSeqRegNode(SeqRegNode node, HashMap<SingleRegNode, IndexPointer> map) throws IOException{
		boolean[] covered=new boolean[node.nodeList.size()];
		Arrays.fill(covered, false);
		for (int i=0;i<node.nodeList.size();i++){
			if (covered[i]) continue;
			RegNode n=node.nodeList.get(i);
			if (n instanceof KeywordRegNode){
				KeywordRegNode kn=(KeywordRegNode)n;
				if (i<node.nodeList.size()-1 && node.nodeList.get(i+1) instanceof KeywordRegNode){
					KeywordPairIndexPointer p=new KeywordPairIndexPointer(kn.keyword, ((KeywordRegNode)node.nodeList.get(i+1)).keyword, true);
					map.put(kn, p);
					p=new KeywordPairIndexPointer(kn.keyword, ((KeywordRegNode)node.nodeList.get(i+1)).keyword, false);
					map.put(((KeywordRegNode)node.nodeList.get(i+1)), p);
					covered[i+1]=true;
				}
				else if (i>0 && node.nodeList.get(i-1) instanceof KeywordRegNode){
					KeywordPairIndexPointer p=new KeywordPairIndexPointer(((KeywordRegNode)node.nodeList.get(i-1)).keyword, kn.keyword, false);
					map.put(kn, p);					
				}
				else{
					map.put(kn, new SingleKeywordIndexPointer(kn.keyword));
				}
			}
			else{
				checkRegNode(n,map);
			}
			covered[i]=true;
		}
	}
	
	void checkSingleRegNode(SingleRegNode node, HashMap<SingleRegNode, IndexPointer> map) throws IOException{
		if (node instanceof KeywordRegNode)
			map.put(node, new SingleKeywordIndexPointer(((KeywordRegNode)node).keyword));			
		else if (node instanceof EntityRegNode){
			String entityName = ((EntityRegNode)node).entityName;
			if (entityName.startsWith("#freebase"))
				map.put(node, new MybaseEntityIndexPointer(entityName));
			else
				map.put(node, new NEREntityIndexPointer(entityName));			
		}
		else assert(false);
	}

}
