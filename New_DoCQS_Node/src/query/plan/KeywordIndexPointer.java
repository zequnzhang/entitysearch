package query.plan;

import index.IndexReaderManager;
import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.index.TermPositions;

public abstract class KeywordIndexPointer extends IndexPointer {
	TermPositions tp;
	byte[] buffer;

	public KeywordIndexPointer(){
		buffer=new byte[10000];
	}
	
	public boolean nextDoc() throws IOException{
		if (currentDoc==Integer.MAX_VALUE) return false;
		itemList=null;
		boolean p=tp.next();
		if (p) 
			currentDoc=tp.doc();
		else
			currentDoc=Integer.MAX_VALUE;
		return p;		
	}	
	
	public ArrayList<Item> getDataList() throws IOException{
		if (currentDoc==-1 || currentDoc==Integer.MAX_VALUE) return null;
		if (itemList==null){
			itemList=new ArrayList<Item>();
			for (int i=0;i<tp.freq();i++){
				int pos=tp.nextPosition();
				int payloadLen=tp.getPayloadLength();
				if (payloadLen>10000) payloadLen=0;
				if (payloadLen>0)
					tp.getPayload(buffer, 0);
					
				Item[] items=parseData(tp.doc(), pos, buffer, payloadLen);
				for (int j=0;j<items.length;j++)
					itemList.add(items[j]);
			}
		}
		return itemList;
	}
	
	public void skipTo(int doc) throws IOException{
		if (currentDoc>=doc) return;
		if (tp.skipTo(doc))
			currentDoc=tp.doc();
		else
			currentDoc=Integer.MAX_VALUE;
	}
	
	abstract Item[] parseData(int doc, int pos, byte[] buffer, int bufferLen);

}
