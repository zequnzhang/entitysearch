package query.plan;

import index.FullDoc;
import index.IndexReaderManager;
import index.NEREntity;
import index.TextTokenList;
import index.Item;
import index.reader.ForwardIndexReader;

import java.io.IOException;
import java.util.ArrayList;

public class NEREntityIndexPointer extends IndexPointer {
	String entity;
	ForwardIndexReader indexReader;

	int docNum;

	public NEREntityIndexPointer(String entity)
			throws IOException {
		this.entity = entity;
		this.indexReader = IndexReaderManager.getForwardIndexReader();
		this.docNum = indexReader.docNum();
	}

	@Override
	public boolean nextDoc() throws IOException {
		currentDoc++;
		itemList = null;
		if (currentDoc >= docNum)
			return false;
		return true;
	}

	@Override
	public ArrayList<Item> getDataList() throws IOException {
		if (currentDoc == -1 || currentDoc >= docNum)
			return null;
		if (itemList == null) {
			itemList = new ArrayList<Item>();
			FullDoc fullDoc = indexReader.getDoc(currentDoc);
			ArrayList<NEREntity> elist = fullDoc.nerEntities;
			TextTokenList tl = fullDoc.tokenList;
			for (NEREntity e : elist) {
				if (e.type.equals(entity)) {
					Item item = new Item(currentDoc, e.pos, e.len, tl.getText(
							e.pos, e.len), entity);
					itemList.add(item);
				}
			}
		}
		return itemList;
	}

	@Override
	public void skipTo(int doc) throws IOException {
		currentDoc = doc;
		itemList = null;
	}

}
