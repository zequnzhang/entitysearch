package query.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import exception.DoCQSException;
import query.PatternParserManager;
import query.tree.reg.RegDataInfo;
import query.tree.reg.RegNode;
import query.tree.reg.SeqRegNode;
import query.tree.reg.SetRegNode;
import query.tree.reg.SingleRegNode;
import util.PorterStemmer;
import util.SampleList;
import index.IndexReaderManager;
import index.TextToken;
import index.TextTokenList;
import index.Item;
import index.tokenizer.DoCQSTokenizer;

public class QueryUtil {

	public static DoCQSTokenizer tokenizer=new DoCQSTokenizer();
	public static PorterStemmer stemmer=new PorterStemmer();
	
	public static String stem(String word){
		return stemmer.stem(word);
	}
	
	public static TextTokenList query2TokenList(String query) throws IOException{
		return tokenizer.parseText(query);
	}
	
	public static String query2Pattern(String query) throws IOException{
		TextTokenList tokenList=query2TokenList(query);
		String pattern="{";
		for (int i=0;i<tokenList.size();i++){
			if (i!=0) pattern+=" ";
			pattern+=tokenList.get(i);
		}
		return pattern+"}";		
	}
	
	public static RegNode query2RegNode(String query) throws IOException, DoCQSException{
		query=query.replaceAll("#", " ");
		RegNode node;
		TextTokenList queryList=query2TokenList(query);
		ArrayList<RegNode> nlist=new ArrayList<RegNode>();
		for (int i=0;i<queryList.size();i++){
			nlist.add(SingleRegNode.createSingleRegNode(queryList.get(i).text));
		}
		if (nlist.size()==1)
			node=nlist.get(0);
		else
			node=new SeqRegNode(nlist, null);
		return node;
		
	}
	
	public static ArrayList<TermPair> getTermList(String query, String ans, int window, int sampleSize) throws DoCQSException, IOException{
		RegNode queryNode = PatternParserManager.parsePatternToNode(QueryUtil
				.query2Pattern(query));
		RegNode ansNode = PatternParserManager.parsePatternToNode(QueryUtil
				.query2Pattern(ans));
		queryNode.setAlias("query");
		ansNode.setAlias("answer");
		ArrayList<RegNode> nodeList=new ArrayList<RegNode>();
		nodeList.add(queryNode);
		nodeList.add(ansNode);
		SetRegNode node=new SetRegNode(nodeList, window, null);
		SampleList<TermPair> termList = new SampleList<TermPair>(sampleSize);
		while (node.nextDoc()){
			for (RegDataInfo d:node.getDataInfoList()){
				TermPair tp=new TermPair();
				tp.doc=d.getDoc();
				tp.query=query;
				tp.answer=ans;
				Item queryItem=d.getItem("query");
				Item ansItem=d.getItem("answer");
				tp.queryPos=queryItem.pos;
				tp.queryLen=queryItem.len;
				tp.ansPos=ansItem.pos;
				tp.ansLen=ansItem.len;
				termList.addItemWithSampling(tp);
			}
		}
		return termList;
	}
	
	public static ArrayList<Item> getItemList(String query, int sampleSize) throws DoCQSException, IOException{
		RegNode queryNode=PatternParserManager.parsePatternToNode(QueryUtil.query2Pattern(query));
		SampleList<Item> itemList=new SampleList<Item>(sampleSize);
		while (queryNode.nextDoc()){
			for (RegDataInfo d:queryNode.getDataInfoList()){
				itemList.addItemWithSampling(d.basedItem);
			}
		}
		return itemList;	
	}
	
	//For long phrase, just estimate
	public static int docFreq(IndexReaderManager manager, String word) throws IOException{
		word=word.toLowerCase();
		TextTokenList tl=tokenizer.parseText(word);
		if (tl.size()==1) return manager.getInvertedTextIndexReader().docFreq(new Term("Content", tl.get(0).text));
		int freq=Integer.MAX_VALUE;
		for (int i=0;i<tl.size()-1;i++){
			String w=tl.get(i).text+" "+tl.get(i+1).text;
			int f=manager.getInvertedTextIndexReader().docFreq(new Term("Content", w));
			if (f<freq) freq=f;
		}
		return freq;
	}
	
	//For long phrase, just estimate
	public static HashMap<Integer, Integer> tfMap(IndexReaderManager manager, String word, HashSet<Integer> docSet) throws IOException{
		TextTokenList tl=tokenizer.parseText(word.toLowerCase());
		ArrayList<String> list=new ArrayList<String>();
		if (tl.size()==1) list.add(tl.get(0).text);
		else
			for (int i=0;i<tl.size()-1;i++)
				list.add(tl.get(i).text+" "+tl.get(i+1).text);
		
		HashMap<Integer, Integer> result=new HashMap<Integer, Integer>();
		for (Integer doc:docSet)
			result.put(doc, Integer.MAX_VALUE);

		for (String w:list){
			TermDocs docs=manager.getInvertedTextIndexReader().termDocs(new Term("Content", w));
			HashMap<Integer, Integer> tmp=new HashMap<Integer, Integer>();
			while (docs.next()){
				if (docSet.contains(docs.doc()))
					tmp.put(docs.doc(), docs.freq());
			}
			for (Integer id:result.keySet()){
				if (tmp.containsKey(id)){
					int oriFreq=result.get(id);
					int newFreq=tmp.get(id);
					if (newFreq<oriFreq) result.put(id, newFreq);
				}
				else
					result.put(id, 0);
			}
		}
		return result;
		
	}
	
	public static void main(String[] args) throws IOException {
//		System.out.println(docFreq("a dream"));
	}
}

