package query.util;

public class TermPair {
	public int doc;
	public int queryPos;
	public int ansPos;
	public int queryLen;
	public int ansLen;
	public String query;
	public String answer;

	public String toString() {
		String s = "" + doc + ",(" + queryPos + ","+ queryLen+"), (" + ansPos+","+ansLen+")";
		return s;
	}
}
