package query.tree.func;

import java.util.ArrayList;

import exception.DoCQSException;

import query.tree.DataInfo;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncNode;
import query.tree.stmt.ValNode;


public class FuncCONF extends FuncNode {
	public FuncCONF(){
		paraTypes=new int[0];
		resultType=ExprNode.NUMBER;	
		funcID="conf";
	}

	public ValNode getFunctionResult(DataInfo dataInfo)
			throws DoCQSException {
		return new ValNode(dataInfo.conf); 
	}
}
