package query.tree.func;


import index.Item;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.DataInfo;
import query.tree.stmt.EntityExprNode;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncNode;
import query.tree.stmt.ValNode;

public class FuncCONTEXT extends FuncNode {
	
	static final int WINDOW=20; 
	
	public FuncCONTEXT(){
		paraTypes=new int[]{ExprNode.ENTITY};
		resultType=ExprNode.ENTITY;
	}
	
	public ValNode getFunctionResult(DataInfo dataInfo)
			throws DoCQSException, IOException {
		EntityExprNode e=(EntityExprNode)paras[0];
		Item item=e.getEntityItem(dataInfo);
		if (item==null) return null;
		int pos=item.pos-WINDOW/2;
		if (pos<0) pos=0;
		return new ValNode(new Item(item.doc,pos,WINDOW, null,null));
	}

}
