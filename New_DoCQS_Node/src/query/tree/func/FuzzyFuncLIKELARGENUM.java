package query.tree.func;

import index.Item;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.BasicDataInfo;
import query.tree.stmt.EntityExprNode;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncBoolNode;
import query.tree.stmt.FuncFuzzyNode;

public class FuzzyFuncLIKELARGENUM extends FuncFuzzyNode{
	
	public FuzzyFuncLIKELARGENUM(){
		this.paraTypes=new int[]{ExprNode.ENTITY};
	}

	public double getWeighting(BasicDataInfo d) throws DoCQSException, IOException {
		double res=0;
		Item item=((EntityExprNode)paras[0]).getEntityItem(d);
		if (item==null) throw new DoCQSException("Variable Error!");
		String text=item.getText().trim().toLowerCase();
		//Scale non-digit -> large digit (0 -> 1)
		int i,nc=0;
		boolean counting=true;
		for (i=0;i<text.length();i++){
			if (text.charAt(i)=='.') counting=false;
			if (Character.isDigit(text.charAt(i)) && counting) nc++;
			if (Character.isLetter(text.charAt(i))) break;
		}
		if (i<text.length()){
			res=0.1;
			if (text.contains("hundred")) res=0.2;
			if (text.contains("thousand")) res=0.4;
			if (text.contains("million")) res=0.7;
			if (text.contains("billion") || text.contains("trillion")) res=1;
		}
		else{
			if (nc>10) res=1;
			else res=nc/10.0;
		}
		return res;
		
	}

}
