package query.tree.func;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.BasicDataInfo;
import query.tree.stmt.BoolExprNode;
import query.tree.stmt.BoolNode;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncFuzzyNode;
import query.tree.stmt.ValExprNode;

public class FuzzyFuncPREFER extends FuncFuzzyNode {
	BoolExprNode condition;
	ValExprNode w1,w2;
	public FuzzyFuncPREFER(){
		this.paraTypes=new int[]{ExprNode.BOOL,ExprNode.NUMBER,ExprNode.NUMBER};
		condition=null;
	}

	public double getWeighting(BasicDataInfo dataInfo)
			throws DoCQSException, IOException {
		if (condition==null){
			condition=(BoolExprNode)paras[0];
			w1=(ValExprNode)paras[1];
			w2=(ValExprNode)paras[2];
		}
		if (condition.getBoolValue(dataInfo))
			return w1.calculate(dataInfo);
		else
			return w2.calculate(dataInfo);
	}

}
