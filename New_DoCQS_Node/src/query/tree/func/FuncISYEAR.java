package query.tree.func;

import index.Item;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.BasicDataInfo;
import query.tree.DataInfo;
import query.tree.stmt.EntityExprNode;
import query.tree.stmt.ExprNode;
import query.tree.stmt.FuncBoolNode;
import query.tree.stmt.FuncNode;
import query.tree.stmt.ValNode;

public class FuncISYEAR extends FuncNode {

	public FuncISYEAR(){
		this.paraTypes=new int[]{ExprNode.ENTITY};
		this.resultType=ExprNode.BOOL;
		funcID="isYear";
	}

	public ValNode getFunctionResult(DataInfo dataInfo) throws DoCQSException, IOException {
		Item item=((EntityExprNode)this.paras[0]).getEntityItem(dataInfo);
		if (item==null) return new ValNode(false);
		String val=item.getText();
		for (int i=0;i<val.length();i++)
			if (!Character.isDigit(val.charAt(i))) return new ValNode(false);
		if (val.length()!=4) return new ValNode(false);
		Integer year=Integer.parseInt(val);
		if (year>=1000 && year<=2200) return new ValNode(true);
		return new ValNode(false);		
	}
}
