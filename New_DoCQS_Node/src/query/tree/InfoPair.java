package query.tree;

public class InfoPair {
	public String schema;
	public String value;
	
	public InfoPair(String schema, String value){
		this.schema=schema;
		this.value=value;
	}
	
	public String toString(){
		return schema+":"+value;
	}
}
