package query.tree.stmt;

import index.Item;

import java.io.IOException;

public class ValNode {
	public static final int STRING=0;
	public static final int ENTITY=1;
	public static final int NUMBER=2;
	public static final int BOOL=3;
	public int type;
	double numVal;
	String strVal;
	Item entityVal;
	boolean boolVal;
	
	public ValNode(ValNode val){
		this.type=val.type;
		this.numVal=val.numVal;
		this.entityVal=val.entityVal;
		this.strVal=val.strVal;
		this.boolVal=val.boolVal;
	}
	public ValNode(String str){
		this.type=STRING;
		this.strVal=str;
	}
	public ValNode(Item entity){
		this.type=ENTITY;
		this.entityVal=entity;
	}
	public ValNode(double num){
		this.type=NUMBER;
		this.numVal=num;
	}
	public ValNode(boolean bool){
		this.type=BOOL;
		this.boolVal=bool;
	}
	
	public String toString(){
		try {
			switch (type){
			case ENTITY:	return entityVal.getText();
			case STRING: 	return strVal;
			case NUMBER:	return ""+numVal;
			case BOOL:		return ""+boolVal;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
