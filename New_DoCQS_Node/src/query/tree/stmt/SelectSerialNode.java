package query.tree.stmt;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import query.tree.BasicDataInfo;
import query.tree.InfoPair;
import exception.DoCQSException;

public class SelectSerialNode extends SerialNode{
	ArrayList<ExprNode> exprNodeList;
	ArrayList<String> schemas;
	SerialNode srcNode;
	BasicDataInfo currentDataInfo;
	
	public SelectSerialNode(SerialNode srcNode, ArrayList<ExprNode> exprNodeList, ArrayList<String> schemas){
		this.srcNode=srcNode;
		this.schemas=schemas;
		this.exprNodeList=exprNodeList;
		currentDataInfo=null;
	}
	
	public int getCurrentDoc() {
		return srcNode.getCurrentDoc();
	}

	public BasicDataInfo getCurrentPosition() {
		return currentDataInfo;
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		return srcNode.nextDoc();
	}

	public boolean nextPosition() throws DoCQSException, IOException {
		boolean p=srcNode.nextPosition();
		BasicDataInfo d=srcNode.getCurrentPosition();
		if (p){
			int n=schemas.size();
			BasicDataInfo currentDataInfo=new BasicDataInfo();
			currentDataInfo.entities=new ArrayList<Item>();
			currentDataInfo.infos=new ArrayList<InfoPair>();
			for (int i=0;i<n;i++){
				String schema=schemas.get(i);
				String value=null;
				Item item=null;
				ExprNode exprNode=exprNodeList.get(i);
				if (exprNode.getExprNodeType()==ExprNode.ENTITY){
					item=((EntityExprNode)exprNode).getEntityItem(d);
				}
				else{
					value=exprNodeList.get(i).getValue(d);
				}
				currentDataInfo.entities.add(item);
				currentDataInfo.infos.add(new InfoPair(schema, value));
			}
			this.currentDataInfo=currentDataInfo;
		}
		else
			currentDataInfo=null;
		return p;
	}
}
