package query.tree.stmt;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import exception.DoCQSException;
import query.tree.BasicDataInfo;
import query.tree.InfoPair;
public class AndNode extends SerialNode{
	
	SerialNode n1,n2;
	int currentDoc;
	ArrayList<BasicDataInfo> dataInfoList;
	int p;
	
	public AndNode(SerialNode n1, SerialNode n2){
		this.n1=n2;
		this.n2=n1;
		currentDoc=-1;
		p=-1;
		dataInfoList=null;
	}

	public int getCurrentDoc() {
		return currentDoc;
	}

	public BasicDataInfo getCurrentPosition() {
		if (p<0 || p>=dataInfoList.size()) return null;
		return dataInfoList.get(p);
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		dataInfoList=null;
		p=-1;
		if (!n1.nextDoc()) return false;
		while (true){
			if (n1.getCurrentDoc()==n2.getCurrentDoc()){
				currentDoc=n1.getCurrentDoc();
				return true;
			}
			else if (n1.getCurrentDoc()<n2.getCurrentDoc()){
				if (!(n1.nextDoc())) {
					currentDoc=-1;
					return false;
				}
			}
			else {
				if (!(n2.nextDoc())) {
					currentDoc=-1;
					return false;
				}
			}			
		}
	}

	public boolean nextPosition() throws IOException, DoCQSException {
		if (dataInfoList==null){
			dataInfoList=new ArrayList<BasicDataInfo>();
			ArrayList<BasicDataInfo> l1=new ArrayList<BasicDataInfo>(), 
				l2=new ArrayList<BasicDataInfo>();
			while (n1.nextPosition())
				l1.add(n1.getCurrentPosition());
			
			if (l1.size()==0) return false;
			
			while (n2.nextPosition())
				l2.add(n2.getCurrentPosition());
			for (BasicDataInfo d1:l1){
				for (BasicDataInfo d2:l2){
					ArrayList<InfoPair> infos=new ArrayList<InfoPair>();
					ArrayList<Item> entities=new ArrayList<Item>();
					infos.addAll(d1.infos);
					entities.addAll(d1.entities);
					
					boolean p=true;
					for (int i=0;i<d2.infos.size();i++){
						int j;
						for (j=0;j<infos.size();j++)
							if (d2.infos.get(i).schema.equals(infos.get(j).schema)) break;
						if (j>=infos.size()){
							infos.add(d2.infos.get(i));
							entities.add(d2.entities.get(i));
						}
						else{
							if (!entities.get(j).equals(d2.entities.get(i))) {
//								System.out.println(entities.get(j)+"....."+d2.entities[i]);
								p=false;
								break;
							}
						}
					}
					if (p){
						BasicDataInfo r=new BasicDataInfo();
						r.infos=infos;
						r.entities=entities;
						r.conf=d1.conf*d2.conf;
						dataInfoList.add(r);
					}
				}
			}
			p=-1;
		}
		p++;
		return p<dataInfoList.size();
	}

}
