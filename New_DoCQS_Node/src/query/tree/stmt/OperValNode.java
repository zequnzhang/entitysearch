package query.tree.stmt;

import java.io.IOException;

import exception.DoCQSException;
import query.parser.stmt.OperExpr;
import query.tree.DataInfo;

public class OperValNode implements ValExprNode {
	public ValExprNode n1, n2;

	public final static int PLUS = OperExpr.PLUS;

	public final static int MINUS = OperExpr.MINUS;

	public final static int TIMES = OperExpr.TIMES;

	public final static int DIV = OperExpr.DIV;

	int oper;

	public OperValNode(int oper, ValExprNode n1, ValExprNode n2) {
		this.oper = oper;
		this.n1 = n1;
		this.n2 = n2;
	}

	public double calculate(DataInfo dataInfo) throws DoCQSException, IOException {
		switch (oper) {
		case PLUS:
			return n1.calculate(dataInfo) + n2.calculate(dataInfo);
		case MINUS:
			return n1.calculate(dataInfo) - n2.calculate(dataInfo);
		case TIMES:
			return n1.calculate(dataInfo) * n2.calculate(dataInfo);
		case DIV:
			return n1.calculate(dataInfo) / n2.calculate(dataInfo);
		}
		return 0;
	}
	
	public String getValue(DataInfo dataInfo) throws DoCQSException, IOException {
		return calculate(dataInfo)+"";
	}

	public int getExprNodeType() {
		return ExprNode.NUMBER;
	}
	
	
}
