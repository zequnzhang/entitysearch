package query.tree.stmt;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.DataInfo;

public interface BoolExprNode extends ExprNode {
	boolean getBoolValue(DataInfo dataInfo) throws DoCQSException, IOException;
}
