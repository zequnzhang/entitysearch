package query.tree.stmt;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.BasicDataInfo;

public class FuncBoolNode extends BoolNode {
	
	FuncNode funcNode;
	
	public FuncBoolNode(SerialNode srcNode, FuncNode funcNode){
		this.srcNode=srcNode;
		this.funcNode=funcNode;		
	}

	public boolean checkDataInfo(BasicDataInfo dataInfo) throws DoCQSException, IOException {
		return funcNode.getBoolValue(dataInfo);
	}
	
}
