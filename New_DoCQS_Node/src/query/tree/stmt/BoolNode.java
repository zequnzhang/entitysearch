package query.tree.stmt;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.BasicDataInfo;

public abstract class BoolNode extends SerialNode {
	
	SerialNode srcNode;
	BasicDataInfo currentPosition;
	
	public int getCurrentDoc() {
		return srcNode.getCurrentDoc();
	}

	public BasicDataInfo getCurrentPosition() {
		return currentPosition;
	}

	public boolean nextDoc() throws DoCQSException, IOException  {
		currentPosition=null;
		return srcNode.nextDoc();
	}

	public boolean nextPosition() throws IOException, DoCQSException {
		currentPosition=null;
		while (srcNode.nextPosition()){
			if (checkDataInfo(srcNode.getCurrentPosition())){
				currentPosition=srcNode.getCurrentPosition();
				currentPosition.conf*=weight;
				return true;
			}
		}
		return false;
	}
	
	public abstract boolean checkDataInfo(BasicDataInfo dataInfo) throws DoCQSException, IOException;

}
