package query.tree.stmt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

import query.tree.DataInfo;


import exception.DoCQSException;

public class OrderNode extends BatchNode {
	int p;
	boolean finished;
	ValExprNode orderValNode;
	ArrayList<OrderItem> dataList;
	
	public OrderNode(Node srcNode, ValExprNode orderValNode) throws DoCQSException{
		this.srcNode=srcNode;
		this.orderValNode=orderValNode;
		dataList=null;		
		finished=false;
	}
	
	class OrderItem{
		DataInfo tuple;
		double weight;
	}

	public ArrayList<DataInfo> getCurrentDataList() throws DoCQSException, IOException{
		ArrayList<DataInfo> result=new ArrayList<DataInfo>();
		if (finished){
			for (OrderItem o:dataList) result.add(o.tuple);
			return result;
		}
		else{
			if (srcNode instanceof SerialNode){
				if (dataList==null) return result;
				synchronized(dataList){
					Collections.sort(dataList,new Comparator<OrderItem>(){
						public int compare(OrderItem o1, OrderItem o2) {
							return (int)((o2.weight-o1.weight)*100000);
						}						
					});
					for (int i=0;i<dataList.size();i++)
						result.add(dataList.get(i).tuple);
				}
			}
			else{
				Iterator<DataInfo> iter=((BatchNode)srcNode).getCurrentDataList().iterator();
				ArrayList<OrderItem> tempList=new ArrayList<OrderItem>();
				while (iter.hasNext()){
					OrderItem item=new OrderItem();	
					item.tuple=iter.next();
					item.weight=orderValNode.calculate(item.tuple);
					tempList.add(item);
				}
				Collections.sort(tempList,new Comparator<OrderItem>(){
					public int compare(OrderItem o1, OrderItem o2) {
						return (int)((o2.weight-o1.weight)*100000);
					}						
				});
				for (int i=0;i<tempList.size();i++)
					result.add(tempList.get(i).tuple);					
			}
			return result;
		}
	}

	public DataInfo getCurrentDataInfo(){
		if (dataList==null || p<0 || p>=dataList.size()) return null;
		return dataList.get(p).tuple;
	}

	public boolean nextDataInfo() throws DoCQSException, IOException {
		if (dataList==null){
			dataList=new ArrayList<OrderItem>();
			while (srcNode.nextDataInfo() && !terminated && dataList.size()<maxTupleNum){
				OrderItem item=new OrderItem();
				item.tuple=srcNode.getCurrentDataInfo();
				item.weight=orderValNode.calculate(item.tuple);
				synchronized(dataList){
					dataList.add(item);
				}
			}
			if (dataList.size()>=maxTupleNum) exceedMaxTupleNum=true;
			synchronized(dataList){
				Collections.sort(dataList,new Comparator<OrderItem>(){
					public int compare(OrderItem o1, OrderItem o2) {
						return (int)((o2.weight-o1.weight)*100000);
					}						
				});
			}
			p=-1;
			finished=true;
		}
		p++;
		if (p>=0 && p<dataList.size()) return true;
		return false;
	}
	
	public String toString(){
		return "ORDER("+srcNode+" BY "+orderValNode+")";
	}

}
