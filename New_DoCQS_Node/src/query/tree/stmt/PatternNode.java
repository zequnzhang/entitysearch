package query.tree.stmt;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;

import query.tree.BasicDataInfo;
import query.tree.reg.RegDataInfo;
import query.tree.reg.RegNode;

public class PatternNode extends SerialNode {
	RegNode regNode;
	ArrayList<BasicDataInfo> dataList=new ArrayList<BasicDataInfo>();
	int p=-1;
	int currentDoc=-1;
	
	public PatternNode(RegNode regNode){
		this.regNode=regNode;	
	}
	
	public int getCurrentDoc() {
		return currentDoc;
	}

	public BasicDataInfo getCurrentPosition() {
		if (p<0 || p>=dataList.size()) return null;
		return dataList.get(p);
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		dataList=null;
		p=-1;
		if (regNode.nextDoc()){
			currentDoc=regNode.getCurrentDoc();
			return true;
		}
		else{
			currentDoc=-1;
			return false;
		}
	}

	public boolean nextPosition() throws IOException, DoCQSException {
		if (dataList==null){
			dataList=new ArrayList<BasicDataInfo>();
			ArrayList<RegDataInfo> infoList=regNode.getDataInfoList();
			for (int i=0;i<infoList.size();i++){
				RegDataInfo regData=infoList.get(i);
				dataList.add(regData);
				//Maybe not 1 in the future.
				regData.conf=1*weight;
			}
		}
		p++;
		if (p>=dataList.size()) return false;
		return true;
	}
}
