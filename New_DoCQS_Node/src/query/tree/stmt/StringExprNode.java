package query.tree.stmt;

import java.io.IOException;

import query.tree.DataInfo;
import exception.DoCQSException;
import exception.DoCQSException;

public interface StringExprNode extends ExprNode {
	public String getStringValue(DataInfo dataInfo) throws DoCQSException, DoCQSException, IOException;
}
