package query.tree.stmt;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import conf.Constant;
import query.tree.BasicDataInfo;
import query.tree.DataInfo;
import query.tree.GroupDataInfo;
import query.tree.InfoPair;
import exception.DoCQSException;

public class GroupNode extends BatchNode{
	
	HashMap<String,GroupDataInfo> map;
	ArrayList<DataInfo> dataList;
	DataInfo dataInfo;
	Iterator<DataInfo> iter;
	ArrayList<EntityExprNodeImpl> groupNodeList;
	boolean finished;
	
	public GroupNode(Node srcNode, ArrayList<EntityExprNodeImpl> groupNodeList) throws DoCQSException{
		this.srcNode=srcNode;
		this.groupNodeList=groupNodeList;
		map=null;
		dataInfo=null;
		finished=false;
	}

	public ArrayList<DataInfo> getCurrentDataList() throws DoCQSException {
		if (finished){
			return dataList;
		}
		else{
			if (map==null) return new ArrayList<DataInfo>();
			ArrayList<DataInfo> result=new ArrayList<DataInfo>();
			synchronized (map){
				for (GroupDataInfo d:map.values())
					result.add(d);
			}
			return result;
		}
	}

	public DataInfo getCurrentDataInfo(){
		return dataInfo;
	}

	public boolean nextDataInfo() throws DoCQSException, IOException{
		if (map==null){
			map=new HashMap<String,GroupDataInfo>();
			while (srcNode.nextDataInfo() && !terminated && map.size()<maxTupleNum){
				BasicDataInfo dataInfo=(BasicDataInfo)srcNode.getCurrentDataInfo();
				String itemStr="";
				String[] vals=new String[groupNodeList.size()];
				for (int i=0;i<vals.length;i++){
					String v=dataInfo.getSchema(groupNodeList.get(i).entityID);
					vals[i]=v;
					itemStr+=v+" ";
				}
				GroupDataInfo d=map.get(itemStr);
				if (d==null){
					d=new GroupDataInfo();
					d.infos=new ArrayList<InfoPair>();
					for (int i=0;i<vals.length;i++){
						InfoPair info=new InfoPair(groupNodeList.get(i).entityID, vals[i]);
						d.infos.add(info);
					}
					d.conf=1;
					synchronized (map){
						map.put(itemStr, d);
					}
				}
				d.aggregateList.add(dataInfo);
				d.conf=d.conf*(1-dataInfo.conf);
//				if (!filterSimilarContextPage(dataInfo, d.aggregateList)) d.aggregateList.add(dataInfo);
			}
			if (map.size()>=maxTupleNum) exceedMaxTupleNum=true;
			dataList=new ArrayList<DataInfo>();
			for (DataInfo d:map.values()){
				d.conf=1-d.conf;
				dataList.add(d);
			}
			iter=dataList.iterator();
			finished=true;
		}
		boolean p=iter.hasNext();
		if (p) dataInfo=iter.next();
		return p;
	}

}
