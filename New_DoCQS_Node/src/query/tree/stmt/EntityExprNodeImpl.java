package query.tree.stmt;

import index.Item;

import java.io.IOException;

import query.tree.BasicDataInfo;
import query.tree.DataInfo;
import query.tree.GroupDataInfo;

public class EntityExprNodeImpl implements EntityExprNode{
	String entityID;
	public EntityExprNodeImpl(String entityID){
		this.entityID=entityID;
	}
	public Item getEntityItem(DataInfo dataInfo){
		if (!(dataInfo instanceof BasicDataInfo)) return null;
		BasicDataInfo d=(BasicDataInfo)dataInfo;
		for (int i=0;i<d.infos.size();i++){
			if (d.infos.get(i).schema.equals(entityID)){
				if (entityID!=d.infos.get(i).schema)
					entityID=d.infos.get(i).schema;
				return d.entities.get(i);
			}
		}
		return null;
	}
	public String getValue(DataInfo dataInfo) throws IOException {
		for (int i=0;i<dataInfo.infos.size();i++){
			if (dataInfo.infos.get(i).schema.equals(entityID)){
				if (entityID!=dataInfo.infos.get(i).schema)
					entityID=dataInfo.infos.get(i).schema;
				return dataInfo.getSchema(entityID);
			}
		}
		return null;
	}
	public int getExprNodeType() {
		return ExprNode.ENTITY;
	}
}
