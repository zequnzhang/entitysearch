package query.tree.stmt;

import java.io.IOException;

import exception.DoCQSException;

import query.tree.DataInfo;

public interface ExprNode {
	public static final int STRING=0;
	public static final int ENTITY=1;
	public static final int NUMBER=2;
	public static final int BOOL=3;
	public String getValue(DataInfo dataInfo) throws IOException, DoCQSException;
	public int getExprNodeType();
}
