package query.tree.stmt;

import index.Item;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.BasicDataInfo;
import query.tree.DataInfo;

public interface EntityExprNode extends ExprNode {
	public Item getEntityItem(DataInfo dataInfo) throws DoCQSException, IOException;
}
