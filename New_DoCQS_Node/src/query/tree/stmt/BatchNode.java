package query.tree.stmt;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;

import query.tree.DataInfo;

public abstract class BatchNode extends Node {
	Node srcNode;
	boolean terminated=false;
	boolean exceedMaxTupleNum=false;
	int maxTupleNum=Integer.MAX_VALUE;
	public abstract ArrayList<DataInfo> getCurrentDataList() throws DoCQSException, IOException;
	public void stop(){
		srcNode.stop();
		terminated=true;
	}
	public void setMaxNumOfTuple(int n) {
		maxTupleNum=n;
		srcNode.setMaxNumOfTuple(n);
	}
	public boolean exceedMaxNumOfTuple(){
		return exceedMaxTupleNum || srcNode.exceedMaxNumOfTuple();		
	}
}
