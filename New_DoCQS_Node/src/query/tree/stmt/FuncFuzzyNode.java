package query.tree.stmt;

import java.io.IOException;

import query.tree.BasicDataInfo;
import exception.DoCQSException;

public abstract class FuncFuzzyNode extends SerialNode{
	
	public int[] paraTypes=null;
	public ExprNode[] paras=null;
	SerialNode srcNode;	
	BasicDataInfo currentPosition;
	
	public void initFunction(SerialNode srcNode, ExprNode[] paras){
		this.paras=paras;
		this.srcNode=srcNode;
		currentPosition=null;
	}

	public int getCurrentDoc() {
		return srcNode.getCurrentDoc();
	}

	public BasicDataInfo getCurrentPosition() {
		return currentPosition;
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		return srcNode.nextDoc();
	}

	public boolean nextPosition() throws DoCQSException, IOException {
		currentPosition=null;
		if (srcNode.nextPosition()){
			currentPosition=srcNode.getCurrentPosition();
			currentPosition.conf*=getWeighting(currentPosition)*weight;
			return true;
		}
		return false;
	}
	
	public abstract double getWeighting(BasicDataInfo dataInfo) throws DoCQSException, IOException;

	public boolean checkParaTypes(){
		if (paraTypes.length!=paras.length) return false;
		for (int i=0;i<paraTypes.length;i++){
			if (paraTypes[i]==ExprNode.BOOL && !(paras[i] instanceof BoolExprNode)) return false;
			if (paraTypes[i]==ExprNode.ENTITY && !(paras[i] instanceof EntityExprNode)) return false;
			if (paraTypes[i]==ExprNode.NUMBER && !(paras[i] instanceof ValExprNode)) return false;
			if (paraTypes[i]==ExprNode.STRING && !(paras[i] instanceof StringExprNode)) return false;
		}
		return true;
	}
}
