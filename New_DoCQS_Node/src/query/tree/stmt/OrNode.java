package query.tree.stmt;

import java.io.IOException;
import java.util.ArrayList;

import query.tree.BasicDataInfo;
import exception.DoCQSException;

public class OrNode extends SerialNode{
	SerialNode n1, n2;
	int currentDoc;
	ArrayList<BasicDataInfo> dataInfoList;
	int p;
	
	public OrNode(SerialNode n1, SerialNode n2){
		this.n1=n1;
		this.n2=n2;
		currentDoc=-1;
		p=-1;
		dataInfoList=null;
	}

	public int getCurrentDoc() {
		return currentDoc;
	}

	public BasicDataInfo getCurrentPosition() {
		if (dataInfoList==null || p<0 || p>=dataInfoList.size()) return null;
		return dataInfoList.get(p);
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		if (n1.getCurrentDoc()==currentDoc) n1.nextDoc();
		if (n2.getCurrentDoc()==currentDoc) n2.nextDoc();
		dataInfoList=null;
		int miniDoc=Integer.MAX_VALUE;
		if (n1.getCurrentDoc()!=-1 && n1.getCurrentDoc()<miniDoc)
			miniDoc=n1.getCurrentDoc();
		if (n2.getCurrentDoc()!=-1 && n2.getCurrentDoc()<miniDoc)
			miniDoc=n2.getCurrentDoc();
		if (miniDoc==Integer.MAX_VALUE){
			currentDoc=-1;
			return false;
		}
		else{
			currentDoc=miniDoc;
			return true;
		}
	}

	public boolean nextPosition() throws DoCQSException, IOException {
		if (dataInfoList==null){
			dataInfoList=new ArrayList<BasicDataInfo>();
			if (n1.getCurrentDoc()==currentDoc)
				while (n1.nextPosition()){
					BasicDataInfo d=n1.getCurrentPosition();
					d.conf*=weight;
					dataInfoList.add(d);
				}
			if (n2.getCurrentDoc()==currentDoc)
				while (n2.nextPosition()){
					BasicDataInfo d=n2.getCurrentPosition();
					d.conf*=weight;
					dataInfoList.add(d);
				}
			p=-1;
		}
		p++;
		return p<dataInfoList.size();
	}

}
