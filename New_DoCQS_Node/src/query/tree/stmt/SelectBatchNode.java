package query.tree.stmt;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import query.tree.BasicDataInfo;
import query.tree.DataInfo;
import query.tree.GroupDataInfo;
import query.tree.InfoPair;
import exception.DoCQSException;

public class SelectBatchNode extends BatchNode{
	ArrayList<DataInfo> dataList;
	ArrayList<ExprNode> exprNodeList;
	ArrayList<String> schemas;
	DataInfo dataInfo;
	boolean finished;
	int p;
	
	public SelectBatchNode(BatchNode srcNode, ArrayList<ExprNode> exprNodeList, ArrayList<String> schemas){
		this.srcNode=srcNode;		
		this.exprNodeList=exprNodeList;
		this.schemas=schemas;
		finished=false;
		dataList=null;
		dataInfo=null;
		p=-1;
	}

	public ArrayList<DataInfo> getCurrentDataList()
			throws DoCQSException, IOException {
		if (finished){
			return dataList;
		}
		else{
			if (srcNode instanceof SerialNode){
				if (dataList==null) return new ArrayList<DataInfo>();
				ArrayList<DataInfo> result=new ArrayList<DataInfo>();
				synchronized(dataList){
					result.addAll(dataList);
				}
				return result;
			}
			else{
				ArrayList<DataInfo> prevRes=((BatchNode)srcNode).getCurrentDataList();
				ArrayList<DataInfo> result=new ArrayList<DataInfo>();
				for (DataInfo d:prevRes){
					if (d instanceof BasicDataInfo){
						BasicDataInfo r=new BasicDataInfo();
						r.infos=new ArrayList<InfoPair>();
						r.entities=new ArrayList<Item>();
						for (int i=0;i<exprNodeList.size();i++){
							String schema=schemas.get(i);
							String value=null;
							Item item=null;
							if (exprNodeList.get(i).getExprNodeType()==ExprNode.ENTITY)
								item=((EntityExprNode)exprNodeList.get(i)).getEntityItem(d);
							else
								value=exprNodeList.get(i).getValue(d);
							r.infos.add(new InfoPair(schema, value));
							r.entities.add(item);
						}
						result.add(r);					
					}
					else{
						GroupDataInfo r=new GroupDataInfo();
						r.infos=new ArrayList<InfoPair>();
						for (int i=0;i<exprNodeList.size();i++){
							r.infos.add(new InfoPair(schemas.get(i), exprNodeList.get(i).getValue(d)));
						}
						r.aggregateList=((GroupDataInfo)d).aggregateList;
						result.add(r);		
					}
				}
				return result;
			}
		}
	}

	public DataInfo getCurrentDataInfo() {
		return dataInfo;
	}

	public boolean nextDataInfo() throws DoCQSException, IOException {
		dataInfo=null;
		if (dataList==null){
			dataList=new ArrayList<DataInfo>();
			while (srcNode.nextDataInfo() && !terminated && dataList.size()<maxTupleNum){
				DataInfo d=srcNode.getCurrentDataInfo();
				if (d instanceof BasicDataInfo){
					BasicDataInfo r=new BasicDataInfo();
					r.infos=new ArrayList<InfoPair>();
					r.entities=new ArrayList<Item>();
					for (int i=0;i<exprNodeList.size();i++){
						r.infos.add(new InfoPair(schemas.get(i), exprNodeList.get(i).getValue(d)));
						if (exprNodeList.get(i).getExprNodeType()==ExprNode.ENTITY)
							r.entities.add(((EntityExprNode)exprNodeList.get(i)).getEntityItem(d));
					}
					synchronized(dataList){
						dataList.add(r);					
					}
				}
				else{
					GroupDataInfo r=new GroupDataInfo();
					r.infos=new ArrayList<InfoPair>();
					for (int i=0;i<exprNodeList.size();i++){
						r.infos.add(new InfoPair(schemas.get(i), exprNodeList.get(i).getValue(d)));
					}
					r.aggregateList=((GroupDataInfo)d).aggregateList;
					synchronized(dataList){
						dataList.add(r);					
					}
				}
			}
			if (dataList.size()>=maxTupleNum) exceedMaxTupleNum=true;
		}
		p++;
		if (p>=dataList.size()) return false;
		dataInfo=dataList.get(p);
		return true;
	}

}
