package query.tree.stmt;

import java.io.IOException;

import index.IndexReaderManager;
import query.tree.BasicDataInfo;
import query.tree.DataInfo;
import exception.DoCQSException;

public abstract class SerialNode extends Node {
	
	double weight=1.0;
	
	public abstract int getCurrentDoc();

	public abstract BasicDataInfo getCurrentPosition();

	public abstract boolean nextDoc() throws DoCQSException, IOException;

	public abstract boolean nextPosition() throws IOException, DoCQSException;
	
	public void stop(){
		//Do nothing for SerialNode.
	}
	
	public boolean nextDataInfo() throws DoCQSException, IOException{
		if (nextPosition())	return true;
		while (nextDoc()){
			boolean p=nextPosition();
			if (p)
				return true;
		}
		return false;
	}

	public DataInfo getCurrentDataInfo(){
		return getCurrentPosition();
	}
	
	public void setWeight(double weight){
		this.weight=weight;
	}
	
	public double getWeight(){
		return weight;
	}
	
	public void setMaxNumOfTuple(int n){
	}
	public boolean exceedMaxNumOfTuple(){
		return false;
	}
}
