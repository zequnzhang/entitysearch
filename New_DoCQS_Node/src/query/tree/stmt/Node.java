package query.tree.stmt;

import java.io.IOException;
import java.util.Collection;

import query.tree.DataInfo;
import exception.DoCQSException;

public abstract class Node {
	public abstract boolean nextDataInfo() throws DoCQSException, IOException;
	public abstract DataInfo getCurrentDataInfo();
	public abstract void stop();
	public abstract void setMaxNumOfTuple(int n);
	public abstract boolean exceedMaxNumOfTuple();
}
