package query.tree.stmt;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import query.tree.BasicDataInfo;
import exception.DoCQSException;

public class UniqueNode extends SerialNode {
	
	SerialNode n;
	BasicDataInfo currentPosition;
	ArrayList<BasicDataInfo> dataInfoList;
	
	public UniqueNode(Node n){
		this.n=(SerialNode)n;	
		currentPosition=null;
		dataInfoList=new ArrayList<BasicDataInfo>();
	}

	public int getCurrentDoc(){
		return n.getCurrentDoc();
	}

	public BasicDataInfo getCurrentPosition(){
		return currentPosition;
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		currentPosition=null;
		dataInfoList.clear();
		return n.nextDoc();
	}

	public boolean nextPosition() throws DoCQSException, IOException {
		while (n.nextPosition()){
			currentPosition=n.getCurrentPosition();
			int i;
			for (i=0;i<dataInfoList.size();i++)
				if (equal((BasicDataInfo)currentPosition,dataInfoList.get(i))){
					//Max for conf;
					currentPosition.conf=Math.max(dataInfoList.get(i).conf, currentPosition.conf);
					break;
				}
					
			if (i>=dataInfoList.size()){
				dataInfoList.add((BasicDataInfo)currentPosition);
				return true;
			}
		}
		currentPosition=null;
		return false;
	}
	
	private boolean equal(BasicDataInfo d1, BasicDataInfo d2){
		if (d1.infos.size()!=d2.infos.size()) return false;
		for (int i=0;i<d1.infos.size();i++){
			int j;
			for (j=0;j<d2.infos.size();j++)
				if (d1.infos.get(i).schema.equals(d2.infos.get(j).schema) 
						&& d1.entities.get(i).equals(d2.entities.get(j))) break;
			if (j>=d2.infos.size()) return false;
		}
		return true;
	}

}
