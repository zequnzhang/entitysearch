package query.tree.stmt;

import index.Item;

import java.io.IOException;

import query.tree.BasicDataInfo;
import query.tree.DataInfo;
import exception.DoCQSException;

public abstract class FuncNode implements BoolExprNode, EntityExprNode, StringExprNode, ValExprNode {
	
	public int[] paraTypes=null;
	public int resultType=-1;
	public ExprNode[] paras=null;
	public String funcID;
	
	public void initFunction(ExprNode[] paras){
		this.paras=paras;
	}
	
	public boolean checkParaTypes(){
		if (paraTypes.length!=paras.length) return false;
		for (int i=0;i<paraTypes.length;i++)
			if (paraTypes[i]!=paras[i].getExprNodeType()) return false;
		return true;
	}
	
	public boolean getBoolValue(DataInfo dataInfo) throws DoCQSException, IOException {
		if (resultType!=ValNode.BOOL) throw new DoCQSException("Function Type Mismatch.");
		ValNode r=getFunctionResult(dataInfo);
		return r.boolVal;
	}
	
	public Item getEntityItem(DataInfo dataInfo) throws DoCQSException, IOException {
		if (resultType!=ValNode.ENTITY) throw new DoCQSException("Function Type Mismatch.");
		ValNode r=getFunctionResult(dataInfo);
		return r.entityVal;		
	}
	
	public String getStringValue(DataInfo dataInfo)  throws DoCQSException, IOException{
		if (resultType!=ValNode.STRING) throw new DoCQSException("Function Type Mismatch.");
		ValNode r=getFunctionResult(dataInfo);
		return r.strVal;			
	}
	
	public double calculate(DataInfo dataInfo) throws DoCQSException, IOException{
		if (resultType!=ValNode.NUMBER) throw new DoCQSException("Function Type Mismatch.");
		ValNode r=getFunctionResult(dataInfo);
		return r.numVal;	
	}
	
	public String getValue(DataInfo dataInfo) throws DoCQSException, IOException {
		ValNode v=getFunctionResult(dataInfo);
		if (v==null) return null;
		return v.toString();
	}
	
	public abstract ValNode getFunctionResult(DataInfo dataInfo) throws DoCQSException, IOException;
	public int getExprNodeType(){
		return resultType;
	}
}
