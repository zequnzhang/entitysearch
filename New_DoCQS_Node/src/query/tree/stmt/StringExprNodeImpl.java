package query.tree.stmt;

import query.tree.DataInfo;
import exception.DoCQSException;

public class StringExprNodeImpl implements StringExprNode{
	public String str;
	public StringExprNodeImpl(String str){
		this.str=str;
	}
	public String getValue(DataInfo dataInfo) throws DoCQSException {
		return str;
	}

	public String getStringValue(DataInfo dataInfo)
			throws DoCQSException {
		return str;
	}
	public int getExprNodeType() {
		return ExprNode.STRING;
	}

}
