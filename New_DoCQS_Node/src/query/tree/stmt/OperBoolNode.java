package query.tree.stmt;

import java.io.IOException;

import query.tree.BasicDataInfo;
import query.tree.DataInfo;

import exception.DoCQSException;

public class OperBoolNode extends BoolNode implements BoolExprNode{
	
	public static final int EQ=0;
	public static final int LT=1;
	public static final int GT=2;
	public static final int LET=3;
	public static final int GET=4;
	public static final int NEQ=5;
	int oper;
	ValExprNode n1,n2;
	final double eps=1e-6;
	
	public OperBoolNode(int oper, ValExprNode n1, ValExprNode n2, SerialNode srcNode){
		this.oper=oper;
		this.n1=n1;
		this.n2=n2;
		this.srcNode=srcNode;
	}

	boolean equal(double d1,double d2){
		return Math.abs(d1-d2)<eps;				
	}

	public boolean checkDataInfo(BasicDataInfo dataInfo) throws DoCQSException, IOException {
		return getBoolValue(dataInfo);
	}

	public boolean getBoolValue(DataInfo dataInfo)
			throws DoCQSException, IOException {
		double d1=n1.calculate(dataInfo),d2=n2.calculate(dataInfo);
		if (oper==EQ && equal(d1,d2)) return true; 
		if (oper==LT && d1<d2 && !equal(d1,d2)) return true;
		if (oper==GT && d1>d2 && !equal(d1,d2)) return true; 
		if (oper==LET && (d1<d2 || equal(d1,d2))) return true;
		if (oper==GET && (d1>d2 || equal(d1,d2))) return true;
		if (oper==NEQ && !equal(d1,d2)) return true;
		return false;
	}

	public String getValue(DataInfo dataInfo) throws DoCQSException, IOException {
		return getBoolValue(dataInfo)+"";
	}

	public int getExprNodeType() {
		return ExprNode.BOOL;
	}

}