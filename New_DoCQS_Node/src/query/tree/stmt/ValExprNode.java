package query.tree.stmt;

import java.io.IOException;

import exception.DoCQSException;
import query.tree.DataInfo;

public interface ValExprNode extends ExprNode{
	public double calculate(DataInfo dataInfo) throws DoCQSException, IOException;
}
