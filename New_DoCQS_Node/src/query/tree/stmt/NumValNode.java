package query.tree.stmt;

import exception.DoCQSException;
import query.tree.DataInfo;


public class NumValNode implements ValExprNode {
	double v;
	public NumValNode(double v){
		this.v=v;		
	}
	public double calculate(DataInfo dataInfo) {
		return v;
	}	
	public String getValue(DataInfo dataInfo) throws DoCQSException {
		return ""+v;
	}
	public int getExprNodeType() {
		return ExprNode.NUMBER;
	}
}
