package query.tree.reg;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;


public abstract class ConjunctiveRegNode extends RegNode{
	public ArrayList<RegNode> nodeList;
	int currentDoc;
	ArrayList<RegDataInfo> dataList;
	
	public ConjunctiveRegNode(String alias){
		super(alias);
		this.nodeList=null;
		this.currentDoc=-1;
		dataList=null;		
	}
	public boolean nextDoc() throws IOException, DoCQSException{
		dataList = null;
		if (currentDoc==Integer.MAX_VALUE) return false;
		assert(nodeList.size()>0);
		if (!nodeList.get(0).nextDoc()){
			currentDoc=Integer.MAX_VALUE;
			return false;
		}
		while (true) {			
			int maxiDoc = nodeList.get(0).getCurrentDoc();
			boolean check = true;
			for (int i = 1; i < nodeList.size(); i++) {
				if (nodeList.get(i).getCurrentDoc() != nodeList.get(0).getCurrentDoc())
					check = false;
				if (nodeList.get(i).getCurrentDoc() > maxiDoc)
					maxiDoc = nodeList.get(i).getCurrentDoc();
			}
			if (check) {
				currentDoc = nodeList.get(0).getCurrentDoc();
				return true;
			}
			if (maxiDoc==Integer.MAX_VALUE){
				currentDoc=maxiDoc=Integer.MAX_VALUE;
				return false;
			}
			for (int i=0;i<nodeList.size();i++){
				while (nodeList.get(i).getCurrentDoc()<maxiDoc)
					nodeList.get(i).nextDoc();
			}
		}
		
	}
	public int getCurrentDoc(){
		return currentDoc;		
	}
	
	public ArrayList<RegNode> getSubRegNode() {
		ArrayList<RegNode> list=new ArrayList<RegNode>();
		for (int i=0;i<nodeList.size();i++){
			list.add(nodeList.get(i));
		}
		return list;
	}
}
