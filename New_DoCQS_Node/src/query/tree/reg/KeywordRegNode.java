package query.tree.reg;

import index.Item;

import java.util.ArrayList;

import query.parser.reg.KeywordToken;

public class KeywordRegNode extends SingleRegNode{
	
	public String keyword;
	public KeywordRegNode(String keyword, String alias){
		super(alias);
		this.keyword=keyword;
	}
	
	@Override
	public boolean isEntity() {
		return false;
	}
	
	public String toString(){
		return "KEYWORD("+keyword+","+pointer+")";
	}

}
