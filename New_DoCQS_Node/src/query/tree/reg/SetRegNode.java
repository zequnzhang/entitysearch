package query.tree.reg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import util.StringUtil;

import exception.DoCQSException;

public class SetRegNode extends ConjunctiveRegNode {
	
	int windowSize;
	
	public SetRegNode(ArrayList<RegNode> nodeList, int windowSize, String alias){
		super(alias);
		this.nodeList=nodeList;
		this.windowSize=windowSize;
		dataList=null;
	}

	public ArrayList<RegDataInfo> getDataInfos() throws IOException, DoCQSException{
		if (dataList!=null) return dataList;
		ArrayList<RegDataInfo[]> list=null, prevList;
		for (int i=0;i<nodeList.size();i++){
			if (i==0){
				ArrayList<RegDataInfo> l=nodeList.get(i).getDataInfoList();
				list=new ArrayList<RegDataInfo[]>();
				for (RegDataInfo r:l){
					RegDataInfo[] rs=new RegDataInfo[nodeList.size()];
					rs[i]=r;
					list.add(rs);
				}
			}
			else{
				prevList=list;
				list=new ArrayList<RegDataInfo[]>();
				if (prevList.size()==0) break;
				ArrayList<RegDataInfo> l=nodeList.get(i).getDataInfoList();
				int c=0;
				for (RegDataInfo r:l){
					while (c<prevList.size() && cmp(prevList.get(c)[i-1],r)==-1) c++;
					int t=c;
					while (t<prevList.size() && cmp(prevList.get(t)[i-1],r)==0){
						RegDataInfo[] oldReg=prevList.get(t), newReg=new RegDataInfo[oldReg.length];
						for(int j=0;j<oldReg.length;j++) newReg[j]=oldReg[j];
						newReg[i]=r;
						list.add(newReg);
						t++;
					}
				}				
			}
		}
		dataList=new ArrayList<RegDataInfo>();
		for (RegDataInfo[] rs:list){
			RegDataInfo d=mergeDataInfo(rs);
			if (d!=null) {
				dataList.add(d);
			}
		}
		return dataList;
	}
	int cmp(RegDataInfo ori, RegDataInfo tar){
		int l=ori.basedItem.pos-(windowSize-ori.basedItem.len);
		int u=ori.basedItem.pos+(windowSize-tar.basedItem.len);
		if (tar.basedItem.pos<l) return 1;
		if (tar.basedItem.pos>u) return -1;
		return 0;
	}

	RegDataInfo mergeDataInfo(RegDataInfo[] infos){
		//Recheck
		RegDataInfo r=new RegDataInfo(infos);
		if (r.basedItem.len>windowSize) return null;
		return r;
	}
	
	public String toString(){
		return "Set["+StringUtil.toString(nodeList)+"]";
	}

}
