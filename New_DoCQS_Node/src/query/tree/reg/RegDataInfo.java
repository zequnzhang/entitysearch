package query.tree.reg;

import index.Item;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import query.tree.BasicDataInfo;
import query.tree.InfoPair;

public class RegDataInfo extends BasicDataInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5383715995109258936L;
	private static final int INIT_CAPACITY=5;
	
	public Item basedItem;
	public RegDataInfo(Item basedItem){
		this.basedItem=basedItem;
		entities=new ArrayList<Item>(INIT_CAPACITY);
		infos=new ArrayList<InfoPair>(INIT_CAPACITY);
	}

	public RegDataInfo(Item item, String entitySchema) throws IOException{
		this(item);
		if (entitySchema!=null){
			entities.add(item);
			infos.add(new InfoPair(entitySchema, item.text));
		}
	}
	
	public RegDataInfo(RegDataInfo[] infoList){
		int m1=-1,m2=-1,maxi=-1,mini=Integer.MAX_VALUE;
		for (int i=0;i<infoList.length;i++){
			if (infoList[i].basedItem.pos<mini){
				mini=infoList[i].basedItem.pos;
				m1=i;
			}
			if (infoList[i].basedItem.pos>maxi){
				maxi=infoList[i].basedItem.pos;
				m2=i;
			}
		}
		basedItem=new Item(
				infoList[m1].basedItem.doc,
				infoList[m1].basedItem.pos,
				infoList[m2].basedItem.pos+infoList[m2].basedItem.len-infoList[m1].basedItem.pos,null,null);
		
		infos=new ArrayList<InfoPair>();
		entities=new ArrayList<Item>();
		for (int i=0;i<infoList.length;i++){
			infos.addAll(infoList[i].infos);
			entities.addAll(infoList[i].entities);
		}
	}	

	public String toString(){
		String str=basedItem+" [ ";
		for (int i=0;i<entities.size();i++){
			str+=infos.get(i)+":"+entities.get(i)+" ";
		}		
		str+="]";
		return str;
	}
}
