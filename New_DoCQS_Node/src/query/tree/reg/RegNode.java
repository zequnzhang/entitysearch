package query.tree.reg;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;
import query.parser.reg.Token;
import query.tree.InfoPair;

public abstract class RegNode {
	String alias;
	String forcedValue;
	public RegNode(String alias){
		this.alias=alias;
		this.forcedValue=null;
	}
	public abstract boolean nextDoc() throws IOException, DoCQSException;
	public abstract int getCurrentDoc() throws DoCQSException;
	protected abstract ArrayList<RegDataInfo> getDataInfos() throws IOException, DoCQSException;
	public abstract ArrayList<RegNode> getSubRegNode();
	
	public ArrayList<RegDataInfo> getDataInfoList() throws IOException, DoCQSException{
		ArrayList<RegDataInfo> dataList=getDataInfos();
		for (RegDataInfo info:dataList) {
			assignForcedValue(info);
			assignAlias(info);
		}
		return dataList;
	}
	
	public void setForcedValue(String value){
		this.forcedValue=value;		
	}
	
	private void assignForcedValue(RegDataInfo dataInfo){
		if (forcedValue==null) return;
		dataInfo.basedItem.text=forcedValue;		
	}
	
	private void assignAlias(RegDataInfo dataInfo) throws IOException{
		if (alias==null) return;
		dataInfo.entities.add(dataInfo.basedItem);
		dataInfo.infos.add(new InfoPair(alias, dataInfo.basedItem.text));
	}
	public void setAlias(String alias){
		this.alias=alias;		
	}
}
