package query.tree.reg;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;
import query.parser.reg.SingleToken;
import query.plan.IndexPointer;

public abstract class SingleRegNode extends RegNode {
	
	public SingleRegNode(String alias){
		super(alias);
	}
	
	IndexPointer pointer=null;
	int currentDoc=-1;
	
	public void setIndexPointer(IndexPointer pointer){
		this.pointer=pointer;		
	}
	
	public abstract boolean isEntity();
	
	public int getCurrentDoc() {
		return currentDoc;
	}

	public boolean nextDoc() throws IOException {
//		System.out.println("**"+this+":"+currentDoc+":"+pointer.getCurrentDoc());
		assert(currentDoc<=pointer.getCurrentDoc());
		if (currentDoc==pointer.getCurrentDoc()) {
			boolean hasNext=pointer.nextDoc();
			currentDoc=pointer.getCurrentDoc();
			return hasNext;
		}
		else{
			currentDoc=pointer.getCurrentDoc();
			return currentDoc!=Integer.MAX_VALUE;
		}
	}

	public ArrayList<RegDataInfo> getDataInfos() throws IOException {
		if (currentDoc==-1 || currentDoc==Integer.MAX_VALUE) return new ArrayList<RegDataInfo>();
		if (currentDoc!=pointer.getCurrentDoc()) return new ArrayList<RegDataInfo>();
		ArrayList<Item> itemList=pointer.getDataList();
		ArrayList<RegDataInfo> result=new ArrayList<RegDataInfo>();
		for (Item item:itemList){
			RegDataInfo info=new RegDataInfo(item);
			result.add(info);
		}
		return result;
	}
	
	public ArrayList<RegNode> getSubRegNode() {
		return null;
	}

	public static SingleRegNode createSingleRegNode(String word){
		SingleRegNode regNode;
		if (word.startsWith("#"))
			regNode=new EntityRegNode(word);
		else
			regNode=new KeywordRegNode(word, null);
		return regNode;
	}
	
}
