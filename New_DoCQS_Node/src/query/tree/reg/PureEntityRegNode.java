package query.tree.reg;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;

public class PureEntityRegNode extends ConjunctiveRegNode{
	
	public PureEntityRegNode(ArrayList<SingleRegNode> entityNodeList){
		super(null);
		this.nodeList=new ArrayList<RegNode>();
		for (SingleRegNode n:entityNodeList)
			this.nodeList.add(n);
	}
	
	public ArrayList<RegDataInfo> getDataInfos() throws IOException, DoCQSException {
		if (dataList!=null) return dataList;
		dataList=new ArrayList<RegDataInfo>();
		ArrayList<ArrayList<RegDataInfo>> resultMatrix=new ArrayList<ArrayList<RegDataInfo>>();
		for (int i=0;i<nodeList.size();i++)
			resultMatrix.add(nodeList.get(i).getDataInfoList());
		int c[]=new int[nodeList.size()+1];
		for (int i=0;i<c.length;i++)
			c[i]=0;
		while (c[c.length-1]!=1){
			RegDataInfo[] ds=new RegDataInfo[nodeList.size()];
			for (int i=0;i<ds.length;i++)
				ds[i]=resultMatrix.get(i).get(c[i]);			
			int p=0;
			while (p<resultMatrix.size() && c[p]==resultMatrix.get(p).size()-1){
				c[p]=0;
				p++;
			}
			c[p]++;
			dataList.add(new RegDataInfo(ds));
		}		
		return dataList;
	}

}
