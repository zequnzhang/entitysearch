package query.tree.reg;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;

public class InRegNode extends ConjunctiveRegNode{
	
	RegNode insideNode;
	RegNode srcNode;

	public InRegNode(RegNode insideNode, RegNode srcNode, String alias){
		super(alias);
		this.insideNode=insideNode;
		this.srcNode=srcNode;
		nodeList=new ArrayList<RegNode>();
		nodeList.add(insideNode);
		nodeList.add(srcNode);
	}	

	public ArrayList<RegDataInfo> getDataInfos() throws IOException, DoCQSException {
		if (currentDoc==-1 || currentDoc==Integer.MAX_VALUE) return new ArrayList<RegDataInfo>();
		if (dataList!=null) return dataList;
		dataList=new ArrayList<RegDataInfo>();
		ArrayList<RegDataInfo> srcNodeDataList,insideNodeDataList;
		srcNodeDataList=srcNode.getDataInfoList();
		insideNodeDataList=insideNode.getDataInfoList();
		int p=0;
		for (RegDataInfo srcData:srcNodeDataList){
			Item srcItem=srcData.basedItem;
			while (p<insideNodeDataList.size() 
				&& insideNodeDataList.get(p).basedItem.pos<srcItem.pos){
				p++;
			}
			int i=p;
			while (i<insideNodeDataList.size()){
				Item insideItem=insideNodeDataList.get(i).basedItem;
				if (insideItem.pos>=srcItem.pos && insideItem.pos+insideItem.len<=srcItem.pos+srcItem.len){
					RegDataInfo[] data=new RegDataInfo[2];
					data[0]=srcData;
					data[1]=insideNodeDataList.get(i);
					RegDataInfo r=new RegDataInfo(data);
					dataList.add(r);				
					i++;					
				}
				else{
					break;
				}
			}				
		}
		return dataList;
	}
	
}
