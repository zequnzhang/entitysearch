package query.tree.reg;


public class EntityRegNode extends SingleRegNode {
	
	public String entityName;

	public EntityRegNode(String entityName) {
		super(null);
		this.entityName=entityName;
		setAlias(entityName);
	}

	@Override
	public boolean isEntity() {
		return true;
	}
	
	public String toString(){
		return "Entity("+entityName+","+pointer+")";
	}

}
