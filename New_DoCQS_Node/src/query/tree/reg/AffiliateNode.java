package query.tree.reg;

import index.Item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import exception.DoCQSException;

public class AffiliateNode extends RegNode{
	
	RegNode mainNode;
	ArrayList<RegNode> subNodes;
	int windowSize;
	int currentDoc;
	ArrayList<RegDataInfo> dataList=null;

	public AffiliateNode(RegNode mainNode, ArrayList<RegNode> subNodes, int windowSize) {
		super(null);
		this.mainNode=mainNode;
		this.subNodes=subNodes;
		this.windowSize=windowSize;
	}

	@Override
	public boolean nextDoc() throws IOException, DoCQSException {
		boolean p=mainNode.nextDoc();
		currentDoc=mainNode.getCurrentDoc();
		for (int i=0;i<subNodes.size();i++){
			while (subNodes.get(i).getCurrentDoc()<currentDoc)
				subNodes.get(i).nextDoc();
		}
		dataList=null;
		return p;
	}

	@Override
	public int getCurrentDoc() throws DoCQSException {
		return currentDoc;
	}

	@Override
	public ArrayList<RegDataInfo> getDataInfos() throws IOException,
			DoCQSException {
		if (dataList!=null) return dataList;
		dataList=mainNode.getDataInfoList();
		for (RegDataInfo d:dataList){
			int pos=d.basedItem.pos;
			for (int i=0;i<subNodes.size();i++){
				if (subNodes.get(i).getCurrentDoc()!=currentDoc) continue;
				ArrayList<RegDataInfo> infoList=subNodes.get(i).getDataInfoList();
				int minDis=windowSize;
				RegDataInfo dd=null;
				for (RegDataInfo rd:infoList){
					int dis=Math.abs(rd.basedItem.pos-pos);
					if (dis>minDis) continue;
					minDis=dis;
					dd=rd;
				}
				if (dd==null) continue;
				d.infos.addAll(dd.infos);
				d.entities.addAll(dd.entities);
			}
		}
		return dataList;
	}

	@Override
	public ArrayList<RegNode> getSubRegNode() {
		ArrayList<RegNode> list=new ArrayList<RegNode>();
		list.add(mainNode);
		list.addAll(subNodes);
		return list;
	}

}
