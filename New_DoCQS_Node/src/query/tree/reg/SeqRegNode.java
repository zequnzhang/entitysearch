package query.tree.reg;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;

import query.parser.reg.Token;
import util.StringUtil;

public class SeqRegNode extends ConjunctiveRegNode {

	Dis[] distances;
	
	public SeqRegNode(ArrayList<RegNode> nlist, String alias)
			throws DoCQSException {
		super(alias);
		String v="";
		int j=0;
		for (j=0;j<nlist.size();j++){
			if (j>0) v+=" ";
			RegNode node=nlist.get(j);
			if (!(node instanceof KeywordRegNode)) continue;
			v+=((KeywordRegNode)node).keyword;
		}
		if (j>=nlist.size())
			setForcedValue(v);

		// Remove the QMNode
		int c = 0;
		for (int i = 0; i < nlist.size(); i++) {
			if (!(nlist.get(i) instanceof QMRegNode))
				c++;
			if (nlist.get(i) instanceof QMRegNode
					&& nlist.get(i - 1) instanceof QMRegNode) {
				QMRegNode n = (QMRegNode) nlist.get(i), n0 = (QMRegNode) nlist
						.get(i - 1);
				n0.lowerBound += n.lowerBound;
				n0.upperBound += n.upperBound;
				nlist.remove(i);
				i--;
			}
		}
		while (nlist.size() > 0 && nlist.get(0) instanceof QMRegNode)
			nlist.remove(0);
		while (nlist.size() > 0
				&& nlist.get(nlist.size() - 1) instanceof QMRegNode)
			nlist.remove(nlist.size() - 1);
		if (nlist.size() == 0)
			throw new DoCQSException("No nodes in seq token.");

		distances = new Dis[c - 1];
		nodeList = new ArrayList<RegNode>();
		for (int i = 0; i < nlist.size(); i++) {
			if (!(nlist.get(i) instanceof QMRegNode)) {
				nodeList.add(nlist.get(i));
				int s=nodeList.size()-1;
				if (s > 0) {
					if (nodeList.get(s - 1) == nlist.get(i - 1))
						distances[s - 1] = new Dis(0, 0);
					else {
						distances[s - 1] = new Dis(
								((QMRegNode) nlist.get(i - 1)).lowerBound,
								((QMRegNode) nlist.get(i - 1)).upperBound);
					}
				}
			}
		}
	}

	public ArrayList<RegDataInfo> getDataInfos() throws IOException, DoCQSException {
		if (dataList != null)
			return dataList;
		ArrayList<RegDataInfo[]> list = null, prevList;
		for (int i = 0; i < nodeList.size(); i++) {
			// System.out.println("----->"+i+"----->"+p);
			if (i == 0) {
				ArrayList<RegDataInfo> l = nodeList.get(i).getDataInfoList();
				list = new ArrayList<RegDataInfo[]>();
//				System.out.println(nodeList.get(i)+"::"+nodeList.get(i).getCurrentDoc());
				for (RegDataInfo r : l) {
					RegDataInfo[] rs = new RegDataInfo[nodeList.size()];
					rs[i] = r;
					list.add(rs);
				}
			} else {
				prevList = list;
				list = new ArrayList<RegDataInfo[]>();
				if (prevList.size() == 0)
					break;
				ArrayList<RegDataInfo> l = nodeList.get(i).getDataInfoList();
				int c = 0;
				for (RegDataInfo r : l) {
					Dis dis = distances[i-1];
					while (c < prevList.size()
							&& cmp(prevList.get(c)[i-1], r, dis) == 1)
						c++;
					int t = c;
					while (t < prevList.size()
							&& cmp(prevList.get(t)[i-1], r, dis) == 0) {
						RegDataInfo[] oldReg = prevList.get(t), newReg = new RegDataInfo[oldReg.length];
						for (int j = 0; j < oldReg.length; j++)
							newReg[j] = oldReg[j];
						newReg[i] = r;
						list.add(newReg);
						t++;
					}
				}
			}
		}
		dataList = new ArrayList<RegDataInfo>();
		for (RegDataInfo[] rs : list){
			RegDataInfo info = new RegDataInfo(rs);
			dataList.add(info);
		}
		return dataList;
	}

	int cmp(RegDataInfo ori, RegDataInfo tar, Dis dis) {
		// System.out.println(ori+"::"+tar);
		int l, u;
		l = ori.basedItem.pos + ori.basedItem.len + dis.lower;
		u = ori.basedItem.pos + ori.basedItem.len + dis.upper;
		if (tar.basedItem.pos < l)
			return -1;
		if (tar.basedItem.pos > u)
			return 1;
		return 0;
	}

	class Dis {
		int lower;
		int upper;

		public Dis(int lower, int upper) {
			this.lower = lower;
			this.upper = upper;
		}

		public String toString() {
			return "[" + lower + "," + upper + "]";
		}
	}
	
	public String toString(){
		return "Seq["+StringUtil.toString(nodeList)+"]";
	}

}
