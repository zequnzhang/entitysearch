package query.tree.reg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import exception.DoCQSException;

import query.parser.reg.Token;

public class OrRegNode extends RegNode {
	ArrayList<RegNode> nodeList;
	int currentDoc=-1;
	ArrayList<RegDataInfo> dataList;
	
	public OrRegNode(ArrayList<RegNode> nodeList, String alias){
		super(alias);
		this.nodeList=nodeList;		
		this.dataList=null;
	}
	
	public int getCurrentDoc() {
		return currentDoc;
	}

	public ArrayList<RegDataInfo> getDataInfos() throws DoCQSException, IOException {
		if (currentDoc==-1 || currentDoc==Integer.MAX_VALUE) return new ArrayList<RegDataInfo>();
		if (dataList==null){
			dataList=new ArrayList<RegDataInfo>();
			
			ArrayList<ArrayList<RegDataInfo>> dataMatrix=new ArrayList<ArrayList<RegDataInfo>>();
			for (int i=0;i<nodeList.size();i++)
				if (nodeList.get(i).getCurrentDoc()==currentDoc){
					ArrayList<RegDataInfo> infoList=nodeList.get(i).getDataInfoList();
					dataMatrix.add(infoList);
				}
			int[] p=new int[dataMatrix.size()];
			for (int i=0;i<p.length;i++)
				p[i]=0;
			
			while (true){
				int mini=Integer.MAX_VALUE, f=-1;
				for (int i=0;i<dataMatrix.size();i++)
					if (p[i]<dataMatrix.get(i).size() && dataMatrix.get(i).get(p[i]).basedItem.pos<mini){
						f=i;
						mini=dataMatrix.get(i).get(p[i]).basedItem.pos;
					}
				if (f==-1) break;
				if (dataList.size()==0 || !dataList.get(dataList.size()-1).equals(dataMatrix.get(f).get(p[f])))
					dataList.add(dataMatrix.get(f).get(p[f]));	
				p[f]++;
			}
		}
		return dataList;
	}

	public boolean nextDoc() throws DoCQSException, IOException {
		dataList=null;
		for (int i=0;i<nodeList.size();i++)
			if (nodeList.get(i).getCurrentDoc()==currentDoc){
				nodeList.get(i).nextDoc();
			}
		int miniDoc=Integer.MAX_VALUE;
		for (int i=0;i<nodeList.size();i++){
			if (nodeList.get(i).getCurrentDoc()<miniDoc){
				miniDoc=nodeList.get(i).getCurrentDoc();
			}
		}
		currentDoc=miniDoc;
		return currentDoc!=Integer.MAX_VALUE;
	}
	public String toString(){
		String r="(";
		for (int i=0;i<nodeList.size();i++){
			r+=nodeList.get(i)+"|";
		}
		return r+"):"+alias;
	}
	public ArrayList<RegNode> getSubRegNode() {
		return nodeList;
	}
}
