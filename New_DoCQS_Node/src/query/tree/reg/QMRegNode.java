package query.tree.reg;

import java.io.IOException;
import java.util.ArrayList;

import exception.DoCQSException;

import query.parser.reg.Token;

public class QMRegNode extends RegNode {
	int lowerBound, upperBound;
	public QMRegNode(int lowerBound, int upperBound){
		super(null);
		this.lowerBound=lowerBound;
		this.upperBound=upperBound;
	}
	public int getCurrentDoc() throws DoCQSException {
		throw new DoCQSException("Misuse question mark.");
	}

	public ArrayList<RegDataInfo> getDataInfos() throws IOException, DoCQSException {
		throw new DoCQSException("Misuse question mark.");
	}

	public boolean nextDoc() throws IOException, DoCQSException {
		throw new DoCQSException("Misuse question mark.");
	}
	public ArrayList<RegNode> getSubRegNode() {
		return null;
	}
	public void jumpToDoc(int doc) throws DoCQSException {
		throw new DoCQSException("Misuse question mark.");
	}
}
