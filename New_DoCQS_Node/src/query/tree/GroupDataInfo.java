package query.tree;

import index.Item;

import java.util.ArrayList;

public class GroupDataInfo extends DataInfo{
	public ArrayList<BasicDataInfo> aggregateList;
	public GroupDataInfo(){
		super();
		aggregateList=new ArrayList<BasicDataInfo>();
	}
}
