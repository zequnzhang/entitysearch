package query.tree;

import index.Item;

import java.io.Serializable;
import java.util.ArrayList;

public class BasicDataInfo extends DataInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7183193281723081176L;
	public ArrayList<Item> entities;
	public Item getItem(String schema){
		for (int i=0;i<infos.size();i++)
			if (infos.get(i).schema.equals(schema)) return entities.get(i);
		return null;
	}
	
	public String toString(){
		String result="";
		for (int i=0;i<entities.size();i++)
			result+=infos.get(i)+":"+entities.get(i);
		return result;
	}
	public int getDoc(){
		if (entities.size()==0) return -1;
		return entities.get(0).doc;
	}
}
