package query.tree;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;


public abstract class DataInfo  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9027180817633130747L;
	public ArrayList<InfoPair> infos;
	public double conf;
	public String getSchema(String s) throws IOException {
		for (int i=0;i<infos.size();i++)
			if (infos.get(i).schema.equals(s)) {
//				if (this instanceof BasicDataInfo && values[i]==null){
//					return ((BasicDataInfo)this).entities[i].getText();
//				}
				return infos.get(i).value;
			}
		return null;
	}
}
