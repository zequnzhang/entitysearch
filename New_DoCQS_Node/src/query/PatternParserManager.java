package query;

import index.IndexReaderManager;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import exception.DoCQSException;

import query.parser.reg.RegExprLexer;
import query.parser.reg.RegExprParser;
import query.parser.reg.Token;
import query.plan.IndexPointer;
import query.plan.SimpleQueryPlan;
import query.tree.reg.RegNode;
import query.tree.reg.SingleRegNode;

public class PatternParserManager {
	static RegExprLexer regExprLexer=null;
	static RegExprParser regExprParser=null;
	public static Token parsePatternToToken(String pattern) throws DoCQSException{
		if (regExprLexer==null){
			regExprLexer=new RegExprLexer(new StringReader(pattern));
			regExprParser=new RegExprParser(regExprLexer);
		}
		else
			regExprLexer.yyreset(new StringReader(pattern));
		
		try {
			return (Token)(regExprParser.parse().value);
		} catch (Error e) {
			e.printStackTrace();
			throw new DoCQSException("Parsing Error: "+e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DoCQSException("Parsing Error: "+e.getMessage());
		}
	}
	
	public static RegNode parsePatternToNode(String pattern) throws DoCQSException, IOException {
		Token t=parsePatternToToken(pattern);
		RegNode node=t.translate();
		HashMap<SingleRegNode, IndexPointer> map=new SimpleQueryPlan().computeQueryPlan(node);
		for (SingleRegNode n:map.keySet()){
			n.setIndexPointer(map.get(n));
		}
		return node;
	}
}
