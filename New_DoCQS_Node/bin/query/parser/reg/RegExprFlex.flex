package query.parser.reg;
import java_cup.runtime.*;
%%
%line
%column
%class RegExprLexer
%cupsym RegSym
%cup
%caseless
%public
%{
%}

LineTerminator 	=\r|\n|\r\n
WhiteSpace     	=({LineTerminator}|[\t\f]|" ")+
KEYWORD = ([^"^""#""!""?""("")""[""]""{""}"":""|"","" ""<"">"\r\n\t\f])+
ENTITY	= "#"{KEYWORD}

%%
<YYINITIAL>{
{WhiteSpace} 	{	}
":"				{	return new Symbol(RegSym.COLON,yyline,yycolumn);	}
"("				{	return new Symbol(RegSym.LPARAE,yyline,yycolumn);	}
")"				{	return new Symbol(RegSym.RPARAE,yyline,yycolumn);	}
"["				{	return new Symbol(RegSym.LBRACK,yyline,yycolumn);	}
"]"				{	return new Symbol(RegSym.RBRACK,yyline,yycolumn);	}
"{"				{	return new Symbol(RegSym.LBRACE,yyline,yycolumn);	}
"}"				{	return new Symbol(RegSym.RBRACE,yyline,yycolumn);	}
"|"				{	return new Symbol(RegSym.VERLINE,yyline,yycolumn);	}
"?"				{	return new Symbol(RegSym.QUESTION,yyline,yycolumn);	}
","				{	return new Symbol(RegSym.COMMA,yyline,yycolumn);	}
"<"				{	return new Symbol(RegSym.LT,yyline,yycolumn);	}
">"				{	return new Symbol(RegSym.RT,yyline,yycolumn);	}
"^"				{	return new Symbol(RegSym.CARET,yyline,yycolumn);	}
{ENTITY}		{	return new Symbol(RegSym.ENTITY,yyline,yycolumn,yytext().toLowerCase());	}
{KEYWORD}		{	return new Symbol(RegSym.KEYWORD,yyline,yycolumn,yytext().toLowerCase());	}
}



