package query.parser.stmt;
import java_cup.runtime.*;

%%
%line
%column
%class StatementLexer
%cupsym StatementSym
%cup
%unicode
%caseless
%public
%{
	String normalizeEntity(String entity){
		if (entity.length()>1)
			return "#"+Character.toUpperCase(entity.charAt(1))+entity.substring(2).toLowerCase();
		else return entity;
	}
%}

LineTerminator 	=\r|\n|\r\n
WhiteSpace     	=({LineTerminator}|[\t\f]|" ")+
ECHAR			=\\[tbnrf\"']
STRING1			='(([^\u0027\u005C\u000A\u000D])|{ECHAR})*'
STRING2			=\"(([^\u0022\u005C\u000A\u000D])|{ECHAR})*\"
ID				=[a-zA-Z"_"]([a-zA-Z0-9"_"])*
ENTITYID		="#"{ID}
FUZZYFUNCID		="~"{ID}
NUMBER			=["."0-9]+

%%
<YYINITIAL>		{
{WhiteSpace}	{	}
"GROUP"			{	return new Symbol(StatementSym.GROUP,yyline,yycolumn);	}
"SELECT"		{	return new Symbol(StatementSym.SELECT,yyline,yycolumn);	}
"FROM"			{	return new Symbol(StatementSym.FROM,yyline,yycolumn);	}
"WHERE"			{	return new Symbol(StatementSym.WHERE,yyline,yycolumn);	}
"AND"			{	return new Symbol(StatementSym.AND,yyline,yycolumn);	}
"OR"			{	return new Symbol(StatementSym.OR,yyline,yycolumn);		}
"ORDER"			{	return new Symbol(StatementSym.ORDER,yyline,yycolumn);	}
"BY"			{	return new Symbol(StatementSym.BY,yyline,yycolumn);		}
"UNIQUE"		{	return new Symbol(StatementSym.UNIQUE,yyline,yycolumn);	}
"DEFINE"		{	return new Symbol(StatementSym.DEFINE,yyline,yycolumn);	}
"DATATYPE"		{	return new Symbol(StatementSym.DATATYPE,yyline,yycolumn);	}
"AS"			{	return new Symbol(StatementSym.AS,yyline,yycolumn);		}
">"				{	return new Symbol(StatementSym.GT,yyline,yycolumn);		}
"<"				{	return new Symbol(StatementSym.LT,yyline,yycolumn);		}
">="			{	return new Symbol(StatementSym.GET,yyline,yycolumn);	}
"<="			{	return new Symbol(StatementSym.LET,yyline,yycolumn);	}
"!="			{	return new Symbol(StatementSym.NEQ,yyline,yycolumn);	}
"+"				{	return new Symbol(StatementSym.PLUS,yyline,yycolumn);	}
"-"				{	return new Symbol(StatementSym.MINUS,yyline,yycolumn);	}
"*"				{	return new Symbol(StatementSym.TIMES,yyline,yycolumn);	}
"/"				{	return new Symbol(StatementSym.DIV,yyline,yycolumn);	}
","				{	return new Symbol(StatementSym.COMMA,yyline,yycolumn);	}
"("				{	return new Symbol(StatementSym.LPARAE,yyline,yycolumn);	}
")"				{	return new Symbol(StatementSym.RPARAE,yyline,yycolumn);	}
"["				{	return new Symbol(StatementSym.LBRACK,yyline,yycolumn);	}
"]"				{	return new Symbol(StatementSym.RBRACK,yyline,yycolumn);	}
"="				{	return new Symbol(StatementSym.EQ,yyline,yycolumn);		}
{STRING1}		{	return new Symbol(StatementSym.STRING,yyline,yycolumn,yytext().substring(1,yytext().length()-1));	}
{STRING2}		{	return new Symbol(StatementSym.STRING,yyline,yycolumn,yytext().substring(1,yytext().length()-1));	}
{ENTITYID}		{	return new Symbol(StatementSym.ENTITYID,yyline,yycolumn,normalizeEntity(yytext()));	}
{FUZZYFUNCID}	{	return new Symbol(StatementSym.FUZZYFUNCID,yyline,yycolumn,yytext());	}
{ID}			{	return new Symbol(StatementSym.ID,yyline,yycolumn,yytext());	}
{NUMBER}		{	return new Symbol(StatementSym.NUMBER,yyline,yycolumn,yytext());	}
.				{	throw new Error("Exception on line "+yyline+" column "+yycolumn+".");	}
}