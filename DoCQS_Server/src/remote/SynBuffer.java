package remote;

import java.util.ArrayList;
import java.util.Collection;

public class SynBuffer<T> {
	ArrayList<T> buffer;
	public SynBuffer(){
		buffer=new ArrayList<T>();
	}
	
	public synchronized ArrayList<T> read(){
		if (buffer==null) return null;
		ArrayList<T> result=new ArrayList<T>();
		result.addAll(buffer);
		buffer.clear();
		return result;
	}
	
	public synchronized void add(T obj){
		if (buffer==null) return;
		buffer.add(obj);		
	}
	
	public synchronized void addAll(Collection<T> l) {
		if (buffer==null) return;
		buffer.addAll(l);
	}
	
	public synchronized int size(){
		if (buffer==null) return 0;
		return buffer.size();
	}
	
	public synchronized void setNull(){
		if (buffer!=null) buffer.clear();
		buffer=null;
	}
}
