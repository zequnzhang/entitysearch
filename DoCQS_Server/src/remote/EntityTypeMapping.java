package remote;

import java.util.HashMap;

public class EntityTypeMapping {
	static HashMap<String, Integer> mapping = null;
	static HashMap<Integer, String> types = null;

	static String[] typeList = new String[] { "#organization", "#date",
			"#percent", "#person", "#location", "#money", "#time", "#email",
			"#number" };
	static int maxFBTypeNum = 3000;

	static void init() {
		mapping = new HashMap<>();
		types = new HashMap<>();
		for (int i = 0; i < typeList.length; i++)
			types.put(i, typeList[i]);
		for (int i = 0; i < maxFBTypeNum; i++)
			types.put(getFreebaseTypeId(i), "#freebase." + i);
		for (int i = 0; i < maxFBTypeNum; i++)
			types.put(getMybaseTypeId(i), "#mybase." + i);
		for (Integer k:types.keySet()){
			mapping.put(types.get(k), k);
		}
	}

	public static int getFreebaseTypeId(int k) {
		return k + 10000;
	}
	
	public static int getMybaseTypeId(int k) {
		return k + 20000;
	}


	public static Integer getId(String type) {
		if (mapping == null)
			init();
		return mapping.get(type);
	}

	public static String getType(int id) {
		if (mapping == null)
			init();
		return types.get(id);
	}
}
