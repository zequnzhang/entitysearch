package remote.server.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.DocInfo;

public interface DocumentStreamInterface extends Remote{
	public ArrayList<DocInfo> read() throws RemoteException;
	public void close() throws RemoteException;
}
