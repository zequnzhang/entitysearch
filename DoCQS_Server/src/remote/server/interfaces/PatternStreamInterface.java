package remote.server.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.TokenItem;

public interface PatternStreamInterface extends Remote {
	public ArrayList<TokenItem[]> read() throws RemoteException;
	public void close() throws RemoteException;
}
