package remote.server.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.Word;

public interface QueryHandlerInterface extends Remote{
	public PatternStreamInterface getPatternStream(int timeout, int totalNum, String pattern) throws RemoteException;
	public DocumentStreamInterface getDocumentStream(int timeout, int totalNum, ArrayList<Long> docList) throws RemoteException;
	public WordStreamInterface getWordStream(int timeout, int totalNum, Word[] words) throws RemoteException;
}
