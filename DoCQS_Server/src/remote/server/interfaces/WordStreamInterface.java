package remote.server.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.WordFreq;

public interface WordStreamInterface extends Remote{
	public ArrayList<WordFreq> read() throws RemoteException;
	public void close() throws RemoteException;
}
