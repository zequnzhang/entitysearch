package remote.server.impls;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import remote.Word;
import remote.server.interfaces.DocumentStreamInterface;
import remote.server.interfaces.PatternStreamInterface;
import remote.server.interfaces.QueryHandlerInterface;
import remote.server.interfaces.WordStreamInterface;

public class QueryHandler extends UnicastRemoteObject implements QueryHandlerInterface {
	
	protected QueryHandler() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7395951109345746536L;

	@Override
	public PatternStreamInterface getPatternStream(int timeout, int totalNum, String pattern)
			throws RemoteException {
		PatternStream patternStream=new PatternStream(timeout, totalNum, pattern);
		Thread thread=new Thread(patternStream);
		thread.start();
		return patternStream;
	}

	@Override
	public DocumentStreamInterface getDocumentStream(int timeout, int totalNum, ArrayList<Long> docList)
			throws RemoteException {
		DocumentStream documentStream=new DocumentStream(timeout, totalNum, docList);
		Thread thread=new Thread(documentStream);
		thread.start();
		return documentStream;
	}
	
	@Override	
	public WordStreamInterface getWordStream(int timeout, int totalNum, Word[] words) throws RemoteException {
		WordStream wordStream = new WordStream(timeout, totalNum, words);
		Thread thread = new Thread(wordStream);
		thread.start();
		return wordStream;
	}

}
