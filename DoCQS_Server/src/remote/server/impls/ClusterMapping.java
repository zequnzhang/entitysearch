package remote.server.impls;

import util.Pair;

public class ClusterMapping {
	public static final long DOC_BLOCK=1000000000l;
	
	public static Pair<Integer, Long> global2local(long gDoc){
		int cluster=(int)(gDoc/DOC_BLOCK);
		long lDoc=gDoc%DOC_BLOCK;
		return new Pair<Integer, Long>(cluster, lDoc);
	}
	
	public static long local2global(int cluster, long lDoc){
		return cluster*DOC_BLOCK+lDoc;		
	}
	
	public static void main(String[] args){
		System.out.println(local2global(0, 153682));
		
	}

}
