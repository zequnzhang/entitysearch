package remote.server.impls;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import com.sun.org.apache.bcel.internal.generic.MONITORENTER;

import remote.DocInfo;
import remote.TokenItem;
import remote.node.interfaces.NodeDocumentStreamInterface;
import remote.node.interfaces.NodePatternStreamInterface;
import remote.server.interfaces.DocumentStreamInterface;
import remote.server.interfaces.PatternStreamInterface;
import util.Pair;

public class PatternStream extends UnicastRemoteObject implements
		PatternStreamInterface, Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6494333681528195660L;
	private static final int BUFFER_SIZE = 100000;
	
	String pattern;
	PatternDataStream stream;
	
	public PatternStream(int timeout, int totalNum, String pattern) throws RemoteException {
		this.pattern = pattern;
		this.stream = new PatternDataStream(timeout, totalNum, BUFFER_SIZE);
	}

	@Override
	public ArrayList<TokenItem[]> read() throws RemoteException {
		return stream.read();
	}

	@Override
	public void close() throws RemoteException {
		stream.close();
	}

	@Override
	public void run() {
		stream.run();
	}
	
	class PatternDataStream extends DataStream<TokenItem[]> {
		
		NodePatternStreamInterface[] interfaces;
		
		public PatternDataStream(int timeout, int totalNum, int bufferSize) throws RemoteException {
			super(timeout, totalNum, bufferSize);
			interfaces = new NodePatternStreamInterface[Nodes.names.length];
			for (int i = 0;i<interfaces.length;i ++) {
				interfaces[i] = null;
				if (Monitor.handlers[i] != null) {
					interfaces[i] = Monitor.handlers[i].getNodePatternStream(pattern);
				}
			}
		}

		@Override
		public ArrayList<TokenItem[]> collectData(int cid) throws RemoteException {
			if (interfaces[cid] == null)
				return null;
			ArrayList<TokenItem[]> res = interfaces[cid].read();
			if (res == null)
				return null;
			for (TokenItem[] info:res)
				for (int i = 0 ;i<info.length;i++)
					info[i].doc = ClusterMapping.local2global(cid, info[i].doc);
			return res;
		}

		@Override
		public void closeThread(int cid) throws RemoteException {
			if (interfaces[cid] != null) {
				interfaces[cid].close();
			}
		}
		
	}

}
