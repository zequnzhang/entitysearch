package remote.server.impls;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import com.sun.org.apache.bcel.internal.generic.MONITORENTER;

import remote.DocInfo;
import remote.node.interfaces.NodeDocumentStreamInterface;
import remote.server.interfaces.DocumentStreamInterface;
import util.Pair;

public class DocumentStream extends UnicastRemoteObject implements
		DocumentStreamInterface, Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6494333681528195660L;
	private static final int BUFFER_SIZE = 10000;
	
	ArrayList<Long> docList;
	DocumentDataStream stream;
	ArrayList<ArrayList<Integer>> docs;
	
	public DocumentStream(int timeout, int totalNum, ArrayList<Long> docList) throws RemoteException {
		docs = new ArrayList<>();
		for (int i = 0 ;i < Nodes.names.length;i++)
			docs.add(new ArrayList<Integer>());
		for (int i = 0; i<docList.size();i++) {
			Pair<Integer, Long> p = ClusterMapping.global2local(docList.get(i));
			docs.get(p.first).add(p.second.intValue());			
		}
		this.docList = docList;
		this.stream = new DocumentDataStream(timeout, totalNum, BUFFER_SIZE);
	}

	@Override
	public ArrayList<DocInfo> read() throws RemoteException {
		return stream.read();
	}

	@Override
	public void close() throws RemoteException {
		stream.close();
	}

	@Override
	public void run() {
		stream.run();
	}
	
	class DocumentDataStream extends DataStream<DocInfo> {
		
		NodeDocumentStreamInterface[] interfaces;
		
		public DocumentDataStream(int timeout, int totalNum, int bufferSize) throws RemoteException {
			super(timeout, totalNum, bufferSize);
			interfaces = new NodeDocumentStreamInterface[Nodes.names.length];
			for (int i = 0;i<interfaces.length;i ++) {
				interfaces[i] = null;
				if (Monitor.handlers[i] != null) {
					interfaces[i] = Monitor.handlers[i].getNodeDocumentStream(docs.get(i));
				}
			}
		}

		@Override
		public ArrayList<DocInfo> collectData(int cid) throws RemoteException {
			if (interfaces[cid] == null)
				return null;
			ArrayList<DocInfo> res = interfaces[cid].read();
			if (res == null)
				return null;
			for (DocInfo info:res)
				info.id = ClusterMapping.local2global(cid, info.id);
			return res;
		}

		@Override
		public void closeThread(int cid) throws RemoteException {
			if (interfaces[cid] != null) {
				interfaces[cid].close();
			}
		}
		
	}

}
