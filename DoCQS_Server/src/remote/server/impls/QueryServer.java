package remote.server.impls;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class QueryServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			Monitor.init();
			LocateRegistry.createRegistry(1099);
			QueryHandler handler=new QueryHandler();
			Naming.rebind("server", handler);
			System.out.println("Query Server is ready.");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
