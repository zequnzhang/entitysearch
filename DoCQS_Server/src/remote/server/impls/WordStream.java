package remote.server.impls;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import com.sun.org.apache.bcel.internal.generic.MONITORENTER;

import remote.DocInfo;
import remote.TokenItem;
import remote.Word;
import remote.WordFreq;
import remote.node.interfaces.NodeDocumentStreamInterface;
import remote.node.interfaces.NodePatternStreamInterface;
import remote.node.interfaces.NodeWordStreamInterface;
import remote.server.interfaces.DocumentStreamInterface;
import remote.server.interfaces.PatternStreamInterface;
import remote.server.interfaces.WordStreamInterface;
import util.Pair;

public class WordStream extends UnicastRemoteObject implements
		WordStreamInterface, Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6494333681528195660L;
	private static final int BUFFER_SIZE = 1000000;
	
	Word[] words;
	WordDataStream stream;
	
	public WordStream(int timeout, int totalNum, Word[] words) throws RemoteException {
		this.words = words;
		this.stream = new WordDataStream(timeout, totalNum, BUFFER_SIZE);
	}

	@Override
	public ArrayList<WordFreq> read() throws RemoteException {
		return stream.read();
	}

	@Override
	public void close() throws RemoteException {
		stream.close();
	}

	@Override
	public void run() {
		stream.run();
	}
	
	class WordDataStream extends DataStream<WordFreq> {
		
		NodeWordStreamInterface[] interfaces;
		
		public WordDataStream(int timeout, int totalNum, int bufferSize) throws RemoteException {
			super(timeout, totalNum, bufferSize);
			interfaces = new NodeWordStreamInterface[Nodes.names.length];
			for (int i = 0;i<interfaces.length;i ++) {
				interfaces[i] = null;
				if (Monitor.handlers[i] != null) {
					interfaces[i] = Monitor.handlers[i].getNodeWordStream(words);
				}
			}
		}

		@Override
		public ArrayList<WordFreq> collectData(int cid) throws RemoteException {
			if (interfaces[cid] == null)
				return null;
			ArrayList<WordFreq> res = interfaces[cid].read();
			if (res == null) 
				return null;
			for (WordFreq info:res)
				info.doc = ClusterMapping.local2global(cid, info.doc);
			return res;
		}

		@Override
		public void closeThread(int cid) throws RemoteException {
			if (interfaces[cid] != null) {
				interfaces[cid].close();
			}
		}
		
	}

}
