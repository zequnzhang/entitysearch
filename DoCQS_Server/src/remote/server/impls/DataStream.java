package remote.server.impls;

import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.SynBuffer;
import remote.node.interfaces.NodeQueryHandlerInterface;

public abstract class DataStream<T> {
	static final int INTERVAL = 1000;
	static final int WAKEUP_TIME = 10;

	private int timeout;
	private int totalNum;
	private int bufferSize;

	private SynBuffer<T> buffer;
	private boolean stop;
	private int collectedNum;
	private ArrayList<StreamThread> streamThreads;

	public DataStream(int timeout, int totalNum, int bufferSize) {
		this.timeout = timeout;
		this.totalNum = totalNum;
		this.bufferSize = bufferSize;
		buffer = new SynBuffer<T>();
		stop = false;
		streamThreads = new ArrayList<StreamThread>();
		for (int i = 0; i < Nodes.names.length; i++)
			streamThreads.add(new StreamThread(i));
		collectedNum = 0;
	}

	public ArrayList<T> read() throws RemoteException {
		ArrayList<T> data = buffer.read();
		if (stop)
			buffer.setNull();
		return data;
	}

	public void close() throws RemoteException {
		stop = true;
	}

	public abstract ArrayList<T> collectData(int cid) throws RemoteException;

	public abstract void closeThread(int cid) throws RemoteException;

	public void run() {
		Thread[] threads = new Thread[streamThreads.size()];
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(streamThreads.get(i));
			threads[i].start();
		}
		long startTime = System.currentTimeMillis();
		while (true) {
			try {
				Thread.sleep(INTERVAL);
			} catch (InterruptedException e) {
			}
			if (System.currentTimeMillis() - startTime > timeout
					|| collectedNum > totalNum) {
				stop = true;
			}
			boolean allstop = true;
			for (int i = 0; i < streamThreads.size(); i++) {
				if (!streamThreads.get(i).finished)
					allstop = false;
			}
			if (allstop)
				break;
		}
		System.out.println("Retrieve " + collectedNum + " Items.");
		System.out.println("Finished!!!");
		stop = true;
	}

	class StreamThread implements Runnable {
		int cid;
		NodeQueryHandlerInterface handler;
		boolean finished;

		public StreamThread(int cid) {
			this.cid = cid;
			this.handler = Monitor.handlers[cid];
			finished = false;
		}

		@Override
		public void run() {
			try {
				while (!stop) {
					ArrayList<T> dataList = collectData(cid);
//					System.out.println(cid + "\t" + dataList.size());
					if (dataList == null)
						break;
					if (!stop) {
						buffer.addAll(dataList);
						collectedNum += dataList.size();
					}
					if (buffer.size() > bufferSize) {
						int time = 0;
						for (; time < WAKEUP_TIME; time++) {
							try {
								Thread.sleep(INTERVAL);
							} catch (InterruptedException e) {
							}
							if (stop || buffer.size() <= bufferSize)
								break;
						}
						if (stop || time >= WAKEUP_TIME) {
							break;
						}
					}
				}
				closeThread(cid);
			} catch (RemoteException e) {
				System.err.println("Error in remote call.");
			}
			finished = true;
		}

	}
}
