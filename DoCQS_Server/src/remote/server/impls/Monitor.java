package remote.server.impls;

import java.rmi.Naming;
import java.rmi.RemoteException;

import remote.node.interfaces.NodeQueryHandlerInterface;

public class Monitor {
	// Time out for one rmi query call.
	static final int RMI_QUERY_TIME_OUT = 5000;
	static final int PORT = 1100;

	static NodeQueryHandlerInterface[] handlers = null;
	
	public static void init() {
		System.setProperty("sun.rmi.transport.tcp.responseTimeout",
				RMI_QUERY_TIME_OUT + "");
		handlers = new NodeQueryHandlerInterface[Nodes.names.length];
		for (int i = 0 ;i<handlers.length;i++)
			handlers[i] = connect(Nodes.names[i]);
		Thread thread = new Thread(new DaemonThread());
		thread.start();
	}
	
	public static NodeQueryHandlerInterface connect(String cluster) {
		try {
			NodeQueryHandlerInterface i = (NodeQueryHandlerInterface) Naming
					.lookup("//" + cluster + ":" + PORT + "/query");
			System.out.println("Connect to " + cluster + " ...");
			return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static NodeQueryHandlerInterface getQueryHandler(int k) {
		if (handlers == null)
			init();
		return handlers[k];
	}
}
class DaemonThread implements Runnable {
	static final int CHECK_INTERVAL = 10000;

	public DaemonThread() {
	}

	@Override
	public void run() {
		while (true) {
			for (int i = 0; i < Monitor.handlers.length; i++) {
				synchronized (Nodes.names[i]) {
					if (Monitor.handlers[i] == null)
						Monitor.handlers[i] = Monitor.connect(Nodes.names[i]);
					else {
						try {
							Monitor.handlers[i].ping();
						} catch (RemoteException e) {
							System.out.println("Disconnected from " + Nodes.names[i] + " ...");
							Monitor.handlers[i] = null;
						}
					}
				}
			}

			try {
				Thread.sleep(CHECK_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
