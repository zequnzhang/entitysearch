package remote.node.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import remote.DocInfo;

public interface NodeDocumentStreamInterface extends Remote{
	public ArrayList<DocInfo> read() throws RemoteException;
	public void close() throws RemoteException;
}
