package remote;

import java.io.Serializable;

public class WordFreq implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6673973177682894478L;
	public long doc;
	public float pagerank;
	public int[] freqs;

	public String toString() {
		String r = doc + ", " + pagerank + ": ";
		for (int i = 0; i < freqs.length; i++)
			r += " " + freqs[i];
		return r;

	}
}
