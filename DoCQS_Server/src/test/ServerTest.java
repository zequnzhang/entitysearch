package test;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Random;

import remote.DocInfo;
import remote.TokenItem;
import remote.Word;
import remote.WordFreq;
import remote.server.impls.ClusterMapping;
import remote.server.interfaces.DocumentStreamInterface;
import remote.server.interfaces.QueryHandlerInterface;
import remote.server.interfaces.WordStreamInterface;

public class ServerTest {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, InterruptedException{
		QueryHandlerInterface handler=(QueryHandlerInterface)Naming.lookup("//localhost:1099/server");
		
//		Word[] words = new Word[3];
//		words[0] = new Word("news", false, true);
//		words[1] = new Word("business", true, true);
//		words[2] = new Word("sina", false, false);
//		WordStreamInterface stream = handler.getWordStream(200000, 5000000, words);
//		ArrayList<WordFreq> itemList;
//		while ((itemList = stream.read())!=null) {
//			System.out.println(itemList.size());
//			for (WordFreq f:itemList)
//				System.out.println(f);			
//			Thread.sleep(100);
//		}

		
		ArrayList<Long> docList=new ArrayList<Long>();
		Random r = new Random();
		for (int i=0;i<1000;i++){
			docList.add(ClusterMapping.local2global(r.nextInt(19), r.nextInt(6000000)));
		}
		int nn=0;
		DocumentStreamInterface stream=handler.getDocumentStream(100000, 100000, docList);
		ArrayList<DocInfo> infoList = new ArrayList<DocInfo>();
		while ((infoList=stream.read())!=null){
			for (DocInfo info:infoList) {
				nn++;
				System.out.println(info.docId + ":"+info.entities.length);
			}
			Thread.sleep(100);
		}
		System.out.println(nn);
		
//		NodePatternStreamInterface stream=handler.getNodePatternStream("[Kevin #email]<30>");
//		ArrayList<DataItem[]> items;
//		while ((items=stream.read())!=null){
//			System.out.println(items.size());
//		}
//		stream.close();
	}

}
